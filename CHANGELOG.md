# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.3.11 - 2024-11-21(09:41:43 +0000)

### Changes

- [PacketInterception] Plugin Missing After Config Change and Reboot

## Release v0.3.10 - 2024-10-15(09:59:01 +0000)

### Other

- [PacketInterception] Delete sockets when process connects to it

## Release v0.3.9 - 2024-10-10(11:16:40 +0000)

### Changes

- [PacketInterception] standardize the socket paths

## Release v0.3.8 - 2024-10-03(09:48:24 +0000)

### Other

- SKIPLOG not available on non broadcom hardware

## Release v0.3.7 - 2024-10-01(12:35:26 +0000)

### Changes

- [PacketInterception] standardize the socket paths

## Release v0.3.6 - 2024-09-26(11:35:35 +0000)

### Changes

- [PacketInterception] standardize the socket paths

## Release v0.3.5 - 2024-09-19(06:43:42 +0000)

### Other

- [PacketInterception] NrOfPacketsTimedout increases after a long time

## Release v0.3.4 - 2024-08-15(10:06:28 +0000)

### Other

- [PacketInterception] NrOfPacketsTimedout increase when...

## Release v0.3.3 - 2024-08-08(09:06:49 +0000)

### Changes

- CLONE - [PacketInterception] Do not open listen sockets when disabled

## Release v0.3.2 - 2024-07-23(09:44:45 +0000)

### Fixes

- Better shutdown script

## Release v0.3.1 - 2024-07-09(13:15:10 +0000)

### Other

- TR-181 PacketInterception DM issues

## Release v0.3.0 - 2024-07-04(08:27:33 +0000)

### New

- [PacketInterception] When Flowmonitor is disabled no interception is done on HTTP, HTPS and very few on Quic

## Release v0.2.5 - 2024-04-11(07:52:14 +0000)

### Changes

- [ServiceId][AMX} No more packet information

## Release v0.2.4 - 2024-04-10(10:00:45 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.2.3 - 2024-03-21(17:30:28 +0000)

### Changes

- Useragent collection is broken on tmnet generic since v05.65.00

## Release v0.2.2 - 2024-03-06(15:24:36 +0000)

### Changes

- [PacketInterception] Add Flowmonitor support

### Other

- Disable debian package generation

## Release v0.2.1 - 2024-02-08(08:48:54 +0000)

### Changes

- Embedded filtering AMX -Could not classify entry for classifier

## Release v0.2.0 - 2024-02-05(16:23:57 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.1.1 - 2024-01-22(10:48:13 +0000)

### Changes

- [ServiceID] Add components to meta(s)

## Release v0.1.0 - 2024-01-16(19:50:53 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.0.17 - 2024-01-04(14:34:55 +0000)

### Changes

- [ServiceID] Update Config options for SOP

## Release v0.0.16 - 2023-12-13(14:38:48 +0000)

### Changes

- [PacketInterception] Intercept DNS traffic to local DNS server

## Release v0.0.15 - 2023-12-07(10:51:43 +0000)

### Other

- [prplos] packet-interception: package should be enabed only on full-featured-prplwrt config

## Release v0.0.14 - 2023-11-13(13:54:44 +0000)

### Changes

- [ServiceIdentification] Enable QUIC support

## Release v0.0.13 - 2023-11-13(12:13:21 +0000)

### Other

- CLONE - [Packet Interception] remove peafowl references

## Release v0.0.12 - 2023-10-09(08:42:24 +0000)

### Changes

- [ServiceIdentification] Increase performance

## Release v0.0.11 - 2023-09-11(06:44:02 +0000)

### Changes

- [AMX][IMTP] Use new libimtp APIs

## Release v0.0.10 - 2023-07-04(15:08:13 +0000)

### Fixes

- Init script has no shutdown function

## Release v0.0.9 - 2023-06-09(08:47:46 +0000)

## Release v0.0.8 - 2023-04-20(09:00:00 +0000)

### Changes

- [Packet Interception] Add support for HTTP(s) interception and parsing

## Release v0.0.7 - 2023-03-21(10:23:29 +0000)

### Changes

- [AMX][Packet-interception] Rework Communication Config

## Release v0.0.6 - 2023-03-17(12:04:14 +0000)

### Other

- Use defaults odl directory

## Release v0.0.5 - 2023-03-17(09:33:12 +0000)

### Fixes

- Add peafowl to the supported parsers

## Release v0.0.4 - 2022-12-19(16:04:01 +0000)

### Other

- Add missing iptables-mod-nfqueue dependency

## Release v0.0.3 - 2022-11-29(15:54:52 +0000)

### Other

- Opensource component

## Release v0.0.2 - 2022-11-23(11:59:34 +0000)

### Other

- [Packet Interception] The whole component should be able to be turned on/off

## Release v0.0.1 - 2022-11-17(12:33:08 +0000)

### Other

- Improve amx template
- [Packet Interception] Create the new Packet Interception component

