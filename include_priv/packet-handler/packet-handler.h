/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __PACKETHANDLER_H__
#define __PACKETHANDLER_H__

#include <netlink-utils/nfqueue.h>
#include <packet-interception/packet.h>

#include "packet-interception.h"

typedef struct {
    uint32_t nr_of_packets_received;    // The number of packets that were received
    uint32_t nr_of_packets_handled;     // The number of packets that were handled
    uint32_t nr_of_packets_timedout;    // The number of packets that timed out.
    uint32_t nr_of_packets_accepted;    // The number of accepted packets
    uint32_t nr_of_packets_dropped;     // The number of dropped packets.
    uint32_t min_response_time;         // minimum response time, specified in ms
    uint32_t max_response_time;         // maximum response time, specified in ms
    uint32_t avg_response_time;         // average response time, specified in ms
} packet_handler_stats_t;

typedef struct {
    common_t common;
    amxc_llist_it_t it;
    int32_t interval;
    packet_verdict_t default_verdict;
    amxc_llist_t packets_llist;
    amxp_timer_t* timer;
    packet_handler_stats_t stats;
} packet_handler_t;

bool packet_handler_verdict_cb(packet_handler_t* handler, nfqueue_t* nfqueue, const packet_id_t* packet, const packet_verdict_t* verdict, uint32_t nr_of_packets);
void packet_handler_packet_cb(int fd, void* priv);

bool packet_handler_activate(packet_handler_t* handler);
bool packet_handler_deactivate(packet_handler_t* handler);
void packet_handler_activate_deactivate_all(bool activate);

packet_handler_t* packet_handler_find(const char* name);

packet_handler_t* packet_handler_create(amxd_object_t* object);
void packet_handler_delete(packet_handler_t* handler);

#endif // __PACKETHANDLER_H__
