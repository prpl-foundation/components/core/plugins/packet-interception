/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __PACKETINTERCEPTION_H__
#define __PACKETINTERCEPTION_H__

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <amxo/amxo_save.h>
#include <amxp/amxp_timer.h>
#include <amxm/amxm.h>

#include <netlink-utils/conntrack.h>

#define OBJECT_NAME(OBJECT) amxd_object_get_name(OBJECT, AMXD_OBJECT_NAMED)
#define STRING_EMPTY(TEXT) ((TEXT == NULL) || (*TEXT == 0))

typedef enum {
    INTERCEPTION_ENABLED,
    INTERCEPTION_DISABLED,
    INTERCEPTION_ERROR_MISCONFIG,
    INTERCEPTION_ERROR
} status_t;

typedef struct {
    status_t status;
    amxd_object_t* object;
} common_t;

int _packetinterception_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser);
void _packetinterception_enabled_changed(const char* const sig_name,
                                         const amxc_var_t* const data,
                                         void* const priv);
void _packetinterception_start(const char* const sig_name,
                               const amxc_var_t* const data,
                               void* const priv);

bool packetinterception_get_enable(void);
amxd_dm_t* PRIVATE packetinterception_get_dm(void);
amxo_parser_t* PRIVATE packetinterception_get_parser(void);
amxm_module_t* PRIVATE packetinterception_get_module(void);
conntrack_t* packetinterception_get_conntrack(void);
uint32_t packetinterception_get_qnum_min_max(bool min);
uint32_t packetinterception_get_notoffload_mark(void);
uint32_t packetinterception_get_tcpreset_mark(void);

void set_status(common_t* c, status_t status);

const char* packetinterception_get_table(const char* search_path, const char* default_table);
const char* packetinterception_get_chain(const char* search_path, const char* default_chain);

const char* packetinterception_get_parser_module_dir(const char* default_dir);
const char* packetinterception_get_comm_config_module_dir(const char* default_dir);
const char* packetinterception_get_comm_config_mib_dir(const char* default_dir);

const char* object_da_string(amxd_object_t* const object, const char* name);

#endif // __PACKETINTERCEPTION_H__
