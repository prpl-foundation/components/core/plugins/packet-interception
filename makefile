include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C mod-comm-ctrl-socket/src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C test clean
	$(MAKE) -C mod-comm-ctrl-socket/src clean
	$(MAKE) -C mod-comm-ctrl-socket/test clean

install: all
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/mod-comm-ctrl-socket.so $(DEST)/usr/lib/amx/modules/mod-comm-ctrl-socket.so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/defaults.d
	$(foreach odl,$(wildcard odl/defaults.d/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/defaults.d/;)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/interception
	$(INSTALL) -D -p -m 0644 odl/interception/* $(DEST)/etc/amx/$(COMPONENT)/interception/
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/condition
	$(foreach odl,$(wildcard odl/condition/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/condition/;)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/packet-handler
	$(foreach odl,$(wildcard odl/packet-handler/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/packet-handler/;)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/comm-config
	$(foreach odl,$(wildcard odl/comm-config/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/comm-config/;)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/modules
	$(foreach odl,$(wildcard mod-comm-ctrl-socket/odl/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/modules/;)
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-packetinterception_mapping.odl
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 config/packet-interception.firewall $(DEST)$(CONFIG_SAH_AMX_PACKET_INTERCEPTION_TR181_FIREWALL_RULEPATH)/rules4/packet-interception.firewall
	$(INSTALL) -D -p -m 0644 config/packet-interception_ipv6.firewall $(DEST)$(CONFIG_SAH_AMX_PACKET_INTERCEPTION_TR181_FIREWALL_RULEPATH)/rules6/packet-interception.firewall
ifeq ($(CONFIG_SAH_AMX_PACKET_INTERCEPTION_SUPPORT_SKIPLOG),y)
	$(INSTALL) -D -p -m 0644 config/packet-interception_bcm.firewall $(DEST)$(CONFIG_SAH_AMX_PACKET_INTERCEPTION_TR181_FIREWALL_RULEPATH)/rules4/packet-interception_bcm.firewall
endif
ifeq ($(CONFIG_SAH_AMX_PACKET_INTERCEPTION_SUPPORT_SKIPLOG),y)
	$(INSTALL) -D -p -m 0644 config/packet-interception_ipv6_bcm.firewall $(DEST)$(CONFIG_SAH_AMX_PACKET_INTERCEPTION_TR181_FIREWALL_RULEPATH)/rules6/packet-interception_bcm.firewall
endif

package: all
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/mod-comm-ctrl-socket.so $(PKGDIR)/usr/lib/amx/modules/mod-comm-ctrl-socket.so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/defaults.d
	$(INSTALL) -D -p -m 0644 odl/defaults.d/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d/
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/interception
	$(INSTALL) -D -p -m 0644 odl/interception/* $(PKGDIR)/etc/amx/$(COMPONENT)/interception/
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/condition
	$(INSTALL) -D -p -m 0644 odl/condition/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/condition/
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/packet-handler
	$(INSTALL) -D -p -m 0644 odl/packet-handler/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/packet-handler/
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/comm-config
	$(INSTALL) -D -p -m 0644 odl/comm-config/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/comm-config/
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/modules
	$(INSTALL) -D -p -m 0644 mod-comm-ctrl-socket/odl/*.odl $(PKGDIR)/etc/amx/modules/
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-packetinterception_mapping.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 config/packet-interception.firewall $(PKGDIR)$(CONFIG_SAH_AMX_PACKET_INTERCEPTION_TR181_FIREWALL_RULEPATH)/rules4/packet-interception.firewall
	$(INSTALL) -D -p -m 0644 config/packet-interception_ipv6.firewall $(PKGDIR)$(CONFIG_SAH_AMX_PACKET_INTERCEPTION_TR181_FIREWALL_RULEPATH)/rules6/packet-interception.firewall
ifeq ($(CONFIG_SAH_AMX_PACKET_INTERCEPTION_SUPPORT_SKIPLOG),y)
	$(INSTALL) -D -p -m 0644 config/packet-interception_bcm.firewall $(PKGDIR)$(CONFIG_SAH_AMX_PACKET_INTERCEPTION_TR181_FIREWALL_RULEPATH)/rules4/packet-interception_bcm.firewall
endif
ifeq ($(CONFIG_SAH_AMX_PACKET_INTERCEPTION_SUPPORT_SKIPLOG),y)
	$(INSTALL) -D -p -m 0644 config/packet-interception_ipv6_bcm.firewall $(PKGDIR)$(CONFIG_SAH_AMX_PACKET_INTERCEPTION_TR181_FIREWALL_RULEPATH)/rules6/packet-interception_bcm.firewall
endif
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	mkdir -p output/doc
	VERSION=$(VERSION) doxygen doc/netmodel.doxy

	$(eval ODLFILES += odl/$(COMPONENT)_definition.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C mod-comm-ctrl-socket/test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test