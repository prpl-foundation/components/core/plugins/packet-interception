/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dm_socket-config.h"
#include "mod-comm-socket.h"
#include "socket.h"

#include "amxm/amxm.h"

#define ME "socket"

amxd_object_t* socket_config_template(amxd_dm_t* dm) {
    amxd_object_t* templ = NULL;
    templ = amxd_dm_findf(dm, "PacketInterception.CommunicationConfig.Socket");
    return templ;
}

static int comm_config_init(UNUSED amxd_object_t* templ, amxd_object_t* instance, UNUSED void* priv) {
    socket_config_t* socket = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    const char* parser = NULL;
    int retval = 0;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_null(instance, exit);

    socket = (socket_config_t*) instance->priv;
    if(socket == NULL) {
        socket = socket_config_create(instance);
        when_null_trace(socket, exit, ERROR, "Could not create Socket config [%s]", OBJECT_NAME(instance));
    }

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Name", OBJECT_NAME(instance));
    amxc_var_add_key(cstring_t, &args, "Module", ME);

    parser = object_da_string(instance, "Parser");
    if(!STRING_EMPTY(parser)) {
        amxc_var_add_key(cstring_t, &args, "Parser", parser);
    }

    if(amxm_execute_function("self", CORE, "register-comm-config", &args, &ret) != 0) {
        SAH_TRACEZ_ERROR(ME, "Could not register socket communication config [%s]", OBJECT_NAME(instance));
        goto exit;
    }

    socket_config_activate(socket);
    retval = 1;

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return retval;
}

int socket_config_dm_cleanup(UNUSED amxd_object_t* templ, amxd_object_t* instance, UNUSED void* priv) {
    socket_config_t* socket = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_null(instance, exit);

    socket = (socket_config_t*) instance->priv;
    when_null(socket, exit);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Name", OBJECT_NAME(instance));

    amxm_execute_function("self", CORE, "deregister-comm-config", &args, &ret);
    socket_config_delete(socket);

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return 1;
}

void _socket_config_added(UNUSED const char* const sig_name, const amxc_var_t* const event_data, UNUSED void* const priv) {
    amxd_dm_t* dm = socket_get_dm();
    amxd_object_t* socket_configs = amxd_dm_signal_get_object(dm, event_data);
    amxd_object_t* socket_config = amxd_object_get_instance(socket_configs, NULL, GET_UINT32(event_data, "index"));

    SAH_TRACEZ_INFO(ME, "Received event: [%s]", sig_name);

    when_null(socket_config, exit);

    comm_config_init(socket_configs, socket_config, NULL);

exit:
    return;
}

void _socket_config_changed(UNUSED const char* const sig_name,
                            const amxc_var_t* const event_data,
                            UNUSED void* const priv) {
    amxd_dm_t* dm = socket_get_dm();
    amxd_object_t* objSocketConfig = amxd_dm_signal_get_object(dm, event_data);
    socket_config_t* socket = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxc_var_t* status_var = GET_ARG(params, "Status");
    const amxc_htable_t* hparams = amxc_var_constcast(amxc_htable_t, params);
    const char* param = NULL;

    // Status update should not trigger deactivation of socket config
    amxc_var_take_it(status_var);

    SAH_TRACEZ_INFO(ME, "Received event: [%s]", sig_name);

    when_null(objSocketConfig, exit);
    socket = (socket_config_t*) objSocketConfig->priv;
    when_null(socket, exit);

    param = GETP_CHAR(params, "Parser.to");
    if(param != NULL) {
        amxc_var_t args;
        amxc_var_t ret;

        amxc_var_init(&args);
        amxc_var_init(&ret);

        amxc_var_add_key(cstring_t, &args, "Name", OBJECT_NAME(objSocketConfig));
        amxc_var_add_key(cstring_t, &args, "Module", ME);

        if(!STRING_EMPTY(param)) {
            amxc_var_add_key(cstring_t, &args, "Parser", param);
        }

        amxm_execute_function("self", CORE, "register-comm-config", &args, &ret);

        amxc_var_clean(&args);
        amxc_var_clean(&ret);
    }

    // Parser update should not trigger deactivation of socket config
    amxc_var_take_it(GET_ARG(params, "Parser"));
    if(amxc_htable_is_empty(hparams)) {
        goto exit;
    }

    socket_config_deactivate(socket);
    socket_config_activate(socket);

exit:
    return;
}

void _socket_global_enabled_changed(UNUSED const char* const sig_name,
                                    const amxc_var_t* const event_data,
                                    UNUSED void* const priv) {
    bool enable = GETP_BOOL(event_data, "parameters.Enable.to");
    socket_set_global_enable(enable);
}
