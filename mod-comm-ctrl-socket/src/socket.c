/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "socket.h"

#include <imtp/imtp_frame.h>

#include <yajl/yajl_gen.h>
#include <amxj/amxj_variant.h>

#define ME "socket"

static amxc_llist_t socket_configs;

static bool socket_send_verdict(socket_config_t* socket, packet_id_t* packet, packet_verdict_t* verdict) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* verdict_var = NULL;
    bool res = false;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Communication Config", OBJECT_NAME(socket->common.object));
    amxc_var_add_key(uint32_t, &args, "ID", packet->id);
    amxc_var_add_key(uint16_t, &args, "Group", packet->group);
    verdict_var = amxc_var_add_new_key(&args, "Verdict");
    amxc_var_set_packet_verdict_t(verdict_var, verdict);

    when_failed_trace(amxm_execute_function("self", CORE, "comm-config-verdict", &args, &ret),
                      exit, ERROR, "Could not send verdict");

    res = true;

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return res;
}

void socket_read_cb(UNUSED int fd, void* data) {
    const char* uri = NULL;
    socket_config_t* socket = (socket_config_t*) data;
    imtp_frame_t* frame = NULL;
    const imtp_tlv_t* packet_tlv = NULL;
    const imtp_tlv_t* verdict_tlv = NULL;
    packet_id_t packet;
    packet_verdict_t verdict;
    int ret = 0;
    int res = -1;

    when_null(socket, exit);

    ret = imtp_connection_read_frame(socket->accepted_conn, &frame);
    if(ret < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not read data, closing socket");
        amxo_connection_remove(socket_get_parser(), imtp_connection_get_fd(socket->accepted_conn));
        imtp_connection_delete(&socket->accepted_conn);
        amxd_object_set_bool(socket->common.object, "Connected", false);
        uri = object_da_string(socket->common.object, "URI");
        if(!uri) {
            SAH_TRACEZ_ERROR(ME, "Parameter URI can not be empty");
            set_status(&socket->common, INTERCEPTION_ERROR_MISCONFIG);
            goto exit;
        }
        res = imtp_connection_listen(&socket->listen_conn, uri, NULL);
        if(res != 0) {
            SAH_TRACEZ_ERROR(ME, "Could not create listen socket on URI [%s]", uri);
            set_status(&socket->common, INTERCEPTION_ERROR);
            goto exit;
        }

        res = amxo_connection_add(socket_get_parser(), imtp_connection_get_fd(socket->listen_conn),
                                  socket_accept_cb, uri, AMXO_CUSTOM, socket);
        if(res != 0) {
            SAH_TRACEZ_ERROR(ME, "Could not add parser to URI [%s]", uri);
            imtp_connection_delete(&socket->listen_conn);
            set_status(&socket->common, INTERCEPTION_ERROR);
            goto exit;
        }

        goto exit;
    }

    if(ret > 0) {
        SAH_TRACEZ_INFO(ME, "Not all data was read, more data still coming");
        goto exit;
    }

    packet_tlv = imtp_frame_get_first_tlv(frame, imtp_tlv_type_packet_id);
    if(!packet_tlv) {
        SAH_TRACEZ_ERROR(ME, "Data did not not contain Packet");
        goto exit;
    }

    memcpy(&packet, (uint8_t*) packet_tlv->value + packet_tlv->offset, packet_tlv->length);

    verdict_tlv = imtp_frame_get_first_tlv(frame, imtp_tlv_type_packet_verdict);
    if(!verdict_tlv) {
        SAH_TRACEZ_ERROR(ME, "Data did not not contain Packet Verdict");
        goto exit;
    }

    memcpy(&verdict, (uint8_t*) verdict_tlv->value + verdict_tlv->offset, verdict_tlv->length);

    SAH_TRACEZ_INFO(ME, "Received verdict [%d] for packet [%d][%d]", verdict.verdict, packet.group, packet.id);

    if(!socket_send_verdict(socket, &packet, &verdict)) {
        SAH_TRACEZ_ERROR(ME, "Could not apply verdict to packet");
    }

exit:
    imtp_frame_delete(&frame);
    return;
}

static bool socket_config_add_packetdata_msg(imtp_frame_t* frame, packet_data_t* data) {
    imtp_tlv_t* nl_data = NULL;
    imtp_tlv_t* payload = NULL;
    int retval = -1;
    bool ret = false;

    retval = imtp_tlv_new(&nl_data, imtp_tlv_type_packet_meta, sizeof(packet_data_t), data, 0, IMTP_TLV_COPY);
    when_failed(retval, exit);

    retval = imtp_tlv_new(&payload, imtp_tlv_type_packet_payload, data->payload_len, data->payload, 0, IMTP_TLV_COPY);
    when_failed(retval, exit);

    retval = imtp_frame_tlv_add(frame, nl_data);
    when_failed(retval, exit);

    retval = imtp_frame_tlv_add(frame, payload);
    when_failed(retval, exit);

    ret = true;

exit:
    if(!ret) {
        imtp_tlv_delete(&nl_data);
        imtp_tlv_delete(&payload);
    }
    return ret;
}

static bool socket_config_add_JSON_msg(imtp_frame_t* frame, amxc_var_t* data) {
    amxc_var_t json_var;
    imtp_tlv_t* json_tlv = NULL;
    const char* json = NULL;
    bool ret = false;
    int retval = -1;

    amxc_var_init(&json_var);

    retval = amxc_var_convert(&json_var, data, AMXC_VAR_ID_JSON);
    when_failed(retval, exit);

    json = amxc_var_get_const_jstring_t(&json_var);

    retval = imtp_tlv_new(&json_tlv, imtp_tlv_type_json, strlen(json), (void* ) json, 0, IMTP_TLV_COPY);
    when_failed(retval, exit);

    retval = imtp_frame_tlv_add(frame, json_tlv);
    when_failed(retval, exit);

    ret = true;

exit:
    if(!ret) {
        imtp_tlv_delete(&json_tlv);
    }
    amxc_var_clean(&json_var);
    return ret;
}

static bool socket_config_create_message(imtp_frame_t** frame, packet_id_t* packet) {
    imtp_tlv_t* packet_tlv = NULL;
    int retval = -1;
    bool ret = false;

    retval = imtp_frame_new(frame);
    when_failed(retval, exit);

    retval = imtp_tlv_new(&packet_tlv, imtp_tlv_type_packet_id, sizeof(packet_id_t), packet, 0, IMTP_TLV_COPY);
    when_failed(retval, exit);

    retval = imtp_frame_tlv_add(*frame, packet_tlv);
    when_failed(retval, exit);

    ret = true;

exit:
    if(!ret) {
        imtp_frame_delete(frame);
    }
    return ret;
}

bool socket_config_send(socket_config_t* socket, packet_id_t* packet, amxc_var_t* data) {
    imtp_frame_t* frame = NULL;
    bool ret = false;

    when_null(socket, exit);
    when_null(packet, exit);
    when_null(data, exit);

    if(!socket->accepted_conn) {
        goto exit;
    }

    ret = socket_config_create_message(&frame, packet);
    when_false(ret, exit);

    if(amxc_var_type_of(data) == AMXC_VAR_ID_PACKET_DATA) {
        ret = socket_config_add_packetdata_msg(frame, data->data.data);
    } else {
        ret = socket_config_add_JSON_msg(frame, data);
    }

    when_false(ret, exit);

    when_failed_trace(imtp_connection_write_frame(socket->accepted_conn, frame), exit, ERROR, "Cannot write imtp");

    ret = true;

exit:
    imtp_frame_delete(&frame);
    return ret;
}

static int socket_config_mod_send(UNUSED const char* function_name,
                                  amxc_var_t* args,
                                  UNUSED amxc_var_t* ret) {
    socket_config_t* socket = NULL;
    packet_id_t packet;
    amxc_var_t* data = NULL;
    int res = -1;

    when_null_trace(args, exit, ERROR, "Packet NULL");

    socket = socket_config_find(GET_CHAR(args, "Name"));
    when_null_trace(socket, exit, ERROR, "Could not find Socket Communication Config with name [%s]", GET_CHAR(args, "Name"));

    packet.id = GET_UINT32(args, "ID");
    packet.group = GET_UINT32(args, "Group");

    data = GET_ARG(args, "Data");
    when_false(socket_config_send(socket, &packet, data), exit);

    res = 0;

exit:
    return res;
}

void socket_accept_cb(UNUSED int fd, void* data) {
    socket_config_t* socket = (socket_config_t*) data;
    int fd_new = -1;
    int ret = 0;

    when_null(socket, exit);
    when_null(socket->listen_conn, exit);

    if(socket->accepted_conn) {
        SAH_TRACEZ_WARNING(ME, "Socket Communication Config already has accepted a connection");
        goto exit;
    }

    fd_new = imtp_connection_accept(socket->listen_conn);
    if(fd_new < 0) {
        goto exit;
    }

    socket->accepted_conn = imtp_connection_get_con(fd_new);
    if(!socket->accepted_conn) {
        SAH_TRACEZ_ERROR(ME, "Could not accept connection");
        goto exit;
    }

    ret = amxo_connection_add(socket_get_parser(), imtp_connection_get_fd(socket->accepted_conn),
                              socket_read_cb, NULL, AMXO_CUSTOM, socket);

    if(ret != 0) {
        SAH_TRACEZ_ERROR(ME, "Could not add parser to connection");
        imtp_connection_delete(&socket->accepted_conn);
        goto exit;
    }

    amxd_object_set_bool(socket->common.object, "Connected", true);
    amxo_connection_remove(socket_get_parser(), imtp_connection_get_fd(socket->listen_conn));
    imtp_connection_delete(&socket->listen_conn);

exit:
    return;
}

bool socket_config_activate(socket_config_t* socket) {
    const char* uri = NULL;
    int res = -1;
    bool enabled = false;
    bool ret = false;

    when_null(socket, exit);

    enabled = amxd_object_get_value(bool, socket->common.object, "Enable", NULL);

    if(enabled && socket_get_global_enable()) {
        if(socket->common.status != INTERCEPTION_ENABLED) {
            uri = object_da_string(socket->common.object, "URI");
            if(!uri) {
                SAH_TRACEZ_ERROR(ME, "Parameter URI can not be empty");
                set_status(&socket->common, INTERCEPTION_ERROR_MISCONFIG);
                goto exit;
            }

            res = imtp_connection_listen(&socket->listen_conn, uri, NULL);
            if(res != 0) {
                SAH_TRACEZ_ERROR(ME, "Could not create listen socket on URI [%s]", uri);
                set_status(&socket->common, INTERCEPTION_ERROR);
                goto exit;
            }

            res = amxo_connection_add(socket_get_parser(), imtp_connection_get_fd(socket->listen_conn),
                                      socket_accept_cb, uri, AMXO_CUSTOM, socket);
            if(res != 0) {
                SAH_TRACEZ_ERROR(ME, "Could not add parser to URI [%s]", uri);
                imtp_connection_delete(&socket->listen_conn);
                set_status(&socket->common, INTERCEPTION_ERROR);
                goto exit;
            }

            set_status(&socket->common, INTERCEPTION_ENABLED);
        } else {
            SAH_TRACEZ_NOTICE(ME, "Socket [%s] is already active", OBJECT_NAME(socket->common.object));
        }
    } else {
        SAH_TRACEZ_INFO(ME, "Socket [%s] is not active", OBJECT_NAME(socket->common.object));
    }

    ret = true;

exit:
    return ret;
}

bool socket_config_deactivate(socket_config_t* socket) {
    bool ret = false;

    when_null(socket, exit);

    if(socket->common.status == INTERCEPTION_ENABLED) {
        if(socket->accepted_conn) {
            amxo_connection_remove(socket_get_parser(), imtp_connection_get_fd(socket->accepted_conn));
            imtp_connection_delete(&socket->accepted_conn);
        }

        if(socket->listen_conn) {
            amxo_connection_remove(socket_get_parser(), imtp_connection_get_fd(socket->listen_conn));
            imtp_connection_delete(&socket->listen_conn);
        }
        amxd_object_set_bool(socket->common.object, "Connected", false);
        set_status(&socket->common, INTERCEPTION_DISABLED);
    } else {
        SAH_TRACEZ_INFO(ME, "Socket [%s] already inactive, continue", OBJECT_NAME(socket->common.object));
    }

    ret = true;

exit:
    return ret;
}

void socket_config_activate_deactivate_all(bool activate) {
    socket_config_t* socket = NULL;

    amxc_llist_for_each(it, &socket_configs) {
        socket = amxc_llist_it_get_data(it, socket_config_t, it);
        if(activate) {
            socket_config_activate(socket);
        } else {
            socket_config_deactivate(socket);
        }
    }
}

socket_config_t* socket_config_find(const char* name) {
    socket_config_t* socket = NULL;

    amxc_llist_for_each(it, &socket_configs) {
        socket = amxc_llist_it_get_data(it, socket_config_t, it);
        if(strcmp(OBJECT_NAME(socket->common.object), name) == 0) {
            return socket;
        }
    }

    return NULL;
}

socket_config_t* socket_config_create(amxd_object_t* object) {
    socket_config_t* socket = NULL;

    when_null(object, exit);

    socket = calloc(1, sizeof(socket_config_t));
    when_null(socket, exit);

    object->priv = socket;

    socket->common.object = object;

    amxc_llist_append(&socket_configs, &socket->it);

exit:
    return socket;
}

void socket_config_delete(socket_config_t* socket) {
    when_null(socket, exit);

    socket_config_deactivate(socket);
    amxc_llist_it_take(&socket->it);
    socket->common.object->priv = NULL;

    free(socket);
    socket = NULL;

exit:
    return;
}

void socket_config_init(void) {
    amxc_llist_init(&socket_configs);

    amxm_module_add_function(socket_get_module(), "send-packet", socket_config_mod_send);
}

void socket_config_cleanup(void) {
    amxc_llist_clean(&socket_configs, NULL);

    amxm_module_remove_function(socket_get_module(), "send-packet");
}
