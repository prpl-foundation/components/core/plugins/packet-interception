/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdlib.h>
#include <string.h>

#include "imtp_mock.h"
#include "mock.h"
#include "packet-interception/packet.h"

struct _imtp_connection {
    int fd;
};

int __wrap_imtp_connection_connect(imtp_connection_t** icon, char* from_uri, char* to_uri) {
    check_expected_ptr(icon);
    check_expected_ptr(from_uri);
    check_expected_ptr(to_uri);

    assert_non_null(icon);
    assert_non_null(to_uri);
    assert_non_null(from_uri);

    *icon = calloc(1, sizeof(imtp_connection_t));
    (*icon)->fd = mock_type(int);

    return mock_type(int);
}

int __wrap_imtp_connection_accept(imtp_connection_t* icon) {
    check_expected(icon);

    assert_non_null(icon);

    return mock_type(int);
}

int __wrap_imtp_connection_listen(imtp_connection_t** icon, const char* uri, UNUSED imtp_connection_accept_cb_t fn) {
    check_expected(icon);
    check_expected(uri);

    assert_non_null(icon);
    assert_non_null(uri);

    *icon = calloc(1, sizeof(imtp_connection_t));
    assert_non_null(icon);

    (*icon)->fd = mock_type(int);

    return mock_type(int);
}

void __wrap_imtp_connection_delete(imtp_connection_t** icon) {
    check_expected(icon);

    assert_non_null(icon);

    free(*icon);
    *icon = NULL;
}

imtp_connection_t* __wrap_imtp_connection_get_con(int fd) {
    imtp_connection_t* icon = NULL;

    check_expected(fd);

    icon = calloc(1, sizeof(imtp_connection_t));
    assert_non_null(icon);

    icon->fd = fd;

    return icon;
}

int __wrap_imtp_connection_read_frame(imtp_connection_t* icon, imtp_frame_t** frame) {
    imtp_tlv_t* tlv_packet = NULL;
    imtp_tlv_t* tlv_verdict = NULL;
    packet_id_t packet;
    packet_verdict_t verdict;

    check_expected(icon);
    check_expected(frame);

    assert_non_null(icon);
    assert_non_null(frame);

    packet.group = mock_type(int);
    packet.id = mock_type(int);

    verdict.verdict = 1;
    verdict.mark = mock_type(int);
    verdict.mark_mask = mock_type(int);
    verdict.connmark = 0;
    verdict.connmark_mask = 0;

    imtp_frame_new(frame);
    imtp_tlv_new(&tlv_packet, imtp_tlv_type_packet_id, sizeof(packet_id_t), &packet, 0, IMTP_TLV_COPY);
    imtp_frame_tlv_add(*frame, tlv_packet);
    imtp_tlv_new(&tlv_verdict, imtp_tlv_type_packet_verdict, sizeof(packet_verdict_t), &verdict, 0, IMTP_TLV_COPY);
    imtp_frame_tlv_add(*frame, tlv_verdict);

    return 0;
}

int __wrap_imtp_connection_write_frame(imtp_connection_t* icon, imtp_frame_t* frame) {
    check_expected_ptr(icon);
    check_expected_ptr(frame);

    return mock_type(int);
}