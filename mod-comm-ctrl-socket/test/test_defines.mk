MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include_priv )
MOCK_SRCDIR = $(realpath ../mocks/)

HEADERS = $(wildcard $(INCDIR)/$(TARGET)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c)  \
		  $(wildcard $(MOCK_SRCDIR)/*.c)

MOCK_WRAP = amxo_connection_add \
			amxo_connection_remove \
			imtp_connection_connect \
			imtp_connection_listen \
			imtp_connection_accept \
			imtp_connection_delete \
			imtp_connection_read_frame \
			imtp_connection_write_frame \
			imtp_connection_get_con

WRAP_FUNC=-Wl,--wrap=

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
		  --std=gnu99 -fPIC -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. -I../mocks\
		  -fkeep-inline-functions -fkeep-static-functions \
		   -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread -DUNIT_TEST \
		   -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) -lamxc -lamxp -lamxd -lamxb -lamxo -lamxj -lamxm \
		   -ldl -lpthread -lsahtrace -limtp -lpacket-interception

LDFLAGS += -g $(addprefix $(WRAP_FUNC),$(MOCK_WRAP))
