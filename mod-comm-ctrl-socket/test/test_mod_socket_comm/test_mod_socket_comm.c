/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "mod-comm-socket.h"
#include "socket.h"

#include "test_mod_socket_comm.h"
#include "mock.h"

#define SELF "packet-interception"
#define TEST_SOCKET     "dummy"
#define TEST_URI        "/tmp/dummy"

static int dummy_entry_verdict(UNUSED const char* const function_name, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    amxc_var_t* verdict_var = NULL;
    uint32_t id = 0;
    uint32_t group = 0;
    const packet_verdict_t* verdict = NULL;
    const char* comm_config = NULL;

    assert_non_null(args);

    amxc_var_dump(args, STDOUT_FILENO);

    id = GET_UINT32(args, "ID");
    group = GET_UINT32(args, "Group");
    comm_config = GET_CHAR(args, "Communication Config");
    verdict_var = GET_ARG(args, "Verdict");

    check_expected(id);
    check_expected(group);
    check_expected(comm_config);
    assert_non_null(verdict_var);

    verdict = amxc_var_get_const_packet_verdict_t(verdict_var);

    check_expected(verdict->verdict);
    check_expected(verdict->mark);
    check_expected(verdict->mark_mask);

    return 0;
}

static int dummy_mod_register(UNUSED const char* const function_name, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    const char* name = NULL;
    const char* module = NULL;
    const char* parser = NULL;

    assert_non_null(args);

    name = GET_CHAR(args, "Name");
    module = GET_CHAR(args, "Module");
    parser = GET_CHAR(args, "Parser");

    check_expected(name);
    check_expected(module);
    check_expected(parser);

    return 0;
}

static int dummy_mod_deregister(UNUSED const char* const function_name, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    const char* name = NULL;

    assert_non_null(args);

    name = GET_CHAR(args, "Name");

    check_expected(name);

    return 0;
}

int test_setup(UNUSED void** state) {
    amxm_shared_object_t* so = amxm_get_so("self");
    amxm_module_t* mod = NULL;

    mock_init();

    assert_int_equal(_socket_config_main(0, mock_get_dm(), mock_get_parser()), 0);

    handle_events();

    amxm_module_register(&mod, so, SELF);
    assert_int_equal(amxm_module_add_function(mod, "register-comm-config", dummy_mod_register), 0);
    assert_int_equal(amxm_module_add_function(mod, "deregister-comm-config", dummy_mod_deregister), 0);
    assert_int_equal(amxm_module_add_function(mod, "comm-config-verdict", dummy_entry_verdict), 0);
    socket_set_global_enable(true);

    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_int_equal(_socket_config_main(1, mock_get_dm(), mock_get_parser()), 0);

    handle_events();

    mock_cleanup();
    return 0;
}

void test_socket_create(UNUSED void** state) {
    amxd_object_t* object = NULL;
    amxd_trans_t trans;
    amxc_var_t params;

    amxc_var_init(&params);
    amxd_trans_init(&trans);

    expect_string(dummy_mod_register, name, TEST_SOCKET);
    expect_string(dummy_mod_register, module, "socket");
    expect_any(dummy_mod_register, parser);

    amxd_trans_select_pathf(&trans, "PacketInterception.CommunicationConfig.Socket");
    amxd_trans_add_inst(&trans, 0, TEST_SOCKET);
    amxd_trans_set_value(cstring_t, &trans, "URI", TEST_URI);
    assert_int_equal(amxd_trans_apply(&trans, mock_get_dm()), 0);
    handle_events();
    object = amxd_dm_findf(mock_get_dm(), "PacketInterception.CommunicationConfig.Socket.1");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void test_socket_delete(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    expect_string(dummy_mod_deregister, name, TEST_SOCKET);

    // Expect imtp accept connection to be removed.
    expect_any(__wrap_amxo_connection_remove, parser);
    expect_value(__wrap_amxo_connection_remove, fd, 50);

    expect_any(__wrap_imtp_connection_delete, icon);


    amxd_trans_select_pathf(&trans, "PacketInterception.CommunicationConfig.Socket.");
    amxd_trans_del_inst(&trans, 0, TEST_SOCKET);
    assert_int_equal(amxd_trans_apply(&trans, mock_get_dm()), 0);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_socket_enable(UNUSED void** state) {
    amxd_object_t* object = NULL;
    amxd_trans_t trans;
    amxc_var_t params;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    // Expect imtp listen connection to be added at test uri.
    expect_any(__wrap_imtp_connection_listen, icon);
    expect_string(__wrap_imtp_connection_listen, uri, TEST_URI);
    will_return(__wrap_imtp_connection_listen, 99);
    will_return(__wrap_imtp_connection_listen, 0);

    // Expect amxo connection to be added for listen socket.
    expect_any(__wrap_amxo_connection_add, parser);
    expect_value(__wrap_amxo_connection_add, fd, 99);
    expect_string(__wrap_amxo_connection_add, uri, TEST_URI);
    expect_value(__wrap_amxo_connection_add, type, AMXO_CUSTOM);
    expect_any(__wrap_amxo_connection_add, priv);

    // Enable the socket.
    object = amxd_dm_findf(mock_get_dm(), "PacketInterception.CommunicationConfig.Socket.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, mock_get_dm()), 0);
    handle_events();

    // Assert that datamodel parameter connected is not yet set.
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");
    assert_false(GETP_BOOL(&params, "Connected"));

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void test_socket_connect(UNUSED void** state) {
    amxd_object_t* object = NULL;
    amxc_var_t params;

    amxc_var_init(&params);

    // Expect imtp connection to be accepted and amxo connection added.
    expect_any(__wrap_imtp_connection_accept, icon);
    will_return(__wrap_imtp_connection_accept, 50);

    expect_value(__wrap_imtp_connection_get_con, fd, 50);

    expect_any(__wrap_amxo_connection_add, parser);
    expect_value(__wrap_amxo_connection_add, fd, 50);
    expect_value(__wrap_amxo_connection_add, uri, NULL);
    expect_value(__wrap_amxo_connection_add, type, AMXO_CUSTOM);
    expect_any(__wrap_amxo_connection_add, priv);

    // Expect imtp listen connection to be removed.
    expect_any(__wrap_amxo_connection_remove, parser);
    expect_value(__wrap_amxo_connection_remove, fd, 99);
    expect_any(__wrap_imtp_connection_delete, icon);

    // Mock a process connecting to the listen socket.
    object = amxd_dm_findf(mock_get_dm(), "PacketInterception.CommunicationConfig.Socket.1");
    assert_non_null(object);
    socket_accept_cb(99, object->priv);

    // Assert that datamodel parameter connected is set.
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_true(GETP_BOOL(&params, "Connected"));

    // Check that a second connect will not trigger anything.
    socket_accept_cb(99, object->priv);

    amxc_var_clean(&params);
}

void test_socket_send(UNUSED void** state) {
    amxc_var_t* data = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    // Create packet to send.
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Name", TEST_SOCKET);
    amxc_var_add_key(uint32_t, &args, "ID", 5);
    amxc_var_add_key(uint32_t, &args, "Group", 6);
    data = amxc_var_add_new_key(&args, "Data");
    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, data, "INFO", "Hello World");

    // Expect a write to the imtp connection.
    expect_any(__wrap_imtp_connection_write_frame, icon);
    expect_any(__wrap_imtp_connection_write_frame, frame);
    will_return(__wrap_imtp_connection_write_frame, 0);

    // Send out the mock packet and data.
    assert_int_equal(amxm_execute_function("self", "socket", "send-packet", &args, &ret), 0);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_socket_read(UNUSED void** state) {
    amxd_object_t* object = NULL;

    // Expect a read from the imtp connection
    expect_any(__wrap_imtp_connection_read_frame, icon);
    expect_any(__wrap_imtp_connection_read_frame, frame);
    will_return(__wrap_imtp_connection_read_frame, 6);
    will_return(__wrap_imtp_connection_read_frame, 5);
    will_return(__wrap_imtp_connection_read_frame, 1);
    will_return(__wrap_imtp_connection_read_frame, 1);

    // Expect the response cb will be called
    expect_value(dummy_entry_verdict, id, 5);
    expect_value(dummy_entry_verdict, group, 6);
    expect_string(dummy_entry_verdict, comm_config, TEST_SOCKET);
    expect_value(dummy_entry_verdict, verdict->verdict, 1);
    expect_value(dummy_entry_verdict, verdict->mark, 1);
    expect_value(dummy_entry_verdict, verdict->mark_mask, 1);

    // Mock a read from the socket
    object = amxd_dm_findf(mock_get_dm(), "PacketInterception.CommunicationConfig.Socket.1");
    assert_non_null(object);
    socket_read_cb(50, object->priv);
}
