/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

%populate {
    object PacketInterception {
        parameter Enable = 0;
        
        object Condition {
            instance add(Alias=DNS) {
                parameter Protocol = UDP;
                parameter DestPort = 53;
            }

            instance add(Alias=HTTP) {
                parameter Protocol = TCP;
                parameter DestPort = 80;
            }

            instance add(Alias=HTTPS) {
                parameter Protocol = TCP;
                parameter DestPort = 443;
            }

            instance add(Alias=QUIC) {
                parameter Protocol = UDP;
                parameter DestPort = 443;
            }

            instance add(Alias=local_traffic) {
                parameter IPVersion = 4;
                parameter SourceIP = "127.0.0.1";
            }
        }

        object PacketHandler {
            instance add(Alias=default_handler) {
                parameter Enable = 1;
            }
        }

        object CommunicationConfig {
            parameter SupportedParsers = "";
            
            object Socket {
                instance add(Alias=dns_socket) {
                    parameter Enable = 1;
                    parameter Parser = "";
                    parameter URI = "${socket_path}/dns.sock";
                }

                instance add(Alias=http_socket) {
                    parameter Enable = 1;
                    parameter Parser = "";
                    parameter URI = "${socket_path}/http.sock";
                }

                instance add(Alias=https_socket) {
                    parameter Enable = 1;
                    parameter Parser = "";
                    parameter URI = "${socket_path}/https.sock";
                }

                instance add(Alias=quic_socket) {
                    parameter Enable = 1;
                    parameter Parser = "";
                    parameter URI = "${socket_path}/quic.sock";
                }
            }
        }

        object Interception {
            instance add(Alias=Forward) {
                parameter Enable = 1;
                parameter TrafficRoute = FORWARD;

                object Intercept {
                    instance add(Alias=DNS) {
                        parameter Enable = 1;
                        parameter Condition = DNS;
                        parameter NumberOfPackets = 1;
                        parameter PacketHandler = default_handler;

                        object CommunicationConfig {
                            instance add(Alias=dns_socket) {
                                parameter CommunicationConfig=dns_socket;
                            }
                        }
                    }

                    instance add(Alias=HTTP) {
                        parameter Enable = 1;
                        parameter Condition = HTTP;
                        parameter NumberOfPackets = 4;
                        parameter PacketHandler = default_handler;
                        parameter Direction = both;

                        object CommunicationConfig {
                            instance add(Alias=http_socket) {
                                parameter CommunicationConfig=http_socket;
                            }
                        }
                    }

                    instance add(Alias=HTTPS) {
                        parameter Enable = 1;
                        parameter Condition = HTTPS;
                        parameter NumberOfPackets = 6;
                        parameter PacketHandler = default_handler;
                        parameter Direction = both;

                        object CommunicationConfig {
                            instance add(Alias=https_socket) {
                                parameter CommunicationConfig=https_socket;
                            }
                        }
                    }

                    instance add(Alias=QUIC) {
                        parameter Enable = 1;
                        parameter Condition = QUIC;
                        parameter NumberOfPackets = 1;
                        parameter PacketHandler = default_handler;

                        object CommunicationConfig {
                            instance add(Alias=quic_socket) {
                                parameter CommunicationConfig=quic_socket;
                            }
                        }
                    }
                }
            }

            instance add(Alias=Input) {
                parameter Enable = 1;
                parameter TrafficRoute = INPUT;

                object Bypass {
                    instance add(Alias=local_traffic) {
                        parameter Enable = 1;
                        parameter Condition = local_traffic;
                    }
                }

                object Intercept {
                    instance add(Alias=DNS) {
                        parameter Enable = 1;
                        parameter Condition = DNS;
                        parameter NumberOfPackets = 1;
                        parameter PacketHandler = default_handler;

                        object CommunicationConfig {
                            instance add(Alias=dns_socket) {
                                parameter CommunicationConfig=dns_socket;
                            }
                        }
                    }

                    instance add(Alias=HTTP) {
                        parameter Enable = 1;
                        parameter Condition = HTTP;
                        parameter NumberOfPackets = 3;
                        parameter PacketHandler = default_handler;

                        object CommunicationConfig {
                            instance add(Alias=http_socket) {
                                parameter CommunicationConfig=http_socket;
                            }
                        }
                    }
                }
            }

            instance add(Alias=Output) {
                parameter Enable = 1;
                parameter TrafficRoute = OUTPUT;

                object Bypass {
                    instance add(Alias=local_traffic) {
                        parameter Enable = 1;
                        parameter Condition = local_traffic;
                        parameter Direction = reply;
                    }
                }

                object Intercept {
                    instance add(Alias=HTTP) {
                        parameter Enable = 1;
                        parameter Condition = HTTP;
                        parameter NumberOfPackets = 1;
                        parameter PacketHandler = default_handler;
                        parameter Direction = reply;

                        object CommunicationConfig {
                            instance add(Alias=http_socket) {
                                parameter CommunicationConfig=http_socket;
                            }
                        }
                    }
                }
            }
        }
    }
}
