/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <debug/sahtrace.h>

#include <packet-interception/variant_packet_verdict.h>

#include "comm-config/comm-config.h"
#include "comm-config/comm-config-module.h"

#define ME "comm_config"

static int comm_config_verdict(UNUSED const char* const function_name, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    communication_config_t* config = NULL;
    packet_id_t packet;
    const packet_verdict_t* verdict = NULL;
    int rv = -1;

    when_null(args, exit);

    config = comm_config_find(GET_CHAR(args, "Communication Config"));
    packet.id = GET_UINT32(args, "ID");
    packet.group = GET_UINT32(args, "Group");
    verdict = amxc_var_get_const_packet_verdict_t(GET_ARG(args, "Verdict"));

    when_null(config, exit);
    when_null(verdict, exit);

    comm_config_process_verdict(config, &packet, verdict);

    rv = 0;

exit:
    return rv;
}

static int comm_config_mod_register(UNUSED const char* const function_name, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    const char* config_name = NULL;
    const char* parser = NULL;
    const char* module = NULL;
    int rv = -1;

    when_null(args, exit);

    config_name = GET_CHAR(args, "Name");
    parser = GET_CHAR(args, "Parser");
    module = GET_CHAR(args, "Module");

    when_str_empty(config_name, exit);
    when_str_empty(module, exit);

    comm_config_register(config_name, parser, module);

    rv = 0;

exit:
    return rv;
}

static int comm_config_mod_deregister(UNUSED const char* const function_name, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    const char* config_name = NULL;
    int rv = -1;

    when_null(args, exit);

    config_name = GET_CHAR(args, "Name");

    when_str_empty(config_name, exit);

    comm_config_deregister(config_name);

    rv = 0;

exit:
    return rv;
}

void comm_config_module_init(void) {
    amxm_module_t* mod = packetinterception_get_module();

    when_failed(amxm_module_add_function(mod, "register-comm-config", comm_config_mod_register), exit);
    when_failed(amxm_module_add_function(mod, "deregister-comm-config", comm_config_mod_deregister), exit);
    when_failed(amxm_module_add_function(mod, "comm-config-verdict", comm_config_verdict), exit);

exit:
    return;
}

void comm_config_module_cleanup(void) {
    amxm_module_t* mod = packetinterception_get_module();

    amxm_module_remove_function(mod, "register-comm-config");
    amxm_module_remove_function(mod, "deregister-comm-config");
    amxm_module_remove_function(mod, "comm-config-verdict");
}
