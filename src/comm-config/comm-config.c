/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "comm-config/comm-config.h"
#include "comm-config/comm-config-module.h"
#include "comm-config/parser.h"
#include "interception/ic_comm-config.h"

#include <packet-interception/packet.h>
#include <packet-interception/variant_packet_data.h>

#define ME "comm_config"

static amxc_llist_t comm_configs;

bool comm_config_process_verdict(const communication_config_t* config, const packet_id_t* packet, const packet_verdict_t* verdict) {
    ic_condition_t* ic_cond = NULL;
    ic_comm_config_t* ic_comm = NULL;
    bool config_found = false;
    bool ret = false;

    when_null(packet, exit);
    when_null(verdict, exit);
    when_null(config, exit);

    ic_cond = ic_condition_find_by_qnum(packet->group);
    if(!ic_cond) {
        SAH_TRACEZ_ERROR(ME, "Could not find nfqueue with qnum [%d]", packet->group);
        goto exit;
    }

    amxc_llist_for_each(it, &ic_cond->comm_configs) {
        ic_comm = amxc_llist_it_get_data(it, ic_comm_config_t, it);
        if(!ic_comm) {
            continue;
        }

        if(ic_comm->comm_config == config) {
            config_found = true;
            break;
        }
    }

    if(!config_found) {
        SAH_TRACEZ_INFO(ME, "Communication Config [%s] can not decide for packets of Intercept Condition [%s]",
                        amxc_string_get(&config->name, 0), OBJECT_NAME(ic_cond->common.object));
        goto exit;
    }

    ret = packet_handler_verdict_cb(ic_cond->handler, ic_cond->nfqueue, packet, verdict, ic_cond->nr_of_packets);

exit:
    return ret;
}

static bool comm_config_send_packet(communication_config_t* config, packet_id_t* packet, amxc_var_t* data) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* data_var = NULL;
    bool res = false;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Name", amxc_string_get(&config->name, 0));
    amxc_var_add_key(uint32_t, &args, "ID", packet->id);
    amxc_var_add_key(uint16_t, &args, "Group", packet->group);
    data_var = amxc_var_add_new_key(&args, "Data");
    amxc_var_move(data_var, data);

    when_failed_trace(amxm_execute_function("self", amxc_string_get(&config->module, 0), "send-packet", &args, &ret),
                      exit, ERROR, "Could not send packet");

    res = true;

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return res;
}

bool comm_config_send_packet_next(ic_condition_t* ic_cond, packet_id_t* packet, packet_data_t* data, uint64_t* priority) {
    amxc_var_t dest;
    amxc_var_t var_packet_data;
    ic_comm_config_t* ic_comm = NULL;
    ic_comm_config_t* ic_comm_temp = NULL;
    communication_config_t* config = NULL;
    const char* parser = NULL;
    bool ret = false;

    amxc_var_init(&var_packet_data);
    amxc_var_init(&dest);

    when_null(ic_cond, exit);
    when_null(data, exit);

    amxc_llist_for_each(it, &ic_cond->comm_configs) {
        ic_comm_temp = amxc_llist_it_get_data(it, ic_comm_config_t, it);

        if((!ic_comm || (ic_comm->priority > ic_comm_temp->priority)) && (ic_comm_temp->priority > *priority)) {
            ic_comm = ic_comm_temp;
        }
    }

    when_null(ic_comm, exit);
    config = ic_comm->comm_config;
    *priority = ic_comm->priority;

    amxc_var_set_packet_data_t(&var_packet_data, data);
    parser = amxc_string_get(&config->parser, 0);
    when_false(parser_parse(parser, OBJECT_NAME(ic_cond->common.object), &dest, &var_packet_data), exit);

    ret = comm_config_send_packet(config, packet, &dest);

exit:
    amxc_var_clean(&var_packet_data);
    amxc_var_clean(&dest);
    return ret;
}

communication_config_t* comm_config_find(const char* name) {
    communication_config_t* config = NULL;

    amxc_llist_for_each(it, &comm_configs) {
        config = amxc_llist_it_get_data(it, communication_config_t, it);
        if(strcmp(name, amxc_string_get(&config->name, 0)) == 0) {
            return config;
        }
    }

    return NULL;
}

static communication_config_t* comm_config_create(const char* name) {
    communication_config_t* config = NULL;

    config = calloc(1, sizeof(communication_config_t));
    when_null_trace(config, exit, ERROR, "Alloc failed");

    amxc_string_init(&config->name, 64);
    amxc_string_init(&config->parser, 64);
    amxc_string_init(&config->module, 64);

    amxc_string_set(&config->name, name);

    amxc_llist_append(&comm_configs, &config->it);

exit:
    return config;
}

static void comm_config_delete(communication_config_t* config) {
    amxc_llist_it_take(&config->it);

    amxc_string_clean(&config->name);
    amxc_string_clean(&config->parser);
    amxc_string_clean(&config->module);

    free(config);
    config = NULL;
}

static void comm_config_delete_it(amxc_llist_it_t* it) {
    communication_config_t* config = amxc_llist_it_get_data(it, communication_config_t, it);
    comm_config_delete(config);
}

bool comm_config_register(const char* name, const char* parser, const char* module) {
    communication_config_t* config = NULL;
    bool ret = false;

    when_str_empty_trace(name, exit, ERROR, "Name NULL or empty");
    when_str_empty_trace(module, exit, ERROR, "Module name NULL or empty");

    config = comm_config_find(name);
    if(!config) {
        SAH_TRACEZ_INFO(ME, "Communication Config [%s] is being created", name);
        config = comm_config_create(name);
        when_null_trace(config, exit, ERROR, "Could not create Communication Config [%s]", name);
    }

    amxc_string_set(&config->parser, parser);
    amxc_string_set(&config->module, module);

    ret = true;

exit:
    return ret;
}

bool comm_config_deregister(const char* name) {
    communication_config_t* config = NULL;
    bool ret = false;

    when_str_empty_trace(name, exit, ERROR, "Name NULL or empty");

    config = comm_config_find(name);
    when_null_trace(config, exit, INFO, "Could not find Communication Config [%s]", name);

    comm_config_delete(config);

    ret = true;

exit:
    return ret;
}

void comm_config_init(void) {
    amxc_llist_init(&comm_configs);
    comm_config_module_init();
}

void comm_config_cleanup(void) {
    comm_config_module_cleanup();
    amxc_llist_clean(&comm_configs, comm_config_delete_it);
}
