/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "packet-interception.h"

#include "amxm/amxm.h"

#include "comm-config/parser.h"

#define ME "parser"

bool parser_parse(const char* parser, const char* interception, amxc_var_t* dest, amxc_var_t* data) {
    bool ret = false;
    int res = -1;

    when_null(data, exit);
    when_null(dest, exit);
    when_null(parser, exit);

    if(STRING_EMPTY(parser)) {
        amxc_var_copy(dest, data);
    } else {
        if(strcmp(interception, "Flowmonitor") == 0) {
            res = amxm_execute_function(parser, parser, "flow-parser", data, dest);
        } else {
            res = amxm_execute_function(parser, parser, "packet-parser", data, dest);
        }
        when_failed_trace(res, exit, INFO, "Could not parse");
    }

    ret = true;

exit:
    return ret;
}

void parser_init(void) {
    amxd_dm_t* dm = packetinterception_get_dm();
    amxd_object_t* comm_config = amxd_dm_findf(dm, "PacketInterception.CommunicationConfig");
    const amxc_var_t* parsers = amxd_object_get_param_value(comm_config, "SupportedParsers");
    amxc_var_t lparsers;
    amxm_shared_object_t* so = NULL;
    amxc_string_t mod_path;
    int retval;

    amxc_var_init(&lparsers);
    amxc_string_init(&mod_path, 0);

    amxc_var_convert(&lparsers, parsers, AMXC_VAR_ID_LIST);
    amxc_var_for_each(parser, &lparsers) {
        const char* name = GETP_CHAR(parser, NULL);
        amxc_string_setf(&mod_path, "%s/mod-parser-%s.so",
                         packetinterception_get_parser_module_dir("/usr/lib/amx/packet-interception/modules/parser"), name);
        SAH_TRACEZ_INFO(ME, "loading parser [%s] (file = [%s])", name, amxc_string_get(&mod_path, 0));
        retval = amxm_so_open(&so, name, amxc_string_get(&mod_path, 0));
        SAH_TRACEZ_INFO(ME, "Loading parser [%s] returned [%d]", name, retval);
        when_false(retval == 0, exit);
    }

exit:
    amxc_string_clean(&mod_path);
    amxc_var_clean(&lparsers);
}
