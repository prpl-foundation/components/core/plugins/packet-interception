/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>

#include "condition/condition.h"

#define ME "condition"

static amxc_llist_t condition_llist;
static bool cond_init = false;

bool condition_implement(condition_t* condition, fw_folder_t* folder, bool reply) {
    fw_rule_t* r = NULL;
    fw_rule_t* s = NULL;
    bool res = false;

    r = fw_folder_fetch_default_rule(folder);
    if(r == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to fetch rule");
        goto exit;
    }

    if(!amxc_string_is_empty(&condition->source)) {
        if(reply == false) {
            fw_rule_set_source(r, amxc_string_get(&condition->source, 0));
        } else {
            fw_rule_set_destination(r, amxc_string_get(&condition->source, 0));
        }
    }

    if(!amxc_string_is_empty(&condition->destination)) {
        if(reply == false) {
            fw_rule_set_destination(r, amxc_string_get(&condition->destination, 0));
        } else {
            fw_rule_set_source(r, amxc_string_get(&condition->destination, 0));
        }
    }

    if(!amxc_string_is_empty(&condition->connstates)) {
        fw_rule_set_conntrack_state(r, amxc_string_get(&condition->connstates, 0));
    }

    fw_folder_push_rule(folder, r);

    if(!amxc_string_is_empty(&condition->source_mac) && (reply == false)) {
        fw_folder_set_feature(folder, FW_FEATURE_SOURCE_MAC);
        s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_MAC);
        if(s == NULL) {
            SAH_TRACEZ_ERROR(ME, "Failed to fetch feature rule");
            goto exit;
        }
        fw_rule_set_source_mac_address(s, amxc_string_get(&condition->source_mac, 0));
        fw_folder_push_rule(folder, s);
    } else {
        fw_folder_unset_feature(folder, FW_FEATURE_SOURCE_MAC);
    }


    if(condition->nr_of_protocols > 0) {
        fw_folder_set_feature(folder, FW_FEATURE_PROTOCOL);

        for(int i = 0; i < condition->nr_of_protocols; i++) {
            s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_PROTOCOL);
            if(s == NULL) {
                SAH_TRACEZ_ERROR(ME, "Failed to fetch feature rule");
                goto exit;
            }
            fw_rule_set_protocol(s, condition->protocols[i]);
            fw_folder_push_rule(folder, s);
        }
    } else {
        fw_folder_unset_feature(folder, FW_FEATURE_PROTOCOL);
    }

    if(condition->nr_of_src_ports > 0) {
        if(reply == false) {
            fw_folder_set_feature(folder, FW_FEATURE_SOURCE_PORT);

            for(int i = 0; i < condition->nr_of_src_ports; i++) {
                s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_PORT);
                if(s == NULL) {
                    SAH_TRACEZ_ERROR(ME, "Failed to fetch feature rule");
                    goto exit;
                }
                fw_rule_set_source_port(s, condition->src_ports[i]);
                fw_folder_push_rule(folder, s);
            }
        } else {
            fw_folder_set_feature(folder, FW_FEATURE_DESTINATION_PORT);

            for(int i = 0; i < condition->nr_of_src_ports; i++) {
                s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_DESTINATION_PORT);
                if(s == NULL) {
                    SAH_TRACEZ_ERROR(ME, "Failed to fetch feature rule");
                    goto exit;
                }
                fw_rule_set_destination_port(s, condition->src_ports[i]);
                fw_folder_push_rule(folder, s);
            }
        }
    }

    if(condition->nr_of_dst_ports > 0) {
        if(reply == false) {
            fw_folder_set_feature(folder, FW_FEATURE_DESTINATION_PORT);

            for(int i = 0; i < condition->nr_of_dst_ports; i++) {
                s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_DESTINATION_PORT);
                if(s == NULL) {
                    SAH_TRACEZ_ERROR(ME, "Failed to fetch feature rule");
                    goto exit;
                }
                fw_rule_set_destination_port(s, condition->dst_ports[i]);
                fw_folder_push_rule(folder, s);
            }
        } else {
            fw_folder_set_feature(folder, FW_FEATURE_SOURCE_PORT);

            for(int i = 0; i < condition->nr_of_dst_ports; i++) {
                s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_PORT);
                if(s == NULL) {
                    SAH_TRACEZ_ERROR(ME, "Failed to fetch feature rule");
                    goto exit;
                }
                fw_rule_set_source_port(s, condition->dst_ports[i]);
                fw_folder_push_rule(folder, s);
            }
        }
    }

    res = true;

exit:
    return res;
}

condition_t* condition_find(const char* name) {
    condition_t* condition = NULL;

    when_null(name, exit);

    amxc_llist_for_each(it, &condition_llist) {
        condition = amxc_llist_it_get_data(it, condition_t, it);
        if(strcmp(name, OBJECT_NAME(condition->object)) == 0) {
            return condition;
        }
    }

exit:
    return NULL;
}

condition_t* condition_create(amxd_object_t* object) {
    condition_t* condition = NULL;

    when_null(object, exit);

    condition = calloc(1, sizeof(condition_t));
    when_null(condition, exit);

    if(!cond_init) {
        amxc_llist_init(&condition_llist);
        cond_init = true;
    }

    amxc_string_init(&condition->source_mac, 64);
    amxc_string_init(&condition->connstates, 64);

    amxc_string_init(&condition->source, 64);
    amxc_string_init(&condition->destination, 64);

    condition->object = object;
    amxc_llist_append(&condition_llist, &condition->it);

    object->priv = condition;

exit:
    return condition;
}

void condition_delete(condition_t* condition) {
    SAH_TRACEZ_NOTICE(ME, "Delete Condition [%s]", OBJECT_NAME(condition->object));

    amxc_llist_it_take(&condition->it);

    amxc_string_clean(&condition->source_mac);
    amxc_string_clean(&condition->connstates);

    amxc_string_clean(&condition->source);
    amxc_string_clean(&condition->destination);

    free(condition);
    condition = NULL;
}
