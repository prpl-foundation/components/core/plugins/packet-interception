/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <netdb.h>

#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>

#include "condition/condition_utils.h"

#define ME "condition"

bool condition_set_mac(condition_t* condition, const char* mac_txt, bool source) {
    bool retval = false;
    amxc_string_t* mac = NULL;

    when_null(condition, exit);

    if(source) {
        mac = &condition->source_mac;
    } else {
        // Iptables doesn't support destination mac yet.
        goto exit;
    }

    if(STRING_EMPTY(mac_txt)) {
        amxc_string_reset(mac);
    } else {
        amxc_string_set(mac, mac_txt);
    }

    retval = true;

exit:
    return retval;
}

bool condition_set_ipversion(condition_t* condition, int32_t ipversion) {
    bool retval = false;

    when_null(condition, exit);

    condition->ipversion = ipversion;
    retval = true;

exit:
    return retval;
}

bool condition_set_address(condition_t* condition, const char* ip_address, bool source) {
    bool retval = false;
    amxc_string_t* address = NULL;

    when_null(condition, exit);

    if(source) {
        address = &condition->source;
    } else {
        address = &condition->destination;
    }

    if(STRING_EMPTY(ip_address)) {
        amxc_string_reset(address);
    } else {
        amxc_string_set(address, ip_address);
    }

    retval = true;

exit:
    return retval;
}

bool condition_set_protocols(condition_t* condition, const char* protocols) {
    amxc_var_t var_list;
    amxc_string_t var_string;
    const amxc_llist_t* list = NULL;
    int i = 0;
    bool retval = false;

    amxc_var_init(&var_list);
    amxc_string_init(&var_string, 0);

    when_null(condition, exit);
    when_str_empty(protocols, cleanup);

    when_false(amxc_string_set(&var_string, protocols) > 0, cleanup);
    when_false(amxc_string_to_lower(&var_string) == 0, cleanup); // musl libc getprotobyname only accepts lower case names
    when_false(amxc_string_csv_to_var(&var_string, &var_list, NULL) == AMXC_STRING_SPLIT_OK, cleanup);
    list = amxc_var_constcast(amxc_llist_t, &var_list);
    when_null(list, cleanup);

    if(amxc_llist_size(list) > MAX_NR_OF_PROTOCOLS) {
        SAH_TRACEZ_ERROR(ME, "Too many protocols[%s], the limit is [%d]", protocols, MAX_NR_OF_PROTOCOLS);
        goto cleanup;
    }

    amxc_var_for_each(var, (&var_list)) {
        int32_t protocol = amxc_var_dyncast(int32_t, var);
        const char* protocol_name = amxc_var_constcast(cstring_t, var);
        struct protoent* ent = NULL;

        if(protocol > 0) {
            ent = getprotobynumber(protocol);
        } else if(STRING_EMPTY(protocol_name) == false) {
            ent = getprotobyname(protocol_name);
        }

        if(ent == NULL) {
            SAH_TRACEZ_ERROR(ME, "Failed to get protocol number of [%s]", protocol_name);
            i = 0; // this makes sure that any previous set protocol is erased
            goto cleanup;
        }

        condition->protocols[i] = ent->p_proto;
        i++;
    }

cleanup:
    condition->nr_of_protocols = i;

    if(i > 0) {
        retval = true;
    }

    for(; i < MAX_NR_OF_PROTOCOLS; i++) {
        condition->protocols[i] = 0;
    }

exit:
    amxc_var_clean(&var_list);
    amxc_string_clean(&var_string);
    return retval;
}

bool condition_set_ports(condition_t* condition, const char* ports_str, bool source) {
    amxc_var_t var_list;
    amxc_string_t var_string;
    const amxc_llist_t* list = NULL;
    int* ports = NULL;
    int* nr_of_ports = 0;
    int i = 0;
    bool retval = false;

    amxc_var_init(&var_list);
    amxc_string_init(&var_string, 0);

    when_null(condition, exit);

    if(source) {
        ports = condition->src_ports;
        nr_of_ports = &condition->nr_of_src_ports;
    } else {
        ports = condition->dst_ports;
        nr_of_ports = &condition->nr_of_dst_ports;
    }

    when_str_empty(ports_str, cleanup);

    when_false(amxc_string_set(&var_string, ports_str) > 0, cleanup);
    when_false(amxc_string_csv_to_var(&var_string, &var_list, NULL) == AMXC_STRING_SPLIT_OK, cleanup);
    list = amxc_var_constcast(amxc_llist_t, &var_list);
    when_null(list, cleanup);

    if(amxc_llist_size(list) > MAX_NR_OF_PORTS) {
        SAH_TRACEZ_ERROR(ME, "Too many ports[%s], the limit is [%d]", ports_str, MAX_NR_OF_PORTS);
        goto cleanup;
    }

    amxc_var_for_each(var, (&var_list)) {
        int32_t port = amxc_var_dyncast(int32_t, var);

        if(port <= 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to get port number");
            i = 0; // this makes sure that any previous set port is erased
            goto cleanup;
        }

        ports[i] = port;
        i++;
    }

cleanup:
    *nr_of_ports = i;

    if(i > 0) {
        retval = true;
    }

    for(; i < MAX_NR_OF_PORTS; i++) {
        ports[i] = 0;
    }

exit:
    amxc_var_clean(&var_list);
    amxc_string_clean(&var_string);
    return retval;
}

static bool condition_valid_connstate(const char* connstate) {
    bool retval = false;

    when_null(connstate, exit);

    if((strcmp(connstate, "INVALID") == 0) || (strcmp(connstate, "NEW") == 0) || (strcmp(connstate, "ESTABLISHED") == 0)
       || (strcmp(connstate, "RELATED") == 0) || (strcmp(connstate, "UNTRACKED") == 0) || (strcmp(connstate, "SNAT") == 0)
       || (strcmp(connstate, "DNAT") == 0)) {

        retval = true;
    }

exit:
    return retval;
}

bool condition_check_connstates(const char* connstates) {
    amxc_var_t var_list;
    amxc_string_t var_string;
    const amxc_llist_t* list = NULL;
    bool retval = false;

    amxc_var_init(&var_list);
    amxc_string_init(&var_string, 0);

    when_null(connstates, exit);

    when_false(amxc_string_set(&var_string, connstates) > 0, exit);
    when_false(amxc_string_csv_to_var(&var_string, &var_list, NULL) == AMXC_STRING_SPLIT_OK, exit);
    list = amxc_var_constcast(amxc_llist_t, &var_list);
    when_null(list, exit);

    amxc_var_for_each(var, (&var_list)) {
        const char* connstate = amxc_var_constcast(cstring_t, var);
        if(!condition_valid_connstate(connstate)) {
            goto exit;
        }
    }

    retval = true;

exit:
    amxc_var_clean(&var_list);
    amxc_string_clean(&var_string);
    return retval;
}

bool condition_set_connstates(condition_t* condition, const char* connstates) {
    bool retval = false;

    when_null(condition, exit);

    if(STRING_EMPTY(connstates)) {
        amxc_string_reset(&condition->connstates);
    } else {
        amxc_string_set(&condition->connstates, connstates);
    }

    retval = true;

exit:
    return retval;
}
