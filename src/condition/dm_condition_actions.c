/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include "ipat/ipat_text.h"

#include "condition/condition.h"
#include "condition/condition_utils.h"
#include "condition/dm_condition.h"

#define ME "condition"

amxd_status_t _cond_check_IP(amxd_object_t* object, UNUSED amxd_param_t* param,
                             amxd_action_t reason, const amxc_var_t* const args,
                             UNUSED amxc_var_t* const retval, UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* ip_address = NULL;
    int ipversion = 0;
    int ret = 0;

    when_true_status(reason != action_param_validate,
                     exit,
                     status = amxd_status_function_not_implemented);

    ip_address = amxc_var_constcast(cstring_t, args);
    if(STRING_EMPTY(ip_address)) {
        status = amxd_status_ok;
        goto exit;
    }

    ipversion = amxd_object_get_value(int32_t, object, "IPVersion", NULL);

    ret = ipat_text_valid(ip_address);
    if(ret != 1) {
        SAH_TRACEZ_WARNING(ME, "IP Address [%s] is not valid", ip_address);
        goto exit;
    }
    ret = ipat_text_family(ip_address);
    if((ret <= 0) ||
       ((ipversion == 4) && (ipat_text_family(ip_address) != AF_INET)) ||
       ((ipversion == 6) && (ipat_text_family(ip_address) != AF_INET6))) {
        SAH_TRACEZ_WARNING(ME, "IP Address [%s] is not of family [%d]", ip_address, ipversion);
        goto exit;
    }

    status = amxd_status_ok;

exit:
    return status;
}

amxd_status_t _cond_list_check_protocol(UNUSED amxd_object_t* object, UNUSED amxd_param_t* param,
                                        amxd_action_t reason, const amxc_var_t* const args,
                                        UNUSED amxc_var_t* const retval, UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* protocols = NULL;
    condition_t dummy;

    when_true_status(reason != action_param_validate,
                     exit,
                     status = amxd_status_function_not_implemented);

    protocols = amxc_var_constcast(cstring_t, args);
    if(STRING_EMPTY(protocols)) {
        status = amxd_status_ok;
        goto exit;
    }

    memset(&dummy, 0, sizeof(condition_t));

    if(condition_set_protocols(&dummy, protocols)) {
        status = amxd_status_ok;
    }

exit:
    return status;
}

amxd_status_t _cond_list_check_port(UNUSED amxd_object_t* object, UNUSED amxd_param_t* param,
                                    amxd_action_t reason, const amxc_var_t* const args,
                                    UNUSED amxc_var_t* const retval, UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* ports = NULL;
    condition_t dummy;

    when_true_status(reason != action_param_validate,
                     exit,
                     status = amxd_status_function_not_implemented);

    ports = amxc_var_constcast(cstring_t, args);
    if(STRING_EMPTY(ports)) {
        status = amxd_status_ok;
        goto exit;
    }

    memset(&dummy, 0, sizeof(condition_t));

    if(condition_set_ports(&dummy, ports, true)) {
        status = amxd_status_ok;
    }

exit:
    return status;
}

amxd_status_t _cond_list_check_connstate(UNUSED amxd_object_t* object, UNUSED amxd_param_t* param,
                                         amxd_action_t reason, const amxc_var_t* const args,
                                         UNUSED amxc_var_t* const retval, UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* connstates = NULL;

    when_true_status(reason != action_param_validate,
                     exit,
                     status = amxd_status_function_not_implemented);

    connstates = amxc_var_constcast(cstring_t, args);
    if(STRING_EMPTY(connstates)) {
        status = amxd_status_ok;
        goto exit;
    }

    if(condition_check_connstates(connstates)) {
        status = amxd_status_ok;
    }


exit:
    return status;
}

amxd_status_t _cond_instance_cleanup(amxd_object_t* object, UNUSED amxd_param_t* param, amxd_action_t reason,
                                     UNUSED const amxc_var_t* const args, UNUSED amxc_var_t* const retval,
                                     UNUSED void* priv) {

    amxd_status_t status = amxd_status_unknown_error;

    when_null(object, exit);
    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);


    cond_cleanup(NULL, object, NULL);

    status = amxd_status_ok;
exit:
    return status;
}
