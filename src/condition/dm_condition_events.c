/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "condition/condition.h"
#include "condition/dm_condition.h"
#include "condition/condition_utils.h"
#include "interception/ic_condition.h"

#define ME "condition"

amxd_object_t* cond_template(amxd_dm_t* dm) {
    amxd_object_t* templ = NULL;
    templ = amxd_dm_findf(dm, "PacketInterception.Condition.");
    return templ;
}

static int cond_init(UNUSED amxd_object_t* templ, amxd_object_t* instance, UNUSED void* priv) {
    condition_t* condition = NULL;

    when_null(instance, exit);

    condition = (condition_t*) instance->priv;
    if(condition == NULL) {
        condition = condition_create(instance);
        when_null(condition, exit);
    }

    condition_set_mac(condition, object_da_string(condition->object, "SourceMAC"), true);
    condition_set_ipversion(condition, amxd_object_get_value(int32_t, condition->object, "IPVersion", NULL));
    condition_set_address(condition, object_da_string(condition->object, "SourceIP"), true);
    condition_set_address(condition, object_da_string(condition->object, "DestIP"), false);
    condition_set_protocols(condition, object_da_string(condition->object, "Protocol"));
    condition_set_ports(condition, object_da_string(condition->object, "SourcePort"), true);
    condition_set_ports(condition, object_da_string(condition->object, "DestPort"), false);
    condition_set_connstates(condition, object_da_string(condition->object, "ConnectionState"));

    ic_condition_condition_changed(condition, INTERCEPTION_NEW);

exit:
    return 1;
}

int cond_cleanup(UNUSED amxd_object_t* templ, amxd_object_t* instance, UNUSED void* priv) {
    condition_t* condition = NULL;

    when_null(instance, exit);

    condition = (condition_t*) instance->priv;
    when_null(condition, exit);

    ic_condition_condition_changed(condition, INTERCEPTION_DELETED);
    condition_delete(condition);
    instance->priv = NULL;

exit:
    return 1;
}

void _cond_added(UNUSED const char* const sig_name, const amxc_var_t* const event_data, UNUSED void* const priv) {
    amxd_dm_t* dm = packetinterception_get_dm();
    amxd_object_t* conditions = amxd_dm_signal_get_object(dm, event_data);
    amxd_object_t* condition = amxd_object_get_instance(conditions, NULL, GET_UINT32(event_data, "index"));

    SAH_TRACEZ_INFO(ME, "Received event: [%s]", sig_name);

    when_null(condition, exit);

    cond_init(conditions, condition, NULL);

exit:
    return;
}

void _cond_changed(UNUSED const char* const sig_name,
                   const amxc_var_t* const event_data,
                   UNUSED void* const priv) {
    amxd_dm_t* dm = packetinterception_get_dm();
    amxd_object_t* objCondition = amxd_dm_signal_get_object(dm, event_data);
    condition_t* condition = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    const amxc_htable_t* hparams = amxc_var_constcast(amxc_htable_t, params);

    const char* param = NULL;
    int ipversion = 0;

    if(amxc_htable_is_empty(hparams)) {
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Received event: [%s]", sig_name);

    when_null(objCondition, exit);
    condition = (condition_t*) objCondition->priv;
    when_null(condition, exit);

    param = GETP_CHAR(params, "SourceMAC.to");
    if(param != NULL) {
        condition_set_mac(condition, param, true);
    }

    ipversion = GETP_UINT32(params, "IPVersion.to");
    if(ipversion != 0) {
        condition_set_ipversion(condition, ipversion);
    }

    param = GETP_CHAR(params, "SourceIP.to");
    if(param != NULL) {
        condition_set_address(condition, param, true);
    }

    param = GETP_CHAR(params, "DestIP.to");
    if(param != NULL) {
        condition_set_address(condition, param, false);
    }

    param = GETP_CHAR(params, "Protocol.to");
    if(param != NULL) {
        condition_set_protocols(condition, param);
    }

    param = GETP_CHAR(params, "SourcePort.to");
    if(param != NULL) {
        condition_set_ports(condition, param, true);
    }

    param = GETP_CHAR(params, "DestPort.to");
    if(param != NULL) {
        condition_set_ports(condition, param, false);
    }

    param = GETP_CHAR(params, "ConnectionState.to");
    if(param != NULL) {
        condition_set_connstates(condition, param);
    }

    ic_condition_condition_changed(condition, INTERCEPTION_MODIFIED);

exit:
    return;
}

void _cond_start(UNUSED const char* const sig_name, UNUSED const amxc_var_t* const event_data, UNUSED void* const priv) {

    SAH_TRACEZ_INFO(ME, "Received event [%s]", sig_name);
}

void _cond_stop(UNUSED const char* const sig_name, UNUSED const amxc_var_t* const event_data, UNUSED void* const priv) {
    amxd_dm_t* dm = packetinterception_get_dm();
    amxd_object_t* conditions = cond_template(dm);

    SAH_TRACEZ_INFO(ME, "Received event: [%s]", sig_name);

    when_null(conditions, exit);

    amxd_object_for_all(conditions, "*", cond_cleanup, NULL);

exit:
    return;
}
