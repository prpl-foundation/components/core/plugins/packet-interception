/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>

#include "interception/ic_comm-config.h"
#include "interception/dm_interception.h"

#define ME "ic_comm_config"

static int ic_comm_config_init(UNUSED amxd_object_t* templ, amxd_object_t* instance, void* priv) {
    ic_comm_config_t* ic_comm = NULL;
    ic_condition_t* ic_cond = NULL;

    when_null(instance, exit);
    when_false(instance->type == amxd_object_instance, exit);

    ic_cond = (ic_condition_t* ) priv;
    when_null(ic_cond, exit);

    SAH_TRACEZ_INFO(ME, "Creating Intercept Communication Config [%s]", OBJECT_NAME(instance));

    ic_comm = instance->priv;
    if(!ic_comm) {
        ic_comm = ic_comm_config_create(instance, ic_cond);
        when_null(ic_comm, exit);
    }

    ic_comm->priority = amxd_object_get_value(uint64_t, instance, "Priority", NULL);
    ic_comm->comm_config = comm_config_find(object_da_string(instance, "CommunicationConfig"));

exit:
    return 1;
}

int ic_comm_config_cleanup(UNUSED amxd_object_t* templ, amxd_object_t* instance, UNUSED void* priv) {
    ic_comm_config_t* ic_comm = NULL;

    when_null(instance, exit);

    ic_comm = (ic_comm_config_t*) instance->priv;
    when_null(ic_comm, exit);

    ic_comm_config_delete(ic_comm);

exit:
    return 1;
}

void _ic_comm_config_added(UNUSED const char* const sig_name,
                           const amxc_var_t* const event_data,
                           UNUSED void* const priv) {
    amxd_dm_t* dm = packetinterception_get_dm();
    amxd_object_t* ic_comms = amxd_dm_signal_get_object(dm, event_data);
    amxd_object_t* ic_comm = amxd_object_get_instance(ic_comms, NULL, GET_UINT32(event_data, "index"));
    amxd_object_t* cond_obj = NULL;
    ic_condition_t* ic_cond = NULL;

    when_null(ic_comm, exit);

    cond_obj = amxd_object_findf(ic_comm, ".^.^");
    ic_cond = (ic_condition_t* ) cond_obj->priv;
    if(!ic_cond) {
        SAH_TRACEZ_ERROR(ME, "Intercept Condition [%s] has no private data", OBJECT_NAME(cond_obj));
        goto exit;
    }

    ic_comm_config_init(ic_comms, ic_comm, ic_cond);

exit:
    return;
}

void _ic_comm_config_changed(UNUSED const char* const sig_name,
                             const amxc_var_t* const event_data,
                             UNUSED void* const priv) {
    amxd_dm_t* dm = packetinterception_get_dm();
    amxd_object_t* objComm = amxd_dm_signal_get_object(dm, event_data);
    ic_comm_config_t* ic_comm = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxc_var_t* status_var = GET_ARG(params, "Status");
    const amxc_htable_t* hparams = amxc_var_constcast(amxc_htable_t, params);
    const char* comm_config_name = NULL;
    amxc_var_t* priority = NULL;

    // Status update should not trigger interception_config_implement_changes
    amxc_var_take_it(status_var);
    if(amxc_htable_is_empty(hparams)) {
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Received event: [%s]", sig_name);

    when_null(objComm, exit);
    ic_comm = (ic_comm_config_t*) objComm->priv;
    when_null(ic_comm, exit);

    comm_config_name = GETP_CHAR(params, "CommunicationConfig.to");
    if(comm_config_name != NULL) {
        ic_comm->comm_config = comm_config_find(comm_config_name);
    }

    priority = GETP_ARG(params, "Priority");
    if(priority != NULL) {
        ic_comm->priority = amxc_var_dyncast(uint64_t, GETP_ARG(priority, "to"));
    }

exit:
    return;
}
