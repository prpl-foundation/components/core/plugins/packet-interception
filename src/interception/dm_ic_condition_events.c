/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "condition/condition.h"
#include "packet-handler/packet-handler.h"
#include "interception/ic_condition.h"
#include "interception/interception.h"
#include "interception/dm_interception.h"

#define ME "ic_condition"

static int ic_cond_init(UNUSED amxd_object_t* templ, amxd_object_t* instance, void* priv) {
    interception_config_t* config = NULL;
    ic_condition_t* ic_cond = NULL;
    amxd_object_t* parent = NULL;
    ic_condition_type_t type;

    when_null(instance, exit);
    when_false(instance->type == amxd_object_instance, exit);

    config = (interception_config_t* ) priv;
    when_null(config, exit);

    SAH_TRACEZ_INFO(ME, "Creating Intercept Condition [%s]", OBJECT_NAME(instance));

    parent = amxd_object_get_parent(instance);
    if(strcmp(amxd_object_get_name(parent, AMXD_OBJECT_NAMED), "Bypass") == 0) {
        type = BYPASS;
    } else {
        type = INTERCEPT;
    }

    ic_cond = instance->priv;
    if(!ic_cond) {
        ic_cond = ic_condition_create(instance, type);
        when_null_trace(ic_cond, exit, ERROR, "Intercept Condition [%s] could not be created", OBJECT_NAME(instance));
    }

    ic_cond->config = config;
    ic_cond->nr_of_packets = amxd_object_get_value(uint32_t, instance, "NumberOfPackets", NULL);
    ic_cond->condition = condition_find(object_da_string(ic_cond->common.object, "Condition"));
    ic_cond->handler = packet_handler_find(object_da_string(ic_cond->common.object, "PacketHandler"));
    ic_cond->direction = fw_direction_from_char(object_da_string(ic_cond->common.object, "Direction"));

exit:
    return 1;
}

int ic_cond_cleanup(UNUSED amxd_object_t* templ, amxd_object_t* instance, UNUSED void* priv) {
    ic_condition_t* ic_cond = NULL;

    when_null(instance, exit);

    ic_cond = (ic_condition_t*) instance->priv;
    when_null(ic_cond, exit);

    ic_cond->flags = INTERCEPTION_DELETED;

exit:
    return 1;
}

void _ic_cond_added(UNUSED const char* const sig_name,
                    const amxc_var_t* const event_data,
                    UNUSED void* const priv) {
    amxd_dm_t* dm = packetinterception_get_dm();
    amxd_object_t* ic_conds = amxd_dm_signal_get_object(dm, event_data);
    amxd_object_t* ic_cond = amxd_object_get_instance(ic_conds, NULL, GET_UINT32(event_data, "index"));
    amxd_object_t* config_obj = NULL;
    interception_config_t* config = NULL;

    when_null(ic_cond, exit);

    config_obj = amxd_object_findf(ic_cond, ".^.^");
    config = (interception_config_t* ) config_obj->priv;
    if(!config) {
        SAH_TRACEZ_ERROR(ME, "Interception [%s] has no private data", OBJECT_NAME(config_obj));
        goto exit;
    }

    ic_cond_init(ic_conds, ic_cond, config);
    ic_condition_implement_changes();

exit:
    return;
}

void _ic_cond_changed(UNUSED const char* const sig_name,
                      const amxc_var_t* const event_data,
                      UNUSED void* const priv) {
    amxd_dm_t* dm = packetinterception_get_dm();
    amxd_object_t* objCond = amxd_dm_signal_get_object(dm, event_data);
    ic_condition_t* ic_cond = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxc_var_t* status_var = GET_ARG(params, "Status");
    amxc_var_t* comm_config_var = GET_ARG(params, "CommunicationConfigNumberOfEntries");
    const amxc_htable_t* hparams = amxc_var_constcast(amxc_htable_t, params);
    amxc_var_t* enable = NULL;
    amxc_var_t* nrofpackets = NULL;
    const char* condition_name = NULL;
    const char* handler_name = NULL;
    const char* direction = NULL;

    // Status or number of comm config update should not trigger interception_config_implement_changes
    amxc_var_take_it(status_var);
    amxc_var_take_it(comm_config_var);
    if(amxc_htable_is_empty(hparams)) {
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Received event: [%s]", sig_name);

    when_null(objCond, exit);
    ic_cond = (ic_condition_t*) objCond->priv;
    when_null(ic_cond, exit);

    enable = GETP_ARG(params, "Enable.to");
    if(enable) {
        ic_cond->flags = INTERCEPTION_MODIFIED;
    }

    condition_name = GETP_CHAR(params, "Condition.to");
    if(condition_name != NULL) {
        ic_cond->condition = condition_find(condition_name);
        ic_cond->flags = INTERCEPTION_MODIFIED;
    }

    handler_name = GETP_CHAR(params, "PacketHandler.to");
    if(handler_name != NULL) {
        ic_cond->handler = packet_handler_find(handler_name);
    }

    nrofpackets = GETP_ARG(params, "NumberOfPackets.to");
    if(nrofpackets) {
        ic_cond->nr_of_packets = amxc_var_dyncast(int32_t, nrofpackets);
        ic_cond->flags = INTERCEPTION_MODIFIED;
    }

    direction = GETP_CHAR(params, "Direction.to");
    if(direction != NULL) {
        ic_cond->direction = fw_direction_from_char(direction);
        ic_cond->flags = INTERCEPTION_MODIFIED;
    }

    if(ic_cond->flags == INTERCEPTION_MODIFIED) {
        ic_condition_implement_changes();
    }

exit:
    return;
}
