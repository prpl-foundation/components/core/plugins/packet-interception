/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "interception/ic_condition.h"
#include "interception/interception.h"
#include "interception/dm_interception.h"

#define ME "interception"

amxd_object_t* ic_template(amxd_dm_t* dm) {
    amxd_object_t* templ = NULL;
    templ = amxd_dm_findf(dm, "PacketInterception.Interception.");
    return templ;
}

static int ic_init(UNUSED amxd_object_t* templ, amxd_object_t* instance, UNUSED void* priv) {
    interception_config_t* config = NULL;
    const char* dest_interface = NULL;
    const char* src_interface = NULL;
    const char* traffic_route = NULL;

    when_null(instance, exit);

    config = (interception_config_t*) instance->priv;
    if(config == NULL) {
        config = interception_config_create(instance);
        when_null(config, exit);
    }

    traffic_route = object_da_string(instance, "TrafficRoute");
    dest_interface = object_da_string(instance, "DestinationInterface");
    src_interface = object_da_string(instance, "SourceInterface");

    when_str_empty_status(traffic_route, exit, SAH_TRACEZ_ERROR(ME, "TrafficRoute can not be empty"));
    amxc_string_set(&config->traffic_route, traffic_route);

    //TODO netmodel support

    if(!STRING_EMPTY(src_interface)) {
        interception_config_get_interface(config, src_interface, true);
    }
    if(!STRING_EMPTY(dest_interface)) {
        interception_config_get_interface(config, dest_interface, false);
    }

exit:
    return 1;
}

int ic_cleanup(UNUSED amxd_object_t* templ, amxd_object_t* instance, UNUSED void* priv) {
    interception_config_t* config = NULL;

    when_null(instance, exit);

    config = (interception_config_t*) instance->priv;
    when_null(config, exit);

    config->flags = INTERCEPTION_DELETED;

    ic_condition_config_deleted(config);

exit:
    return 1;
}

void _ic_added(UNUSED const char* const sig_name,
               const amxc_var_t* const event_data,
               UNUSED void* const priv) {
    amxd_dm_t* dm = packetinterception_get_dm();
    amxd_object_t* configs = amxd_dm_signal_get_object(dm, event_data);
    amxd_object_t* config = amxd_object_get_instance(configs, NULL, GET_UINT32(event_data, "index"));

    SAH_TRACEZ_INFO(ME, "Received event: [%s]", sig_name);

    when_null(config, exit);

    ic_init(configs, config, NULL);
    interception_config_implement_changes();
    ic_condition_implement_changes();

exit:
    return;
}

void _ic_changed(UNUSED const char* const sig_name,
                 const amxc_var_t* const event_data,
                 UNUSED void* const priv) {
    amxd_dm_t* dm = packetinterception_get_dm();
    amxd_object_t* objConfig = amxd_dm_signal_get_object(dm, event_data);
    interception_config_t* config = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxc_var_t* status_var = GET_ARG(params, "Status");
    amxc_var_t* nrofbypass_var = GET_ARG(params, "BypassNumberOfEntries");
    amxc_var_t* nrofintercept_var = GET_ARG(params, "InterceptNumberOfEntries");
    amxc_var_t* param = NULL;
    const amxc_htable_t* hparams = amxc_var_constcast(amxc_htable_t, params);
    const char* traffic_route;

    // Status, BypassNumberOfEntries or InterceptNumberOfEntries update
    // should not trigger interception_config_implement_changes
    amxc_var_take_it(status_var);
    amxc_var_take_it(nrofbypass_var);
    amxc_var_take_it(nrofintercept_var);
    amxc_var_delete(&status_var);
    amxc_var_delete(&nrofbypass_var);
    amxc_var_delete(&nrofintercept_var);

    if(amxc_htable_is_empty(hparams)) {
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Received event: [%s]", sig_name);

    when_null(objConfig, exit);
    config = (interception_config_t*) objConfig->priv;
    when_null(config, exit);

    config->flags = INTERCEPTION_MODIFIED;

    traffic_route = GETP_CHAR(params, "TrafficRoute.to");
    if(traffic_route != NULL) {
        when_str_empty_status(traffic_route, exit, SAH_TRACEZ_ERROR(ME, "TrafficRoute can not be empty"));
        amxc_string_set(&config->traffic_route, traffic_route);
        config->flags = INTERCEPTION_MODIFIED;
    }

    param = GETP_ARG(params, "SourceInterface");
    if(param != NULL) {
        interception_config_get_interface(config, GETP_CHAR(param, "to"), true);
    }

    param = GETP_ARG(params, "DestInterface");
    if(param != NULL) {
        interception_config_get_interface(config, GETP_CHAR(param, "to"), false);
    }

    interception_config_implement_changes();

exit:
    return;
}

void _ic_start(const char* const sig_name,
               UNUSED const amxc_var_t* const event_data,
               UNUSED void* const priv) {

    SAH_TRACEZ_INFO(ME, "Received event: [%s]", sig_name);
}

void _ic_stop(const char* const sig_name,
              UNUSED const amxc_var_t* const event_data,
              UNUSED void* const priv) {
    amxd_dm_t* dm = packetinterception_get_dm();
    amxd_object_t* configs = ic_template(dm);

    SAH_TRACEZ_INFO(ME, "Received event: [%s]", sig_name);

    when_null(configs, exit);

    // undo all firewall changes
    amxd_object_for_all(configs, "*", ic_cleanup, NULL);
    ic_condition_implement_changes();
    interception_config_implement_changes();

exit:
    return;
}
