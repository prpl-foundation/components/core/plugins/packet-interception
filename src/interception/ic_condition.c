/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>

#include "interception/ic_condition.h"

#define ME "ic_condition"

static amxc_llist_t cond_llist;
static amxc_llist_it_t* bypass_cond_last = NULL;
static bool ic_cond_init = false;

ic_condition_t* ic_condition_find_by_qnum(uint32_t qnum) {
    ic_condition_t* ic_cond = NULL;

    amxc_llist_for_each(it, &cond_llist) {
        ic_cond = amxc_llist_it_get_data(it, ic_condition_t, it);

        if((ic_cond->common.status == INTERCEPTION_ENABLED) && ic_cond->handler && (ic_cond->qnum == qnum)) {
            return ic_cond;
        }
    }

    return NULL;
}

static bool ic_condition_get_num(ic_condition_t* ic_cond, uint32_t* qnum) {
    amxc_llist_it_t* it = NULL;
    amxc_llist_it_t* first = NULL;
    uint32_t min = 0;
    uint32_t max = 0;
    uint32_t num_temp = 0;
    bool ret = false;

    min = packetinterception_get_qnum_min_max(true);
    max = packetinterception_get_qnum_min_max(false);

    num_temp = min;

    // Try to retrieve the first intercept condition
    if(bypass_cond_last == NULL) {
        first = amxc_llist_get_first(&cond_llist);
    } else {
        first = amxc_llist_it_get_next(bypass_cond_last);
    }

    // If no first intercept condition, something went wrong
    when_null_trace(first, exit, ERROR, "No Intercept conditions were present in list!");

    for(it = first; it != NULL; it = amxc_llist_it_get_next(it)) {
        ic_condition_t* ref = amxc_llist_it_get_data(it, ic_condition_t, it);

        // Reached the end of enabled intercept conditions, so exit
        if(ref->common.status != INTERCEPTION_ENABLED) {
            break;
        }

        // Break if number is not taken!
        if(ref->qnum != num_temp) {
            break;
        }

        num_temp++;
    }

    when_true_trace(num_temp > max, exit, ERROR, "NFQUEUE number limit reached [%d]", max);
    ic_cond->qnum = num_temp;
    *qnum = num_temp;
    if(it != &ic_cond->it) {
        amxc_llist_it_insert_before(it, &ic_cond->it);
    }
    ret = true;

exit:
    return ret;
}

static bool ic_condition_implement(ic_condition_t* ic_cond, bool ipv4, bool reply) {
    fw_rule_t* r = NULL;
    fw_folder_t* folder = NULL;
    const char* table = NULL;
    char* chain = NULL;
    bool res = false;

    when_null(ic_cond, exit);

    if(ipv4) {
        if(reply) {
            folder = ic_cond->folder_reply;
        } else {
            folder = ic_cond->folder;
        }
    } else {
        if(reply) {
            folder = ic_cond->folder6_reply;
        } else {
            folder = ic_cond->folder6;
        }
    }

    r = fw_folder_fetch_default_rule(folder);
    if(r == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to fetch rule");
        goto exit;
    }

    table = packetinterception_get_table("interception", "mangle");
    chain = interception_config_get_chain_name(ic_cond->config, ipv4, false);

    fw_rule_set_table(r, table);
    fw_rule_set_chain(r, chain);
    fw_rule_set_ipv4(r, ipv4);

    if(ic_cond->type == INTERCEPT) {
        fw_rule_set_connbytes_min(r, 0);
        fw_rule_set_connbytes_max(r, ic_cond->nr_of_packets);
        fw_rule_set_connbytes_param(r, "packets");
        fw_rule_set_connbytes_direction(r, fw_direction_to_char(ic_cond->direction));

        fw_rule_set_target_nfqueue_options(r, ic_cond->qnum, 1, 0);
    } else if(ic_cond->type == BYPASS) {
        fw_rule_set_target_return(r);
    }

    fw_folder_push_rule(folder, r);

    if(!condition_implement(ic_cond->condition, folder, reply)) {
        goto exit;
    }

    fw_folder_set_enabled(folder, true);
    res = true;

exit:
    free(chain);
    return res;
}

static bool ic_condition_activate(ic_condition_t* ic_cond) {
    bool enabled = amxd_object_get_value(bool, ic_cond->common.object, "Enable", NULL);

    if(enabled && (ic_cond->config->common.status == INTERCEPTION_ENABLED) && packetinterception_get_enable()) {
        if(ic_cond->common.status != INTERCEPTION_ENABLED) {

            when_false_status(ic_condition_get_num(ic_cond, &ic_cond->qnum), error, set_status(&ic_cond->common, INTERCEPTION_ERROR));
            when_null_status(ic_cond->condition, error, set_status(&ic_cond->common, INTERCEPTION_ERROR_MISCONFIG));

            if(ic_cond->condition->ipversion != 6) {
                SAH_TRACEZ_INFO(ME, "Apply IPV4 Condition");
                switch(ic_cond->direction) {
                case ORIGINAL:
                    when_false_status(ic_condition_implement(ic_cond, true, false), error, set_status(&ic_cond->common, INTERCEPTION_ERROR));
                    break;
                case REPLY:
                    when_false_status(ic_condition_implement(ic_cond, true, true), error, set_status(&ic_cond->common, INTERCEPTION_ERROR));
                    break;
                case BOTH:
                    when_false_status(ic_condition_implement(ic_cond, true, false), error, set_status(&ic_cond->common, INTERCEPTION_ERROR));

                    bool reply_needed = (!amxc_string_is_empty(&ic_cond->condition->source)) || (!amxc_string_is_empty(&ic_cond->condition->destination)) ||
                        (ic_cond->condition->nr_of_src_ports > 0) || (ic_cond->condition->nr_of_dst_ports > 0);

                    if(reply_needed) {
                        when_false_status(ic_condition_implement(ic_cond, true, true), error, set_status(&ic_cond->common, INTERCEPTION_ERROR));
                    }
                    break;
                }
            }

            if(ic_cond->condition->ipversion != 4) {
                SAH_TRACEZ_INFO(ME, "Apply IPV6 Condition");
                switch(ic_cond->direction) {
                case ORIGINAL:
                    when_false_status(ic_condition_implement(ic_cond, false, false), error, set_status(&ic_cond->common, INTERCEPTION_ERROR));
                    break;
                case REPLY:
                    when_false_status(ic_condition_implement(ic_cond, false, true), error, set_status(&ic_cond->common, INTERCEPTION_ERROR));
                    break;
                case BOTH:
                    when_false_status(ic_condition_implement(ic_cond, false, false), error, set_status(&ic_cond->common, INTERCEPTION_ERROR));

                    bool reply_needed = (!amxc_string_is_empty(&ic_cond->condition->source)) || (!amxc_string_is_empty(&ic_cond->condition->destination)) ||
                        (ic_cond->condition->nr_of_src_ports > 0) || (ic_cond->condition->nr_of_dst_ports > 0);

                    if(reply_needed) {
                        when_false_status(ic_condition_implement(ic_cond, false, true), error, set_status(&ic_cond->common, INTERCEPTION_ERROR));
                    }
                    break;
                }
            }

            if(fw_commit(fw_rule_callback) != 0) {
                SAH_TRACEZ_ERROR(ME, "Failed to commit rules");
                set_status(&ic_cond->common, INTERCEPTION_ERROR);
                goto error;
            }

            if(ic_cond->type == INTERCEPT) {
                when_failed_trace(nfqueue_new(&ic_cond->nfqueue, ic_cond->qnum, 0, true), error, ERROR, "Could not initialize nfqueue for condition [%s]", OBJECT_NAME(ic_cond->common.object));
                amxo_connection_add(packetinterception_get_parser(), nfqueue_get_fd(ic_cond->nfqueue), packet_handler_packet_cb, NULL, AMXO_CUSTOM, ic_cond);
            }

            set_status(&ic_cond->common, INTERCEPTION_ENABLED);

        } else {
            SAH_TRACEZ_NOTICE(ME, "Condition[%s] is already active", OBJECT_NAME(ic_cond->common.object));
        }
    } else {
        SAH_TRACEZ_INFO(ME, "Condition[%s] is not active", OBJECT_NAME(ic_cond->common.object));
    }

    return true;

error:
    SAH_TRACEZ_INFO(ME, "Clean Firewall rules.");
    amxc_llist_append(&cond_llist, &ic_cond->it);
    if(ic_cond->nfqueue) {
        amxo_connection_remove(packetinterception_get_parser(), nfqueue_get_fd(ic_cond->nfqueue));
        nfqueue_delete(&ic_cond->nfqueue, true);
    }
    fw_folder_delete_rules(ic_cond->folder);
    fw_folder_delete_rules(ic_cond->folder6);
    fw_folder_delete_rules(ic_cond->folder_reply);
    fw_folder_delete_rules(ic_cond->folder6_reply);
    fw_commit(fw_rule_callback);
    return false;
}

static bool ic_condition_deactivate(ic_condition_t* ic_cond) {
    if(ic_cond->common.status == INTERCEPTION_ENABLED) {
        SAH_TRACEZ_NOTICE(ME, "Condition [%s] Active, deactivate it", OBJECT_NAME(ic_cond->common.object));
        if(ic_cond->nfqueue) {
            amxo_connection_remove(packetinterception_get_parser(), nfqueue_get_fd(ic_cond->nfqueue));
            nfqueue_delete(&ic_cond->nfqueue, true);
        }
        fw_folder_delete_rules(ic_cond->folder);
        fw_folder_delete_rules(ic_cond->folder6);
        fw_folder_delete_rules(ic_cond->folder_reply);
        fw_folder_delete_rules(ic_cond->folder6_reply);
        fw_commit(fw_rule_callback);
        if(ic_cond->type == INTERCEPT) {
            //Deactivated conditions go to the end of the list
            amxc_llist_append(&cond_llist, &ic_cond->it);
        }
        set_status(&ic_cond->common, INTERCEPTION_DISABLED);
    } else {
        SAH_TRACEZ_INFO(ME, "Condition [%s] already inactive, continue", OBJECT_NAME(ic_cond->common.object));
    }
    return true;
}

void ic_condition_activate_deactivate_all(bool activate) {
    ic_condition_t* ic_cond = NULL;

    amxc_llist_for_each(it, &cond_llist) {
        ic_cond = amxc_llist_it_get_data(it, ic_condition_t, it);

        if(ic_cond == NULL) {
            continue;
        }

        if(activate) {
            ic_condition_activate(ic_cond);
        } else {
            ic_condition_deactivate(ic_cond);
        }
    }
    if(fw_apply() != 0) {
        SAH_TRACEZ_WARNING(ME, "fw_apply failed");
    }
}

ic_condition_t* ic_condition_create(amxd_object_t* object, ic_condition_type_t type) {
    ic_condition_t* ic_cond = NULL;

    when_null(object, exit);

    ic_cond = calloc(1, sizeof(ic_condition_t));
    when_null(ic_cond, exit);

    ic_cond->common.object = object;
    ic_cond->type = type;
    ic_cond->flags = INTERCEPTION_NEW;
    set_status(&ic_cond->common, INTERCEPTION_DISABLED);

    fw_folder_new(&ic_cond->folder);
    fw_folder_new(&ic_cond->folder6);
    fw_folder_new(&ic_cond->folder_reply);
    fw_folder_new(&ic_cond->folder6_reply);

    amxc_llist_init(&ic_cond->comm_configs);

    if(!ic_cond_init) {
        amxc_llist_init(&cond_llist);
        ic_cond_init = true;
    }

    if(type == BYPASS) {
        //Make sure folder bypass folders are before interception folders
        fw_folder_set_order(ic_cond->folder, 1);
        fw_folder_set_order(ic_cond->folder6, 1);
        fw_folder_set_order(ic_cond->folder_reply, 1);
        fw_folder_set_order(ic_cond->folder6_reply, 1);

        // Bypass conditions are added to the beginning of the list.
        amxc_llist_prepend(&cond_llist, &ic_cond->it);
        if(bypass_cond_last == NULL) {
            bypass_cond_last = &ic_cond->it;
        }
    } else {
        amxc_llist_append(&cond_llist, &ic_cond->it);
    }

    object->priv = ic_cond;

exit:
    return ic_cond;
}

static void ic_condition_delete(ic_condition_t* ic_cond) {
    SAH_TRACEZ_NOTICE(ME, "Delete Condition [%s]", OBJECT_NAME(ic_cond->common.object));

    if(&ic_cond->it == bypass_cond_last) {
        bypass_cond_last = amxc_llist_it_get_previous(&ic_cond->it);
    }
    amxc_llist_it_take(&ic_cond->it);
    fw_folder_delete(&ic_cond->folder);
    fw_folder_delete(&ic_cond->folder6);
    fw_folder_delete(&ic_cond->folder_reply);
    fw_folder_delete(&ic_cond->folder6_reply);
    fw_commit(fw_rule_callback);
    //Interception Comm configs will be deleted by object destroy handler
    amxc_llist_clean(&ic_cond->comm_configs, NULL);

    if(ic_cond_init && amxc_llist_is_empty(&cond_llist)) {
        amxc_llist_clean(&cond_llist, NULL);
        ic_cond_init = false;
    }

    ic_cond->common.object->priv = NULL;
    free(ic_cond);
}

bool ic_condition_implement_changes(void) {
    ic_condition_t* ic_cond = NULL;

    amxc_llist_for_each(it, &cond_llist) {
        ic_cond = amxc_llist_it_get_data(it, ic_condition_t, it);

        if(ic_cond == NULL) {
            continue;
        }

        if(ic_cond->flags & INTERCEPTION_DELETED) {
            SAH_TRACEZ_INFO(ME, "Delete Condition [%s] for Config [%s]",
                            OBJECT_NAME(ic_cond->common.object), OBJECT_NAME(ic_cond->config->common.object));
            ic_condition_deactivate(ic_cond);
            ic_condition_delete(ic_cond);
        } else if(ic_cond->flags & INTERCEPTION_NEW) {
            SAH_TRACEZ_INFO(ME, "Add new Condition [%s] for Config [%s]",
                            OBJECT_NAME(ic_cond->common.object), OBJECT_NAME(ic_cond->config->common.object));
            ic_condition_activate(ic_cond);
            ic_cond->flags = 0;
        } else if(ic_cond->flags & INTERCEPTION_MODIFIED) {
            SAH_TRACEZ_INFO(ME, "Deactivate, activate Condition [%s] for Config [%s]",
                            OBJECT_NAME(ic_cond->common.object), OBJECT_NAME(ic_cond->config->common.object));
            ic_condition_deactivate(ic_cond);
            ic_condition_activate(ic_cond);
            ic_cond->flags = 0;
        }
    }

    if(fw_apply() != 0) {
        SAH_TRACEZ_WARNING(ME, "fw_apply failed");
    }
    return true;
}

bool ic_condition_condition_changed(condition_t* condition, flags_t flag) {
    ic_condition_t* ic_cond = NULL;
    const char* cond_name = NULL;
    const char* cond_name_ic = NULL;
    bool changed = false;

    when_null(condition, exit);

    amxc_llist_for_each(it, &cond_llist) {
        ic_cond = amxc_llist_it_get_data(it, ic_condition_t, it);

        if(ic_cond == NULL) {
            continue;
        }

        if(flag == INTERCEPTION_NEW) {
            if(ic_cond->condition != NULL) {
                continue;
            }

            cond_name = OBJECT_NAME(condition->object);
            cond_name_ic = OBJECT_NAME(ic_cond->common.object);

            if(cond_name && cond_name_ic && (strcmp(cond_name, cond_name_ic) == 0)) {
                ic_cond->flags = INTERCEPTION_MODIFIED;
                changed = true;
            }
        } else {
            if(ic_cond->condition == condition) {
                if(flag == INTERCEPTION_DELETED) {
                    ic_cond->condition = NULL;
                }
                ic_cond->flags = INTERCEPTION_MODIFIED;
                changed = true;
            }
        }
    }

    if(changed) {
        ic_condition_implement_changes();
    }

exit:
    return changed;
}

void ic_condition_config_deleted(interception_config_t* config) {
    ic_condition_t* ic_cond = NULL;

    when_null(config, exit);

    amxc_llist_for_each(it, &cond_llist) {
        ic_cond = amxc_llist_it_get_data(it, ic_condition_t, it);

        if(ic_cond == NULL) {
            continue;
        }

        if(ic_cond->config == config) {
            ic_cond->flags = INTERCEPTION_DELETED;
        }
    }

exit:
    return;
}
