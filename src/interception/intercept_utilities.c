/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <fwinterface/interface.h>
#include <fwrules/fw.h>

#include "interception/intercept-utilities.h"

#define ME "interception"

const char* fw_direction_to_char(fw_direction_t direction) {
    switch(direction) {
    case ORIGINAL:
        return "original";
    case REPLY:
        return "reply";
    case BOTH:
        return "both";
    default:
        return NULL;
    }
}

fw_direction_t fw_direction_from_char(const char* direction) {
    fw_direction_t ret = ORIGINAL;

    when_str_empty(direction, exit);

    if(strcmp(direction, "reply") == 0) {
        ret = REPLY;
    } else if(strcmp(direction, "both") == 0) {
        ret = BOTH;
    }

exit:
    return ret;
}

static const char* translate_rule_flag(const fw_rule_flag_t flag) {
    const char* str_flag = "unknown";

    switch(flag) {
    case FW_RULE_FLAG_NEW:
        str_flag = "new";
        break;
    case FW_RULE_FLAG_MODIFIED:
        str_flag = "modified";
        break;
    case FW_RULE_FLAG_DELETED:
        str_flag = "deleted";
        break;
    case FW_RULE_FLAG_HANDLED:
        str_flag = "handled";
        break;
    case FW_RULE_FLAG_LAST:
        str_flag = "last (nothing)";
        break;
    }

    return str_flag;
}

bool fw_rule_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag, UNUSED const char* chain, UNUSED const char* table, int index) {
    int ret = -1;

    if((flag == FW_RULE_FLAG_NEW) || (flag == FW_RULE_FLAG_MODIFIED)) {
        ret = fw_replace_rule(rule, index);
    } else if(flag == FW_RULE_FLAG_DELETED) {
        ret = fw_delete_rule(rule, index);
    }

    if(ret != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to handle %s rule chain[%s] table[%s] index[%d]", translate_rule_flag(flag), chain, table, index);
    } else {
        SAH_TRACEZ_INFO(ME, "Handled %s rule chain[%s] table[%s] index[%d]", translate_rule_flag(flag), chain, table, index);
    }

    return (ret == 0);
}
