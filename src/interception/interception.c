/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <fwrules/fw.h>
#include <fwrules/fw_folder.h>
#include <fwinterface/interface.h>

#include "interception/interception.h"
#include "interception/ic_condition.h"

#define ME "interception"

static amxc_llist_t config_llist;
static bool ic_init = false;

char* interception_config_get_chain_name(interception_config_t* config, bool ipv4, bool reverse) {
    amxc_string_t chain_name;
    char* name = NULL;

    amxc_string_init(&chain_name, 0);
    amxc_string_setf(&chain_name, "INTERCEPT%s_%s", ipv4 ? "" : "6", OBJECT_NAME(config->common.object));
    if(reverse) {
        amxc_string_appendf(&chain_name, "_reverse");
    }
    name = amxc_string_take_buffer(&chain_name);
    amxc_string_clean(&chain_name);

    return name;
}

static const char* interception_config_get_insert_chain_name(interception_config_t* config, bool ipv4) {
    amxc_string_t chain_search_path;
    const char* name = NULL;

    amxc_string_init(&chain_search_path, 0);
    amxc_string_setf(&chain_search_path, "%s%s", amxc_string_get(&config->traffic_route, 0), ipv4 ? "" : "6");
    name = packetinterception_get_chain(amxc_string_get(&chain_search_path, 0), "TEST_CHAIN");

    amxc_string_clean(&chain_search_path);

    return name;
}

static void interception_config_src_interface_cb(UNUSED const char* sig_name, const amxc_var_t* data, void* priv) {
    interception_config_t* config = (interception_config_t*) priv;

    when_null(config, exit);
    when_null(data, exit);
    when_false(amxc_var_type_of(data) == AMXC_VAR_ID_LIST, exit);

    if(config->src_interfaces == NULL) {
        amxc_var_new(&config->src_interfaces);
    }
    amxc_var_copy(config->src_interfaces, data);

    interception_config_implement_changes();

exit:
    return;
}

static void interception_config_dst_interface_cb(UNUSED const char* sig_name, const amxc_var_t* data, void* priv) {
    interception_config_t* config = (interception_config_t*) priv;

    when_null(config, exit);
    when_null(data, exit);
    when_false(amxc_var_type_of(data) == AMXC_VAR_ID_LIST, exit);

    if(config->dest_interfaces == NULL) {
        amxc_var_new(&config->dest_interfaces);
    }
    amxc_var_copy(config->dest_interfaces, data);

    interception_config_implement_changes();

exit:
    return;
}

bool interception_config_get_interface(interception_config_t* config, const char* interface, bool source) {
    bool ret = false;

    when_null(config, exit);

    if(source) {
        if(config->src_interface_query) {
            netmodel_closeQuery(config->src_interface_query);
        }
        amxc_var_delete(&config->src_interfaces);
        config->src_interfaces = NULL;
        if(!STRING_EMPTY(interface)) {
            config->src_interface_query = netmodel_openQuery_getIntfs(interface, "PacketInterception",
                                                                      "netdev && (ipv4 || ipv6) && !inbridge", netmodel_traverse_down,
                                                                      interception_config_src_interface_cb, config);
        }
    } else {
        if(config->dst_interface_query) {
            netmodel_closeQuery(config->dst_interface_query);
        }
        amxc_var_delete(&config->dest_interfaces);
        config->dest_interfaces = NULL;
        if(!STRING_EMPTY(interface)) {
            config->dst_interface_query = netmodel_openQuery_getIntfs(interface, "PacketInterception",
                                                                      "netdev && (ipv4 || ipv6) && !inbridge", netmodel_traverse_down,
                                                                      interception_config_dst_interface_cb, config);
        }
    }

    ret = true;

exit:
    return ret;
}

static bool interception_config_create_chain(interception_config_t* config, bool ipv4) {
    int ret = 0;
    bool res = false;
    const char* table = NULL;
    char* chain_name = NULL;

    //TODO reverse chain creation?

    when_null(config, exit);

    table = packetinterception_get_table("interception", "mangle");

    chain_name = interception_config_get_chain_name(config, ipv4, false);
    ret = fw_add_chain(chain_name, table, !ipv4);
    if(ret < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create chain [%s]", chain_name);
        goto exit;
    }

    res = true;

exit:
    free(chain_name);
    return res;
}

interception_config_t* interception_config_create(amxd_object_t* object) {
    interception_config_t* config = NULL;

    when_null(object, exit);

    if(ic_init == false) {
        amxc_llist_init(&config_llist);
        ic_init = true;
    }

    config = calloc(1, sizeof(interception_config_t));
    when_null_status(config, exit, set_status(&config->common, INTERCEPTION_ERROR));

    config->common.object = object;
    config->flags = INTERCEPTION_NEW;
    set_status(&config->common, INTERCEPTION_DISABLED);

    fw_folder_new(&config->folder);
    fw_folder_new(&config->folder6);

    amxc_string_init(&config->traffic_route, 64);

    interception_config_create_chain(config, true);
    interception_config_create_chain(config, false);

    amxc_llist_append(&config_llist, &config->it);

    object->priv = config;

exit:
    return config;
}

static bool interception_config_delete(interception_config_t* config) {
    when_null(config, exit);

    amxc_llist_it_take(&config->it);

    amxc_string_clean(&config->traffic_route);
    amxc_var_delete(&config->src_interfaces);
    amxc_var_delete(&config->dest_interfaces);

    fw_folder_delete(&config->folder);
    fw_folder_delete(&config->folder6);
    fw_commit(fw_rule_callback);

    //NOTE: There is no API yet to delete a chain

    config->common.object->priv = NULL;
    free(config);
exit:
    return true;
}

static bool interception_config_implement(interception_config_t* config, bool ipv4) {
    fw_rule_t* r = NULL;
    fw_rule_t* s = NULL;
    fw_folder_t* folder = NULL;
    const char* insert_table = NULL;
    const char* insert_chain = NULL;
    const char* src_interface = NULL;
    const char* dst_interface = NULL;
    char* chain = NULL;
    bool res = false;

    when_null(config, exit);

    insert_table = packetinterception_get_table("interception", "mangle");
    insert_chain = interception_config_get_insert_chain_name(config, ipv4);
    chain = interception_config_get_chain_name(config, ipv4, false);

    folder = ipv4 ? config->folder : config->folder6;

    r = fw_folder_fetch_default_rule(folder);
    if(r == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to fetch rule");
        goto exit;
    }

    fw_rule_set_chain(r, insert_chain);
    fw_rule_set_table(r, insert_table);
    fw_rule_set_ipv4(r, ipv4);
    fw_rule_set_target_chain(r, chain);

    fw_folder_push_rule(folder, r);

    if(config->src_interfaces) {
        fw_folder_set_feature(folder, FW_FEATURE_IN_INTERFACE);
        amxc_var_for_each(src_var, config->src_interfaces) {
            src_interface = amxc_var_constcast(cstring_t, src_var);
            if(!src_interface) {
                continue;
            }
            s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_IN_INTERFACE);
            if(s == NULL) {
                SAH_TRACEZ_ERROR(ME, "Failed to fetch feature rule");
                goto exit;
            }
            fw_rule_set_in_interface(s, src_interface);
            fw_folder_push_rule(folder, s);
        }
    } else {
        fw_folder_unset_feature(folder, FW_FEATURE_IN_INTERFACE);
    }

    //NOTE no feature rule for OUT interface!
    if(config->dest_interfaces) {
        dst_interface = amxc_var_constcast(cstring_t, amxc_var_get_first(config->dest_interfaces));
        if(!dst_interface) {
            goto exit;
        }

        r = fw_folder_fetch_default_rule(folder);
        if(r == NULL) {
            SAH_TRACEZ_ERROR(ME, "Failed to fetch rule");
            goto exit;
        }

        fw_rule_set_out_interface(r, dst_interface);
        fw_folder_push_rule(folder, r);
    }

    fw_folder_set_enabled(folder, true);
    res = true;

exit:
    free(chain);
    return res;
}

static bool interception_config_activate(interception_config_t* config) {
    bool res = false;
    bool enabled = amxd_object_get_value(bool, config->common.object, "Enable", NULL);

    if(enabled && packetinterception_get_enable()) {
        if(config->common.status != INTERCEPTION_ENABLED) {
            //TODO determine whether reverse chain should be created

            //IPv4
            SAH_TRACEZ_INFO(ME, "Apply IPV4 Interception");
            res = interception_config_implement(config, 1);
            if(res == false) {
                goto error;
            }

            //IPv6
            SAH_TRACEZ_INFO(ME, "Apply IPV6 Interception");
            res = interception_config_implement(config, 0);
            if(res == false) {
                goto error;
            }

            if(fw_commit(fw_rule_callback) != 0) {
                SAH_TRACEZ_ERROR(ME, "Failed to commit rules");
                goto error;
            }

            set_status(&config->common, INTERCEPTION_ENABLED);
        } else {
            SAH_TRACEZ_NOTICE(ME, "Interception [%s] is already active", OBJECT_NAME(config->common.object));
        }
    } else {
        SAH_TRACEZ_INFO(ME, "Interception [%s] is not active", OBJECT_NAME(config->common.object));
    }

    return true;

error:
    set_status(&config->common, INTERCEPTION_ERROR);
    SAH_TRACEZ_INFO(ME, "Clean Firewall rules.");
    fw_folder_delete_rules(config->folder);
    fw_folder_delete_rules(config->folder6);

    fw_commit(fw_rule_callback);
    return res;
}

static bool interception_config_deactivate(interception_config_t* config) {
    if(config->common.status == INTERCEPTION_ENABLED) {
        SAH_TRACEZ_NOTICE(ME, "Interception [%s] Active, deactivate it", OBJECT_NAME(config->common.object));
        fw_folder_delete_rules(config->folder);
        fw_folder_delete_rules(config->folder6);
        fw_commit(fw_rule_callback);
        set_status(&config->common, INTERCEPTION_DISABLED);
    } else {
        SAH_TRACEZ_INFO(ME, "Interception [%s] already inactive, continue", OBJECT_NAME(config->common.object));
    }
    return true;
}

void interception_config_activate_deactivate_all(bool activate) {
    interception_config_t* config = NULL;

    amxc_llist_for_each(it, &config_llist) {
        config = amxc_llist_it_get_data(it, interception_config_t, it);

        if(config == NULL) {
            continue;
        }

        if(activate) {
            interception_config_activate(config);
        } else {
            interception_config_deactivate(config);
        }
    }
    if(fw_apply() != 0) {
        SAH_TRACEZ_WARNING(ME, "fw_apply failed");
    }
}

bool interception_config_implement_changes(void) {
    interception_config_t* config = NULL;

    amxc_llist_for_each(it, &config_llist) {
        config = amxc_llist_it_get_data(it, interception_config_t, it);

        if(config == NULL) {
            continue;
        }

        if(config->flags & INTERCEPTION_DELETED) {
            SAH_TRACEZ_INFO(ME, "Delete Interception [%s]", OBJECT_NAME(config->common.object));
            interception_config_deactivate(config);
            interception_config_delete(config);
        } else if(config->flags & INTERCEPTION_NEW) {
            SAH_TRACEZ_INFO(ME, "Add new Interception [%s]", OBJECT_NAME(config->common.object));
            interception_config_activate(config);
            config->flags = 0;
        } else if(config->flags & INTERCEPTION_MODIFIED) {
            SAH_TRACEZ_INFO(ME, "Deactivate, activate Interception [%s]", OBJECT_NAME(config->common.object));
            interception_config_deactivate(config);
            interception_config_activate(config);
            config->flags = 0;
        }
    }
    if(fw_apply() != 0) {
        SAH_TRACEZ_WARNING(ME, "fw_apply failed");
    }
    return true;
}
