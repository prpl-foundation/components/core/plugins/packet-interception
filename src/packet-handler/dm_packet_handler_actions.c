/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "statistics.h"
#include "packet-handler/packet-handler.h"
#include "packet-handler/dm_packet-handler.h"
#include "packet-handler/packet-handler_utils.h"

#define ME "packet_handler"

static stat_ref_t handler_stats_refs[] = {
    {"NrOfPacketsReceived", AMXC_VAR_ID_UINT32, offsetof(packet_handler_stats_t, nr_of_packets_received)},
    {"NrOfPacketsHandled", AMXC_VAR_ID_UINT32, offsetof(packet_handler_stats_t, nr_of_packets_handled)},
    {"NrOfPacketsTimedout", AMXC_VAR_ID_UINT32, offsetof(packet_handler_stats_t, nr_of_packets_timedout)},
    {"NrOfPacketsAccepted", AMXC_VAR_ID_UINT32, offsetof(packet_handler_stats_t, nr_of_packets_accepted)},
    {"NrOfPacketsDropped", AMXC_VAR_ID_UINT32, offsetof(packet_handler_stats_t, nr_of_packets_dropped)},
    {"MinResponseTime", AMXC_VAR_ID_UINT32, offsetof(packet_handler_stats_t, min_response_time)},
    {"MaxResponseTime", AMXC_VAR_ID_UINT32, offsetof(packet_handler_stats_t, max_response_time)},
    {"AvgResponseTime", AMXC_VAR_ID_UINT32, offsetof(packet_handler_stats_t, avg_response_time)},
    { NULL, 0, 0}
};

amxd_status_t _handler_stats_read(amxd_object_t* object, amxd_param_t* param,
                                  amxd_action_t reason, const amxc_var_t* const args,
                                  amxc_var_t* const retval, void* priv) {
    packet_handler_t* handler = NULL;
    packet_handler_stats_t* stats = NULL;
    amxc_string_t filter;
    amxd_status_t status = amxd_action_object_read(object, param, reason,
                                                   args, retval, priv);

    amxc_string_init(&filter, 0);

    if((status != amxd_status_ok) &&
       ( status != amxd_status_parameter_not_found)) {
        goto exit;
    }

    handler = (packet_handler_t* ) amxd_object_get_parent(object)->priv;
    stats = handler == NULL ? NULL : &handler->stats;

    status = amxd_action_object_read_filter(&filter, args);
    when_failed(status, exit);
    status = statistics_filtered_stats_value(retval, handler_stats_refs, amxc_string_get(&filter, 0), stats);

exit:
    amxc_string_clean(&filter);
    return status;
}

amxd_status_t _handler_stats_list(amxd_object_t* object, amxd_param_t* param,
                                  amxd_action_t reason, const amxc_var_t* const args,
                                  amxc_var_t* const retval, void* priv) {
    amxc_var_t* params = NULL;
    amxd_status_t status = amxd_action_object_list(object, param, reason,
                                                   args, retval, priv);

    if(status != amxd_status_ok) {
        goto exit;
    }
    params = amxc_var_get_path(retval, "parameters", AMXC_VAR_FLAG_DEFAULT);
    for(int i = 0; handler_stats_refs[i].name != NULL; i++) {
        amxc_var_add(cstring_t, params, handler_stats_refs[i].name);
    }

exit:
    return status;
}

amxd_status_t _handler_stats_describe(amxd_object_t* object, amxd_param_t* param,
                                      amxd_action_t reason, const amxc_var_t* const args,
                                      amxc_var_t* const retval, void* priv) {
    packet_handler_t* handler = NULL;
    packet_handler_stats_t* stats = NULL;
    amxc_var_t* params = NULL;
    amxd_status_t status = amxd_action_object_describe(object, param, reason,
                                                       args, retval, priv);

    if(status != amxd_status_ok) {
        goto exit;
    }

    handler = (packet_handler_t*) amxd_object_get_parent(object)->priv;
    stats = handler == NULL ? NULL : &handler->stats;

    params = amxc_var_get_path(retval, "parameters", AMXC_VAR_FLAG_DEFAULT);
    for(int i = 0; handler_stats_refs[i].name != NULL; i++) {
        statistics_add_stats(params, stats, &handler_stats_refs[i]);
    }

exit:
    return status;
}

amxd_status_t _handler_check_mark(UNUSED amxd_object_t* object, UNUSED amxd_param_t* param,
                                  amxd_action_t reason, const amxc_var_t* const args,
                                  UNUSED amxc_var_t* const retval, UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* mark = NULL;
    packet_handler_t dummy;

    when_true_status(reason != action_param_validate,
                     exit,
                     status = amxd_status_function_not_implemented);

    mark = amxc_var_constcast(cstring_t, args);
    if(STRING_EMPTY(mark)) {
        status = amxd_status_ok;
        goto exit;
    }

    memset(&dummy, 0, sizeof(packet_handler_t));

    if(packet_handler_set_mark(&dummy, mark, true)) {
        status = amxd_status_ok;
    }

exit:
    return status;
}

amxd_status_t _handler_instance_cleanup(amxd_object_t* object, UNUSED amxd_param_t* param, amxd_action_t reason,
                                        UNUSED const amxc_var_t* const args, UNUSED amxc_var_t* const retval,
                                        UNUSED void* priv) {

    amxd_status_t status = amxd_status_unknown_error;

    when_null(object, exit);
    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);


    handler_cleanup(NULL, object, NULL);

    status = amxd_status_ok;
exit:
    return status;
}
