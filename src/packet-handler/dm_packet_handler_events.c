/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "packet-handler/packet-handler.h"
#include "packet-handler/packet-handler_utils.h"
#include "packet-handler/dm_packet-handler.h"

#define ME "packet_handler"

amxd_object_t* handler_template(amxd_dm_t* dm) {
    amxd_object_t* templ = NULL;
    templ = amxd_dm_findf(dm, "PacketInterception.PacketHandler.");
    return templ;
}

static int handler_init(UNUSED amxd_object_t* templ, amxd_object_t* instance, UNUSED void* priv) {
    packet_handler_t* handler = NULL;

    when_null(instance, exit);

    handler = (packet_handler_t*) instance->priv;
    if(handler == NULL) {
        handler = packet_handler_create(instance);
        when_null(handler, exit);
    }

    packet_handler_set_verdict(handler, object_da_string(instance, "DefaultVerdict"));
    packet_handler_set_mark(handler, object_da_string(instance, "DefaultMark"), 0);
    packet_handler_set_mark(handler, object_da_string(instance, "DefaultConnMark"), 1);
    packet_handler_set_interval(handler, amxd_object_get_value(int32_t, instance, "Timeout", NULL));

    packet_handler_activate(handler);

exit:
    return 1;
}

int handler_cleanup(UNUSED amxd_object_t* templ, amxd_object_t* instance, UNUSED void* priv) {
    packet_handler_t* handler = NULL;

    when_null(instance, exit);

    handler = (packet_handler_t*) instance->priv;
    when_null(handler, exit);

    packet_handler_delete(handler);

exit:
    return 1;
}

void _handler_added(UNUSED const char* const sig_name, const amxc_var_t* const event_data, UNUSED void* const priv) {
    amxd_dm_t* dm = packetinterception_get_dm();
    amxd_object_t* handlers = amxd_dm_signal_get_object(dm, event_data);
    amxd_object_t* handler = amxd_object_get_instance(handlers, NULL, GET_UINT32(event_data, "index"));

    SAH_TRACEZ_INFO(ME, "Received event: [%s]", sig_name);

    when_null(handler, exit);

    handler_init(handlers, handler, NULL);

exit:
    return;
}

void _handler_changed(UNUSED const char* const sig_name,
                      const amxc_var_t* const event_data,
                      UNUSED void* const priv) {
    amxd_dm_t* dm = packetinterception_get_dm();
    amxd_object_t* objHandler = amxd_dm_signal_get_object(dm, event_data);
    packet_handler_t* handler = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxc_var_t* status_var = GET_ARG(params, "Status");
    const amxc_htable_t* hparams = amxc_var_constcast(amxc_htable_t, params);
    const amxc_var_t* param_var = NULL;
    const char* param = NULL;
    int interval = 0;

    // Status update should not trigger interception_config_implement_changes
    amxc_var_take_it(status_var);
    if(amxc_htable_is_empty(hparams)) {
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Received event: [%s]", sig_name);

    when_null(objHandler, exit);
    handler = (packet_handler_t*) objHandler->priv;
    when_null(handler, exit);

    param = GETP_CHAR(params, "DefaultVerdict.to");
    if(param != NULL) {
        packet_handler_set_verdict(handler, param);
    }

    param = GETP_CHAR(params, "DefaultMark.to");
    if(param != NULL) {
        packet_handler_set_mark(handler, param, 0);
    }

    param = GETP_CHAR(params, "DefaultConnMark.to");
    if(param != NULL) {
        packet_handler_set_mark(handler, param, 1);
    }

    param_var = GETP_ARG(params, "Timeout");
    if(param_var != NULL) {
        interval = GETP_INT32(params, "Timeout.to");
        packet_handler_set_interval(handler, interval);
    }

    packet_handler_deactivate(handler);
    packet_handler_activate(handler);

exit:
    return;
}

void _handler_start(const char* const sig_name,
                    UNUSED const amxc_var_t* const event_data,
                    UNUSED void* const priv) {

    SAH_TRACEZ_INFO(ME, "Received event: [%s]", sig_name);
}

void _handler_stop(const char* const sig_name,
                   UNUSED const amxc_var_t* const event_data,
                   UNUSED void* const priv) {
    amxd_dm_t* dm = packetinterception_get_dm();
    amxd_object_t* handlers = handler_template(dm);

    SAH_TRACEZ_INFO(ME, "Received event: [%s]", sig_name);

    when_null(handlers, exit);

    // undo all firewall changes
    amxd_object_for_all(handlers, "*", handler_cleanup, NULL);

exit:
    return;
}
