/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <debug/sahtrace.h>

#include <linux/if_ether.h>

#include "packet-handler/packet-handler.h"
#include "comm-config/comm-config.h"
#include "interception/ic_condition.h"

#include <arpa/inet.h>

#define ME "packet_handler"

static amxc_llist_t handler_llist;
static bool handler_init = false;
typedef struct {
    uint16_t group;
    uint32_t packetid;
    uint32_t mark;
    uint32_t connmark;
    uint32_t packet_nr;
    uint64_t current_priority;
    uint8_t tcp_state;
    char* payload;
    struct timeval timestamp;
    amxc_llist_t verdicts;
    amxc_llist_it_t it;
} packet_info_t;

static packet_info_t* packet_info_create(packet_id_t* packet_id, packet_data_t* packet) {
    packet_info_t* info = NULL;

    info = calloc(1, sizeof(packet_info_t));
    when_null(info, exit);

    info->group = packet_id->group;
    info->packetid = packet_id->id;
    info->mark = packet->mark;
    info->connmark = packet->conntrack_data.mark;

exit:
    return info;
}

static void packet_info_delete(packet_info_t* info) {
    amxc_llist_it_take(&info->it);
    amxc_llist_clean(&info->verdicts, NULL);
    free(info->payload);
    free(info);
}

static void packet_info_calculate_time_stats(packet_handler_t* handler, const packet_info_t* info) {
    struct timeval now;
    struct timeval difference;
    uint32_t response_time = 0;
    packet_handler_stats_t* stats = NULL;

    when_null(handler, exit);
    when_null(info, exit);

    gettimeofday(&now, NULL);
    stats = &handler->stats;

    timersub(&now, &info->timestamp, &difference);
    response_time = difference.tv_sec * 1000 + difference.tv_usec / 1000;

    if(response_time > stats->max_response_time) {
        stats->max_response_time = response_time;
    }
    if((response_time < stats->min_response_time) || (stats->min_response_time == 0)) {
        stats->min_response_time = response_time;
    }

    stats->avg_response_time = ((stats->avg_response_time * stats->nr_of_packets_handled) + response_time) / (stats->nr_of_packets_handled + 1);

exit:
    return;
}

static void packet_info_update_verdict_stats(packet_handler_t* handler, const packet_verdict_t* verdict) {
    switch(verdict->verdict) {
    case NF_ACCEPT:
        handler->stats.nr_of_packets_accepted++;
        break;
    case NF_DROP:
        handler->stats.nr_of_packets_dropped++;
        break;
    default:
        break;
    }
}

static bool packet_info_timedout(const packet_handler_t* handler, const packet_info_t* info) {
    struct timeval now;
    struct timeval difference;
    struct timeval interval;
    bool ret = false;

    when_null(handler, exit);
    when_null(info, exit);

    gettimeofday(&now, NULL);

    timersub(&now, &info->timestamp, &difference);

    interval.tv_sec = handler->interval / 1000;
    interval.tv_usec = (handler->interval - interval.tv_sec * 1000) * 1000;

    ret = timercmp(&difference, &interval, >=);

exit:
    return ret;
}

static uint32_t packet_handler_determine_mark(uint32_t packet_mark, uint32_t verdict_mark, uint32_t verdict_mask) {
    uint32_t mark = packet_mark;

    if(verdict_mask != 0) {
        mark = (packet_mark & ~verdict_mask) + (verdict_mark & verdict_mask);
    }

    return mark;
}

static bool packet_handler_judge_packet(nfqueue_t* nfqueue, const packet_info_t* info, const packet_verdict_t* verdict, uint32_t nr_of_packets) {
    uint32_t mark = 0;
    uint32_t connmark = 0;
    uint32_t pkt_verdict = verdict->verdict;
    bool ret = false;

    mark = packet_handler_determine_mark(info->mark, verdict->mark, verdict->mark_mask);
    connmark = packet_handler_determine_mark(info->connmark, verdict->connmark, verdict->connmark_mask);

    if(pkt_verdict == NF_DROP) {
        if((info->tcp_state != TCP_CONNTRACK_NONE) && (info->tcp_state != TCP_CONNTRACK_SYN_SENT)) {
            // Let the packet pass, it will be catched by the tcp-reset iptables rule using connmark.
            pkt_verdict = NF_ACCEPT;
            connmark |= packetinterception_get_tcpreset_mark();
        }
    }

    // Set the not offload mark so the next packet will be intercepted.
    if(nr_of_packets > 0) {
        if(info->packet_nr <= nr_of_packets) {
            connmark |= packetinterception_get_notoffload_mark();
        } else {
            connmark &= !packetinterception_get_notoffload_mark();
        }
    }

    SAH_TRACEZ_INFO(ME, "Setting mark [%d] connmark [%d]", mark, connmark);

    ret = nfqueue_judge_packet_mark(nfqueue, info->packetid, pkt_verdict, mark, connmark);

    return ret;
}

static void packet_handler_judge_packet_default(packet_handler_t* handler, nfqueue_t* nfqueue, packet_info_t* info, uint32_t nr_of_packets) {
    packet_handler_judge_packet(nfqueue, info, &handler->default_verdict, nr_of_packets);
    packet_info_update_verdict_stats(handler, &handler->default_verdict);

    packet_info_delete(info);

    if(amxc_llist_is_empty(&handler->packets_llist)) {
        amxp_timer_stop(handler->timer);
    }
}

bool packet_handler_verdict_cb(packet_handler_t* handler, nfqueue_t* nfqueue, const packet_id_t* packet, const packet_verdict_t* verdict, uint32_t nr_of_packets) {
    packet_info_t* info = NULL;
    bool ret = false;
    bool packet_found = false;

    when_null(handler, exit);
    when_null(nfqueue, exit);
    when_null(packet, exit);
    when_null(verdict, exit);

    amxc_llist_for_each(it, &handler->packets_llist) {
        info = amxc_llist_it_get_data(it, packet_info_t, it);
        if(info && (info->group == packet->group) && (info->packetid == packet->id)) {
            packet_found = true;
            break;
        }
    }

    if(!packet_found) {
        SAH_TRACEZ_NOTICE(ME, "Packet group [%d] id [%d] not found in packethandler [%s]",
                          packet->group, packet->id, OBJECT_NAME(handler->common.object));
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Packet [%d][%d] has verdict [%d] and mark [%d]", packet->group, packet->id, verdict->verdict, verdict->mark);

    //TODO we should actually save verdict instead of applying it directly
    packet_handler_judge_packet(nfqueue, info, verdict, nr_of_packets);
    packet_info_calculate_time_stats(handler, info);
    packet_info_update_verdict_stats(handler, verdict);
    handler->stats.nr_of_packets_handled++;
    packet_info_delete(info);

    if(amxc_llist_is_empty(&handler->packets_llist)) {
        amxp_timer_stop(handler->timer);
    }

    ret = true;

exit:
    return ret;
}

static void packet_handler_timeout_cb(UNUSED amxp_timer_t* timer, void* priv) {
    SAH_TRACEZ_IN(ME);

    packet_handler_t* handler = NULL;
    packet_info_t* info = NULL;
    ic_condition_t* ic_cond = NULL;

    handler = (packet_handler_t* ) priv;
    when_null(handler, exit);

    amxc_llist_for_each(it, &handler->packets_llist) {
        info = amxc_llist_it_get_data(it, packet_info_t, it);

        SAH_TRACEZ_INFO(ME, "Handling packet group [%d] id [%d]", info->group, info->packetid);

        if(packet_info_timedout(handler, info)) {
            ic_cond = ic_condition_find_by_qnum(info->group);
            if(!ic_cond) {
                // We can not do anything with the packet, since we have no nfqueue handle anymore.
                continue;
            }

            SAH_TRACEZ_INFO(ME, "Timeout expired for packet group [%d] id [%d]", info->group, info->packetid);

            packet_handler_judge_packet_default(handler, ic_cond->nfqueue, info, ic_cond->nr_of_packets);
            handler->stats.nr_of_packets_timedout++;
        } else {
            break;
        }
    }

    if(amxc_llist_is_empty(&handler->packets_llist)) {
        amxp_timer_stop(handler->timer);
    }

exit:
    SAH_TRACEZ_OUT(ME);
}

void packet_handler_packet_cb(UNUSED int fd, void* priv) {
    ic_condition_t* ic_cond = NULL;
    packet_handler_t* handler = NULL;
    packet_info_t* info = NULL;
    packet_data_t* packet = NULL;
    packet_id_t packet_id;
    nfqueue_data_t nfqdata;
    uint64_t priority = 0;

    ic_cond = (ic_condition_t* ) priv;
    when_null(ic_cond, exit);

    handler = ic_cond->handler;

    when_false(nfqueue_read(ic_cond->nfqueue, &nfqdata, &packet) == 0, exit);

    if((handler == NULL) || (handler->common.status != INTERCEPTION_ENABLED)) {
        nfqueue_judge_packet(ic_cond->nfqueue, nfqdata.id, NF_ACCEPT);
        goto exit;
    }

    handler->stats.nr_of_packets_received++;

    packet_id.id = nfqdata.id;
    packet_id.group = nfqdata.group;

    info = packet_info_create(&packet_id, packet);

    if(handler->interval <= 0) {
        packet_handler_judge_packet_default(handler, ic_cond->nfqueue, info, ic_cond->nr_of_packets);
        goto exit;
    }

    conntrack_get_connection_info(packetinterception_get_conntrack(), &packet->conntrack_data);

    if((packet->conntrack_data.packets_orig == 0) && (packet->conntrack_data.packets_repl == 0)) {
        // This is the first packet
        packet->conntrack_data.packets_orig = 1;
    }

    //TODO this should sometimes be packets_orig, packets_reply or packets_orig + packets_reply
    info->packet_nr = packet->conntrack_data.packets_orig + packet->conntrack_data.packets_repl;

    if(packet->conntrack_data.l4protocol == 6) {
        info->tcp_state = packet->conntrack_data.tcp_state;
    }

    // NOTE: NFQUEUE timestamp is NOK. This should be fixed in the kernel
    gettimeofday(&packet->timestamp, NULL);
    memcpy(&info->timestamp, &packet->timestamp, sizeof(struct timeval));
    amxc_llist_append(&handler->packets_llist, &info->it);
    if(amxc_llist_size(&handler->packets_llist) == 1) {
        amxp_timer_start(handler->timer, handler->interval);
    }

    //TODO multiple comm configs
    priority = info->current_priority;
    while(comm_config_send_packet_next(ic_cond, &packet_id, packet, &priority) == false) {
        if(priority != info->current_priority) {
            info->current_priority = priority;
        } else {
            packet_handler_judge_packet_default(handler, ic_cond->nfqueue, info, ic_cond->nr_of_packets);
            break;
        }
    }

exit:
    packet_data_cleanup(&packet);
    return;
}

bool packet_handler_activate(packet_handler_t* handler) {
    bool res = false;
    bool enabled = amxd_object_get_value(bool, handler->common.object, "Enable", NULL);

    when_null(handler, exit);

    if(enabled && packetinterception_get_enable()) {
        if(handler->common.status != INTERCEPTION_ENABLED) {
            set_status(&handler->common, INTERCEPTION_ENABLED);

        } else {
            SAH_TRACEZ_NOTICE(ME, "Handler [%s] is already active", OBJECT_NAME(handler->common.object));
        }
    } else {
        SAH_TRACEZ_INFO(ME, "Handler [%s] is not active", OBJECT_NAME(handler->common.object));
    }

    res = true;

exit:
    return res;
}

bool packet_handler_deactivate(packet_handler_t* handler) {
    when_null(handler, exit);

    if(handler->common.status == INTERCEPTION_ENABLED) {
        SAH_TRACEZ_NOTICE(ME, "Handler [%s] Active, deactivate it", OBJECT_NAME(handler->common.object));
        set_status(&handler->common, INTERCEPTION_DISABLED);
    } else {
        SAH_TRACEZ_INFO(ME, "Handler [%s] already inactive, continue", OBJECT_NAME(handler->common.object));
    }
exit:
    return true;
}

void packet_handler_activate_deactivate_all(bool activate) {
    packet_handler_t* handler = NULL;

    amxc_llist_for_each(it, &handler_llist) {
        handler = amxc_llist_it_get_data(it, packet_handler_t, it);
        if(!handler) {
            continue;
        }

        if(activate) {
            packet_handler_activate(handler);
        } else {
            packet_handler_deactivate(handler);
        }
    }
}

packet_handler_t* packet_handler_find(const char* name) {
    packet_handler_t* handler = NULL;

    when_str_empty(name, exit);

    amxc_llist_for_each(it, &handler_llist) {
        handler = amxc_llist_it_get_data(it, packet_handler_t, it);
        if(handler && (strcmp(name, OBJECT_NAME(handler->common.object)) == 0)) {
            return handler;
        }
    }

exit:
    return NULL;
}

packet_handler_t* packet_handler_create(amxd_object_t* object) {
    packet_handler_t* handler = NULL;

    when_null(object, exit);

    handler = calloc(1, sizeof(packet_handler_t));
    when_null(handler, exit);

    if(!handler_init) {
        amxc_llist_init(&handler_llist);
        handler_init = true;
    }

    handler->common.object = object;
    amxc_llist_init(&handler->packets_llist);
    amxp_timer_new(&handler->timer, packet_handler_timeout_cb, handler);

    set_status(&handler->common, INTERCEPTION_DISABLED);

    amxc_llist_append(&handler_llist, &handler->it);

    object->priv = handler;

exit:
    return handler;
}

static void packet_handler_delete_packets(amxc_llist_it_t* it) {
    packet_info_t* info = amxc_llist_it_get_data(it, packet_info_t, it);
    packet_info_delete(info);
}

void packet_handler_delete(packet_handler_t* handler) {
    SAH_TRACEZ_NOTICE(ME, "Delete Packet Handler [%s]", OBJECT_NAME(handler->common.object));

    amxc_llist_clean(&handler->packets_llist, packet_handler_delete_packets);
    amxp_timer_stop(handler->timer);
    amxp_timer_delete(&handler->timer);
    amxc_llist_it_take(&handler->it);

    handler->common.object->priv = NULL;

    free(handler);
    handler = NULL;
}
