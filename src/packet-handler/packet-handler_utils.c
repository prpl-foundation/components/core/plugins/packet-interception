/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <debug/sahtrace.h>

#include "packet-handler/packet-handler_utils.h"

#define ME "packet_handler"

static bool is_numeric(const char* data) {
    bool retval = false;

    while((*data) != '\0') {
        if(isdigit(*data) == 0) {
            goto exit;
        }
        data++;
    }

    retval = true;
exit:
    return retval;
}

bool packet_handler_set_verdict(packet_handler_t* handler, const char* verdict) {
    bool retval = false;

    when_null(handler, exit);
    when_null(verdict, exit);

    if(strcmp(verdict, "DROP") == 0) {
        handler->default_verdict.verdict = NF_DROP;
    } else if(strcmp(verdict, "ACCEPT") == 0) {
        handler->default_verdict.verdict = NF_ACCEPT;
    } else if(strcmp(verdict, "STOLEN") == 0) {
        handler->default_verdict.verdict = NF_STOLEN;
    } else if(strcmp(verdict, "REPEAT") == 0) {
        handler->default_verdict.verdict = NF_REPEAT;
    }

    retval = true;

exit:
    return retval;
}

bool packet_handler_set_mark(packet_handler_t* handler, const char* mark_orig, bool connmark) {
    char mark_txt[41];
    char* mask_txt = NULL;
    uint32_t* mark;
    uint32_t* mask;
    bool retval = false;

    when_null(handler, exit);

    if(connmark) {
        mark = &handler->default_verdict.connmark;
        mask = &handler->default_verdict.connmark_mask;
    } else {
        mark = &handler->default_verdict.mark;
        mask = &handler->default_verdict.mark_mask;
    }

    strncpy(mark_txt, mark_orig, 40);
    mark_txt[40] = 0;

    if(STRING_EMPTY(mark_txt)) {
        *mark = 0;
        *mask = 0;
        retval = true;
        goto exit;
    }

    mask_txt = strchr(mark_txt, '/');
    if(mask_txt == NULL) {
        if(is_numeric(mark_txt)) {
            *mark = atoi(mark_txt);
            *mask = 0;
            retval = true;
        }
        goto exit;
    }

    mask_txt[0] = '\0';
    mask_txt++;

    if(is_numeric(mark_txt) && is_numeric(mask_txt)) {
        *mark = atoi(mark_txt);
        *mask = atoi(mask_txt);
        retval = true;
    }

exit:
    if(handler) {
        SAH_TRACEZ_INFO(ME, "New mark [%d] and mask [%d]", *mark, *mask);
    }

    return retval;
}

bool packet_handler_set_interval(packet_handler_t* handler, int32_t interval) {
    bool retval = false;

    when_null(handler, exit);

    handler->interval = interval;
    amxp_timer_set_interval(handler->timer, interval);

    retval = true;
exit:
    return retval;
}
