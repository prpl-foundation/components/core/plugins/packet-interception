/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include <debug/sahtrace.h>
#include <amxp/amxp_dir.h>

#include "packet-interception.h"
#include "netmodel/client.h"
#include "amxo/amxo_mibs.h"
#include "interception/interception.h"
#include "interception/ic_condition.h"
#include "packet-handler/packet-handler.h"
#include "comm-config/comm-config.h"
#include "comm-config/parser.h"

#define ME "packet-interception"

typedef struct {
    common_t common;
    bool enable;
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    amxm_module_t* mod;
    conntrack_t* conntrack;
} packetinterception_app_t;

static packetinterception_app_t app;

void _packetinterception_enabled_changed(UNUSED const char* const sig_name,
                                         const amxc_var_t* const data,
                                         UNUSED void* const priv) {
    app.enable = GETP_BOOL(data, "parameters.Enable.to");

    if(app.enable == false) {
        set_status(&app.common, INTERCEPTION_DISABLED);
    } else {
        set_status(&app.common, INTERCEPTION_ENABLED);
    }

    interception_config_activate_deactivate_all(app.enable);
    ic_condition_activate_deactivate_all(app.enable);
    packet_handler_activate_deactivate_all(app.enable);
}

void _packetinterception_start(UNUSED const char* const sig_name,
                               UNUSED const amxc_var_t* const data,
                               UNUSED void* const priv) {
    parser_init();

    app.enable = amxd_object_get_value(bool, app.common.object, "Enable", NULL);
    if(app.enable) {
        set_status(&app.common, INTERCEPTION_ENABLED);
    } else {
        set_status(&app.common, INTERCEPTION_DISABLED);
    }
}

bool packetinterception_get_enable(void) {
    return app.enable;
}

amxd_dm_t* PRIVATE packetinterception_get_dm(void) {
    return app.dm;
}

amxo_parser_t* PRIVATE packetinterception_get_parser(void) {
    return app.parser;
}

amxm_module_t* PRIVATE packetinterception_get_module(void) {
    return app.mod;
}

conntrack_t* packetinterception_get_conntrack(void) {
    return app.conntrack;
}

uint32_t packetinterception_get_qnum_min_max(bool min) {
    amxc_var_t* netfilter_queue = amxo_parser_get_config(app.parser, "netfilter");
    int qnum = 0;

    if(min) {
        qnum = GET_UINT32(netfilter_queue, "queue_number_min");
    } else {
        qnum = GET_UINT32(netfilter_queue, "queue_number_max");
    }

    if(qnum == 0) {
        if(min) {
            SAH_TRACEZ_WARNING(ME, "Failed to get queue number minimum, using default [%d]", 1);
            qnum = 1;
        } else {
            SAH_TRACEZ_WARNING(ME, "Failed to get queue number maximum, using default [%d]", UINT32_MAX);
            qnum = UINT32_MAX;
        }
    }

    return qnum;
}

uint32_t packetinterception_get_notoffload_mark(void) {
    amxc_var_t* conntrack = amxo_parser_get_config(app.parser, "conntrack");
    int mark = 0;

    mark = GET_UINT32(conntrack, "notoffload_mark");

    return mark;
}

uint32_t packetinterception_get_tcpreset_mark(void) {
    amxc_var_t* conntrack = amxo_parser_get_config(app.parser, "conntrack");
    int mark = 0;

    mark = GET_UINT32(conntrack, "tcpreset_mark");

    return mark;
}

const char* object_da_string(amxd_object_t* const object, const char* name) {
    const char* res = NULL;
    const amxc_var_t* var = NULL;

    when_null(object, out);
    when_str_empty(name, out);

    var = amxd_object_get_param_value(object, name);
    if(var != NULL) {
        res = amxc_var_constcast(cstring_t, var);
    }
out:
    return res;
}

static const char* read_status(status_t status) {
    switch(status) {
    case INTERCEPTION_DISABLED:
        return "Disabled";
    case INTERCEPTION_ERROR:
        return "Error";
    case INTERCEPTION_ERROR_MISCONFIG:
        return "Error_Misconfigured";
    case INTERCEPTION_ENABLED:
    default:
        return "Enabled";
    }
}

void set_status(common_t* c, status_t status) {
    c->status = status;

    when_null(amxd_object_get_param_def(c->object, "Status"), out);

    amxd_object_set_cstring_t(c->object, "Status", read_status(status));
out:
    return;
}

static void conntrack_read_cb(UNUSED int fd, UNUSED void* priv) {
    conntrack_read(app.conntrack);
}

static void packetinterception_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    amxm_shared_object_t* so = amxm_so_get_current();

    app.dm = dm;
    app.parser = parser;
    app.common.object = amxd_dm_findf(dm, "PacketInterception");
    conntrack_new(&app.conntrack, 0);
    amxo_connection_add(parser, conntrack_get_fd(app.conntrack), conntrack_read_cb, NULL, AMXO_CUSTOM, NULL);

    amxm_module_register(&app.mod, so, ME);

    comm_config_init();

    netmodel_initialize();

    amxp_dir_make(amxc_var_constcast(cstring_t, amxo_parser_get_config(app.parser, "socket_path")), 0700);

    SAH_TRACEZ_INFO(ME, "started");
}

static void packetinterception_exit(amxd_dm_t* dm UNUSED, amxo_parser_t* parser UNUSED) {
    amxo_connection_remove(app.parser, conntrack_get_fd(app.conntrack));
    conntrack_delete(&app.conntrack);
    app.dm = NULL;
    app.parser = NULL;

    comm_config_cleanup();
    netmodel_cleanup();
    amxm_close_all();
    SAH_TRACEZ_INFO(ME, "stopped");
}

const char* packetinterception_get_chain(const char* search_path, const char* default_chain) {
    amxc_var_t* iptables = amxo_parser_get_config(packetinterception_get_parser(), "iptables_chains");
    const char* chain = GETP_CHAR(iptables, search_path);
    if(STRING_EMPTY(chain)) {
        SAH_TRACEZ_WARNING(ME, "Failed to get chain, using default [%s]", default_chain);
        chain = default_chain;
    }
    return chain;
}

const char* packetinterception_get_table(const char* search_path, const char* default_table) {
    amxc_var_t* iptables = amxo_parser_get_config(packetinterception_get_parser(), "iptables_tables");
    const char* table = GETP_CHAR(iptables, search_path);
    if(STRING_EMPTY(table)) {
        SAH_TRACEZ_WARNING(ME, "Failed to get table, using default [%s]", default_table);
        table = default_table;
    }
    return table;
}

const char* packetinterception_get_parser_module_dir(const char* default_dir) {
    amxc_var_t* dir_var = amxo_parser_get_config(packetinterception_get_parser(), "directories");
    const char* module_dir = GETP_CHAR(dir_var, "parser_module_dir");
    if(STRING_EMPTY(module_dir)) {
        SAH_TRACEZ_WARNING(ME, "Failed to get parser module directory, using default [%s]", default_dir);
        module_dir = default_dir;
    }
    return module_dir;
}

const char* packetinterception_get_comm_config_module_dir(const char* default_dir) {
    amxc_var_t* dir_var = amxo_parser_get_config(packetinterception_get_parser(), "directories");
    const char* module_dir = GETP_CHAR(dir_var, "comm_config_module_dir");
    if(STRING_EMPTY(module_dir)) {
        SAH_TRACEZ_WARNING(ME, "Failed to get comm config module directory, using default [%s]", default_dir);
        module_dir = default_dir;
    }
    return module_dir;
}

const char* packetinterception_get_comm_config_mib_dir(const char* default_dir) {
    amxc_var_t* dir_var = amxo_parser_get_config(packetinterception_get_parser(), "directories");
    const char* mib_dir = GETP_CHAR(dir_var, "comm_config_mib_dir");
    if(STRING_EMPTY(mib_dir)) {
        SAH_TRACEZ_WARNING(ME, "Failed to get comm config mib directory, using default [%s]", default_dir);
        mib_dir = default_dir;
    }
    return mib_dir;
}

int _packetinterception_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    SAH_TRACEZ_INFO(ME, "entry point [%s], reason: [%d]", __func__, reason);
    switch(reason) {
    case 0:     // START
        packetinterception_init(dm, parser);
        break;
    case 1:     // STOP
        packetinterception_exit(dm, parser);
        break;
    default:
        break;
    }

    return 0;
}
