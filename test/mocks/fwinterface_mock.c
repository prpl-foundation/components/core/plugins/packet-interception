/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <fwinterface/interface.h>

#include "mock.h"
#include "fwinterface_mock.h"

bool rule_is_being_deleted = false;

int __wrap_fw_add_chain(const char* chain, const char* table, UNUSED bool is_ipv6) {
    check_expected(chain);
    check_expected(table);
    return 0;
}

int __wrap_fw_add_rule(const fw_rule_t* const rule, UNUSED uint32_t index) {
    assert_non_null(rule);
    return 0;
}

int __wrap_fw_replace_rule(const fw_rule_t* const rule, uint32_t index) {
    fprintf(stdout, "fw_replace_rule: %" PRIu32 "\n", index);

    check_expected(rule);
    check_expected(index);

    return 0;
}

int __wrap_fw_delete_rule(const fw_rule_t* const rule, uint32_t index) {
    fprintf(stdout, "fw_delete_rule: %" PRIu32 "\n", index);

    rule_is_being_deleted = true;
    check_expected(rule);
    rule_is_being_deleted = false;

    check_expected(index);

    return 0;
}

int __wrap_fw_apply(void) {
    return 0;
}

int rule_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data) {
    const fw_rule_t* a = (const fw_rule_t*) value;
    const fw_rule_t* b = (const fw_rule_t*) check_value_data;
    fw_rule_dump(a, STDOUT_FILENO);
    fw_rule_dump(b, STDOUT_FILENO);

    string_equal(fw_rule_get_table(a), fw_rule_get_table(b));
    string_equal(fw_rule_get_chain(a), fw_rule_get_chain(b));
    string_equal(fw_rule_get_in_interface(a), fw_rule_get_in_interface(b));
    string_equal(fw_rule_get_out_interface(a), fw_rule_get_out_interface(b));
    string_equal(fw_rule_get_source(a), fw_rule_get_source(b));
    string_equal(fw_rule_get_source_mask(a), fw_rule_get_source_mask(b));
    string_equal(fw_rule_get_destination(a), fw_rule_get_destination(b));
    string_equal(fw_rule_get_destination_mask(a), fw_rule_get_destination_mask(b));
    string_equal(fw_rule_get_source_mac_address(a), fw_rule_get_source_mac_address(b));
    if((!rule_is_being_deleted && (fw_rule_is_enabled(a) != fw_rule_is_enabled(b))) ||
       (fw_rule_get_ipv4(a) != fw_rule_get_ipv4(b)) ||
       (fw_rule_get_protocol(a) != fw_rule_get_protocol(b)) ||
       (fw_rule_get_target(a) != fw_rule_get_target(b)) ||
       (fw_rule_get_icmp_type(a) != fw_rule_get_icmp_type(b)) ||
       (fw_rule_get_source_port(a) != fw_rule_get_source_port(b)) ||
       (fw_rule_get_source_port_range_max(a) != fw_rule_get_source_port_range_max(b)) ||
       (fw_rule_get_destination_port(a) != fw_rule_get_destination_port(b)) ||
       (fw_rule_get_destination_port_range_max(a) != fw_rule_get_destination_port_range_max(b)) ||
       (fw_rule_get_in_interface_excluded(a) != fw_rule_get_in_interface_excluded(b)) ||
       (fw_rule_get_out_interface_excluded(a) != fw_rule_get_out_interface_excluded(b)) ||
       (fw_rule_get_source_excluded(a) != fw_rule_get_source_excluded(b)) ||
       (fw_rule_get_destination_excluded(a) != fw_rule_get_destination_excluded(b)) ||
       (fw_rule_get_source_port_excluded(a) != fw_rule_get_source_port_excluded(b)) ||
       (fw_rule_get_destination_port_excluded(a) != fw_rule_get_destination_port_excluded(b)) ||
       (fw_rule_get_source_mac_excluded(a) != fw_rule_get_source_mac_excluded(b)) ||
       (fw_rule_get_dscp(a) != fw_rule_get_dscp(b)) ||
       (fw_rule_get_dscp_excluded(a) != fw_rule_get_dscp_excluded(b))) {
        return 0;
    }

    if(fw_rule_get_target(a) == FW_RULE_TARGET_RETURN) {
        if(fw_rule_get_target(b) != FW_RULE_TARGET_RETURN) {
            return 0;
        }
    } else if(fw_rule_get_target(a) == FW_RULE_TARGET_POLICY) {
        if(fw_rule_get_target_policy_option(a) != fw_rule_get_target_policy_option(b)) {
            return 0;
        }
    } else if(fw_rule_get_target(a) == FW_RULE_TARGET_CHAIN) {
        if(strcmp(fw_rule_get_target_chain_option(a), fw_rule_get_target_chain_option(b)) != 0) {
            return 0;
        }
    } else if(fw_rule_get_target(a) == FW_RULE_TARGET_NFQUEUE) {
        uint32_t a_qnum = 0;
        uint32_t a_qtotal = 0;
        uint32_t a_flags = 0;
        uint32_t b_qnum = 0;
        uint32_t b_qtotal = 0;
        uint32_t b_flags = 0;
        fw_rule_get_target_nfqueue_options(a, &a_qnum, &a_qtotal, &a_flags);
        fw_rule_get_target_nfqueue_options(b, &b_qnum, &b_qtotal, &b_flags);
        if((a_qnum != b_qnum) ||
           (a_qtotal != b_qtotal) ||
           (a_flags != b_flags)) {
            return 0;
        }
    } else if(fw_rule_get_target(a) == FW_RULE_TARGET_NFLOG) {
        uint32_t a_gnum = 0;
        uint32_t a_len = 0;
        uint32_t a_thresh = 0;
        uint32_t a_flags = 0;
        const char* a_prefix = NULL;
        uint32_t b_gnum = 0;
        uint32_t b_len = 0;
        uint32_t b_thresh = 0;
        uint32_t b_flags = 0;
        const char* b_prefix = NULL;
        fw_rule_get_target_nflog_options(a, &a_gnum, &a_len, &a_thresh, &a_flags, &a_prefix);
        fw_rule_get_target_nflog_options(b, &b_gnum, &b_len, &b_thresh, &b_flags, &b_prefix);
        if((a_gnum != b_gnum) ||
           (a_len != b_len) ||
           (a_thresh != b_thresh) ||
           (a_flags != b_flags) ||
           (strcmp(a_prefix, b_prefix) != 0)) {
            return 0;
        }
    } else {
        fprintf(stdout, "UNKNOWN TARGET not compared (%s:%d %s)!\n", __func__, __LINE__, __FILE__);
        return 0;
    }

    return 1;
}

int mock_fw_rule_new(fw_rule_t** rule) {
    assert_int_equal(fw_rule_new(rule), 0);

    /**
     * Remove the new rule because otherwise the test influences the behavior
     * of lib_fwrules and the firewall manager. Lib_fwrules adds all new rules
     * to a global list. Function fw_commit also triggers the callback on these
     * "test" rules.
     */
    amxc_llist_it_take(&(*rule)->g_it);
    return 0;
}

void duplicate_fw_rule(fw_rule_t* dest, const fw_rule_t* src) {
    // Create a copy of the original rule and also create a copy of the hash table.
    // Otherwise the hash table is shared between the copy and the original.
    // The variant dest->ht needs to be freed with amxc_var_delete.
    assert_non_null(memcpy(dest, src, sizeof(fw_rule_t)));
    amxc_var_new(&dest->ht);
    assert_int_equal(amxc_var_copy(dest->ht, src->ht), 0);
}
