/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __TEST_FWINTERFACE_MOCK_H__
#define __TEST_FWINTERFACE_MOCK_H__

/**
 * Hack to be able to remove the iterator from the global list of rules.
 * Must be in sync with the struct from libfwrules.
 */
#include <fwrules/fw_features.h>
#include <fwrules/fw_rule.h>
typedef struct _fw_rule {
    amxc_llist_it_t it;
    amxc_llist_it_t g_it;
    fw_rule_flag_t flag;
    bool enabled;
    int feature_marks;
    amxc_var_t* ht;
    fw_feature_t current_feature;
    int traversed;
} fw_rule_t;

// use macro so the cmocka error points to the correct line
#define expect_fw_add_chain(CHAIN, TABLE) do { \
        expect_string(__wrap_fw_add_chain, chain, CHAIN); \
        expect_string(__wrap_fw_add_chain, table, TABLE); \
} while(0);

// use macro so the cmocka error points to the correct line
#define expect_fw_replace_rule(RULE, INDEX) do { \
        expect_check(__wrap_fw_replace_rule, rule, rule_equal_check, RULE); \
        expect_value(__wrap_fw_replace_rule, index, INDEX); \
} while(0);

// use macro so the cmocka error points to the correct line
#define expect_fw_delete_rule(RULE, INDEX) do { \
        expect_check(__wrap_fw_delete_rule, rule, rule_equal_check, RULE); \
        expect_value(__wrap_fw_delete_rule, index, INDEX); \
} while(0);

int __wrap_fw_add_chain(const char* chain, const char* table, UNUSED bool is_ipv6);
int __wrap_fw_add_rule(const fw_rule_t* const rule, UNUSED uint32_t index);
int __wrap_fw_replace_rule(const fw_rule_t* const rule, uint32_t index);
int __wrap_fw_delete_rule(const fw_rule_t* const rule, uint32_t index);
int __wrap_fw_apply(void);

int rule_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data);

int mock_fw_rule_new(fw_rule_t** rule);
void duplicate_fw_rule(fw_rule_t* dest, const fw_rule_t* src);

#endif
