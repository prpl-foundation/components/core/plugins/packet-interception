/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <fwrules/fw_rule.h>

#include "condition/dm_condition.h"
#include "interception/dm_interception.h"
#include "packet-handler/dm_packet-handler.h"

#include "mock.h"

static amxd_object_t* root_obj = NULL;

static const char* odl_defs = "test_packetinterception.odl";

static amxd_status_t _dummy_validator(UNUSED amxd_object_t* object,
                                      UNUSED amxd_param_t* param,
                                      UNUSED amxd_action_t reason,
                                      UNUSED const amxc_var_t* const args,
                                      UNUSED amxc_var_t* const retval,
                                      UNUSED void* priv) {
    return amxd_status_ok;
}

amxd_object_t* mock_get_root_obj(void) {
    return root_obj;
}

void mock_init(void** state) {
    amxut_bus_setup(state);

    // actions
    amxut_resolve_function("ic_instance_cleanup", AMXO_FUNC(_ic_instance_cleanup));
    amxut_resolve_function("ic_cond_instance_cleanup", AMXO_FUNC(_ic_cond_instance_cleanup));
    amxut_resolve_function("ic_comm_config_instance_cleanup", AMXO_FUNC(_ic_comm_config_instance_cleanup));

    amxut_resolve_function("cond_instance_cleanup", AMXO_FUNC(_cond_instance_cleanup));

    amxut_resolve_function("handler_instance_cleanup", AMXO_FUNC(_handler_instance_cleanup));
    amxut_resolve_function("handler_stats_read", AMXO_FUNC(_handler_stats_read));
    amxut_resolve_function("handler_stats_list", AMXO_FUNC(_handler_stats_list));
    amxut_resolve_function("handler_stats_describe", AMXO_FUNC(_handler_stats_describe));

    //events
    amxut_resolve_function("ic_start", AMXO_FUNC(_ic_start));
    amxut_resolve_function("ic_stop", AMXO_FUNC(_ic_stop));
    amxut_resolve_function("ic_added", AMXO_FUNC(_ic_added));
    amxut_resolve_function("ic_changed", AMXO_FUNC(_ic_changed));
    amxut_resolve_function("ic_cond_added", AMXO_FUNC(_ic_cond_added));
    amxut_resolve_function("ic_cond_changed", AMXO_FUNC(_ic_cond_changed));
    amxut_resolve_function("ic_comm_config_added", AMXO_FUNC(_ic_comm_config_added));
    amxut_resolve_function("ic_comm_config_changed", AMXO_FUNC(_ic_comm_config_changed));

    amxut_resolve_function("cond_start", AMXO_FUNC(_cond_start));
    amxut_resolve_function("cond_stop", AMXO_FUNC(_cond_stop));
    amxut_resolve_function("cond_added", AMXO_FUNC(_cond_added));
    amxut_resolve_function("cond_changed", AMXO_FUNC(_cond_changed));

    amxut_resolve_function("handler_start", AMXO_FUNC(_handler_start));
    amxut_resolve_function("handler_stop", AMXO_FUNC(_handler_stop));
    amxut_resolve_function("handler_added", AMXO_FUNC(_handler_added));
    amxut_resolve_function("handler_changed", AMXO_FUNC(_handler_changed));

    amxut_resolve_function("packetinterception_start", AMXO_FUNC(_packetinterception_start));
    amxut_resolve_function("packetinterception_enabled_changed", AMXO_FUNC(_packetinterception_enabled_changed));

    //checks
    amxut_resolve_function("cond_check_IP", AMXO_FUNC(_cond_check_IP));
    amxut_resolve_function("cond_list_check_protocol", AMXO_FUNC(_cond_list_check_protocol));
    amxut_resolve_function("cond_list_check_port", AMXO_FUNC(_cond_list_check_port));
    amxut_resolve_function("cond_list_check_connstate", AMXO_FUNC(_cond_list_check_connstate));
    amxut_resolve_function("is_valid_macaddr", AMXO_FUNC(_dummy_validator));

    amxut_resolve_function("handler_check_mark", AMXO_FUNC(_handler_check_mark));

    amxut_dm_load_odl(odl_defs, root_obj);

    amxut_bus_handle_events();
}

void mock_cleanup(void** state) {
    amxut_bus_teardown(state);
}
