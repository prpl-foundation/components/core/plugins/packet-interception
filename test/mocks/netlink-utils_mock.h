/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __TEST_NETLINK_UTILS_MOCK_H__
#define __TEST_NETLINK_UTILS_MOCK_H__

#include "netlink-utils/nfqueue.h"
#include "netlink-utils/nflog.h"
#include "netlink-utils/conntrack.h"

int __wrap_nfqueue_new(nfqueue_t** nfqueue, uint16_t qnum, int8_t ip_version, bool conntrack_info);
void __wrap_nfqueue_delete(nfqueue_t** nfqueue, bool close_socket);
int __wrap_nfqueue_get_fd(nfqueue_t* nfqueue);
int __wrap_nfqueue_read(nfqueue_t* nfqueue, nfqueue_data_t* nfqdata, packet_data_t** packet);
int __wrap_nfqueue_judge_packet(nfqueue_t* nfqueue, uint32_t pkt_id, uint32_t pkt_verdict);
int __wrap_nfqueue_judge_packet_mark(nfqueue_t* nfqueue, uint32_t pkt_id, uint32_t pkt_verdict, uint32_t pkt_mark, uint32_t pkt_connmark);

int __wrap_nflog_new(nflog_t** nflog, uint16_t gnum, int8_t ip_version, bool conntrack_info);
void __wrap_nflog_delete(nflog_t** nflog, bool close_socket);
int __wrap_nflog_get_fd(nflog_t* nflog);

int __wrap_conntrack_new(conntrack_t** conntrack, int group);
void __wrap_conntrack_delete(conntrack_t** conntrack);
int __wrap_conntrack_get_fd(conntrack_t* conntrack);
int __wrap_conntrack_get_connection_info(conntrack_t* conntrack, conntrack_data_t* data);

#endif // __TEST_NETLINK_UTILS_MOCK_H__