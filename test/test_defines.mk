MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../include)

HEADERS = $(wildcard $(INCDIR)/$(TARGET)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c) \
		  $(wildcard $(SRCDIR)/interception/*.c) \
		  $(wildcard $(SRCDIR)/condition/*.c) \
		  $(wildcard $(SRCDIR)/packet-handler/*.c) \
		  $(wildcard $(SRCDIR)/comm-config/*.c)

MOCK_WRAP = fw_add_chain \
			fw_add_rule \
			fw_replace_rule \
			fw_delete_rule \
			fw_apply \
			nfqueue_judge_packet \
			nfqueue_judge_packet_mark \
			nfqueue_new \
			nfqueue_delete \
			nfqueue_get_fd \
			nfqueue_read \
			nflog_new \
			nflog_delete \
			nflog_get_fd \
			conntrack_get_connection_info \
			conntrack_new \
			conntrack_delete \
			conntrack_get_fd \
			packet_parse_flow_info \
			ic_condition_find_by_qnum

WRAP_FUNC=-Wl,--wrap=

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. -I../mocks \
		  -fkeep-inline-functions -fkeep-static-functions -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread \
			-DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) -lamxc -lamxp -lamxd -lamxb -lamxo -lamxj -lamxm -lamxut \
		   -ldl -lpthread -lsahtrace -lfwrules -lipat -lnetlink-utils -lnetmodel -lpacket-interception

LDFLAGS += -g $(addprefix $(WRAP_FUNC),$(MOCK_WRAP))
