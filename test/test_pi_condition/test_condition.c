/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "condition/dm_condition.h"

#include <amxd/amxd_object_expression.h>

#include "mock.h"
#include "test_condition.h"
#include "condition/condition.h"

#define TEST_CONDITION      "cond-test"

int test_condition_setup(UNUSED void** state) {
    amxd_dm_t* dm = NULL;

    mock_init(state);

    dm = amxut_bus_dm();

    assert_int_equal(_packetinterception_main(0, amxut_bus_dm(), amxut_bus_parser()), 0);
    amxp_sigmngr_emit_signal(&dm->sigmngr, "app:start", NULL);

    amxut_bus_handle_events();

    return 0;
}

int test_condition_teardown(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();

    amxp_sigmngr_emit_signal(&dm->sigmngr, "app:stop", NULL);

    amxut_bus_handle_events();

    assert_int_equal(_packetinterception_main(1, dm, amxut_bus_parser()), 0);

    amxut_bus_handle_events();

    mock_cleanup(state);

    return 0;
}

void test_condition_dm_create(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    fprintf(stdout, "add PacketInterception.Condition instance\n");
    amxd_trans_select_pathf(&trans, "PacketInterception.Condition");
    amxd_trans_add_inst(&trans, 0, TEST_CONDITION);
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    amxd_trans_set_value(cstring_t, &trans, "ConnectionState", "NEW");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_select_pathf(&trans, "PacketInterception.Condition");
    amxd_trans_clean(&trans);

    object = amxd_dm_findf(dm, "PacketInterception.Condition.1");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);

    amxc_var_clean(&params);
}

void test_condition_dm_delete(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    object = amxd_dm_findf(dm, "PacketInterception.Condition.1");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);

    amxc_var_clean(&params);

    fprintf(stdout, "delete instance, allocated memory should be freed\n");
    amxd_trans_select_pathf(&trans, "PacketInterception.Condition");
    amxd_trans_del_inst(&trans, 0, TEST_CONDITION);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    fprintf(stdout, "cleanup\n");
}

void test_condition_dm_set_ip(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* object = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    fprintf(stdout, "Set Source IP of condition to not allowed value [Hello World], should be refused\n");
    object = amxd_dm_findf(dm, "PacketInterception.Condition.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "SourceIP", "Hello World");
    assert_int_not_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    fprintf(stdout, "Set Source IP of condition to allowed value [192.168.1.1], should be accepted\n");
    object = amxd_dm_findf(dm, "PacketInterception.Condition.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "SourceIP", "192.168.1.1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
}

void test_condition_dm_set_protocol(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* object = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    fprintf(stdout, "Set Protocol of condition to not allowed string value [Hello World], should be refused\n");
    object = amxd_dm_findf(dm, "PacketInterception.Condition.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "Hello World");
    assert_int_not_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    fprintf(stdout, "Set Protocol of condition to allowed string value [UDP], should be accepted\n");
    object = amxd_dm_findf(dm, "PacketInterception.Condition.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "UDP");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    fprintf(stdout, "Set Protocol of condition to not allowed numeric value [113], should be refused\n");
    object = amxd_dm_findf(dm, "PacketInterception.Condition.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "113");
    assert_int_not_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    fprintf(stdout, "Set Protocol of condition to allowed numeric value [6], should be accepted\n");
    object = amxd_dm_findf(dm, "PacketInterception.Condition.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "6");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
}

void test_condition_dm_set_port(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* object = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    fprintf(stdout, "Set Destination Port of condition to not allowed string value [Hello World], should be refused\n");
    object = amxd_dm_findf(dm, "PacketInterception.Condition.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "DestPort", "Hello World");
    assert_int_not_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    fprintf(stdout, "Set Destination Port of condition to allowed string value [53], should be accepted\n");
    object = amxd_dm_findf(dm, "PacketInterception.Condition.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "DestPort", "53");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
}

void test_condition_dm_set_connstate(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_object_t* object = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    fprintf(stdout, "Set ConnectionState of condition to not allowed string value [Hello World], should be refused\n");
    object = amxd_dm_findf(dm, "PacketInterception.Condition.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "ConnectionState", "Hello World");
    assert_int_not_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    fprintf(stdout, "Set ConnectionState of condition to allowed string value [NEW], should be accepted\n");
    object = amxd_dm_findf(dm, "PacketInterception.Condition.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "ConnectionState", "NEW");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    fprintf(stdout, "Set ConnectionState of condition to allowed string value [NEW, INVALID], should be accepted\n");
    object = amxd_dm_findf(dm, "PacketInterception.Condition.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "ConnectionState", "NEW, INVALID");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
}
