/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "interception/dm_interception.h"

#include <amxd/amxd_object_expression.h>

#include "fwinterface/interface.h"

#include "mock.h"
#include "fwinterface_mock.h"
#include "test_dm_interception.h"
#include "interception/interception.h"
#include "test_ic_setup.h"

void test_ic_condition_dm_create(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    fprintf(stdout, "add PacketInterception.Interception.1.Intercept instance, status will be Disabled by default\n");
    amxd_trans_select_pathf(&trans, "PacketInterception.Interception.1.Intercept");
    amxd_trans_add_inst(&trans, 0, TEST_IC_CONDITION1);
    amxd_trans_set_value(cstring_t, &trans, "Condition", TEST_CONDITION_1);
    amxd_trans_set_value(cstring_t, &trans, "PacketHandler", TEST_HANDLER);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    object = amxd_dm_findf(amxut_bus_dm(), "PacketInterception.Interception.1.Intercept.1");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    fprintf(stdout, "add PacketInterception.Interception.1.Intercept instance, status will be Disabled by default\n");
    amxd_trans_select_pathf(&trans, "PacketInterception.Interception.1.Intercept");
    amxd_trans_add_inst(&trans, 0, TEST_IC_CONDITION2);
    amxd_trans_set_value(cstring_t, &trans, "Condition", TEST_CONDITION_2);
    amxd_trans_set_value(cstring_t, &trans, "PacketHandler", TEST_HANDLER);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    object = amxd_dm_findf(amxut_bus_dm(), "PacketInterception.Interception.1.Intercept.2");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    fprintf(stdout, "add PacketInterception.Interception.1.Bypass instance, status will be Disabled by default\n");
    amxd_trans_select_pathf(&trans, "PacketInterception.Interception.1.Bypass");
    amxd_trans_add_inst(&trans, 0, TEST_BP_CONDITION);
    amxd_trans_set_value(cstring_t, &trans, "Condition", TEST_CONDITION_3);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    object = amxd_dm_findf(amxut_bus_dm(), "PacketInterception.Interception.1.Bypass.1");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void test_ic_condition_dm_enable(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;
    fw_rule_t* r1 = setup_get_rule(2);
    fw_rule_t* r2 = setup_get_rule(3);
    fw_rule_t* r3 = setup_get_rule(4);
    uint32_t qnum = packetinterception_get_qnum_min_max(true);

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    object = amxd_dm_findf(amxut_bus_dm(), "PacketInterception.Condition.1");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);

    expect_any(__wrap_nfqueue_new, nfqueue);
    expect_value(__wrap_nfqueue_new, qnum, qnum);
    expect_value(__wrap_nfqueue_new, ip_version, 0);
    expect_value(__wrap_nfqueue_new, conntrack_info, true);
    will_return(__wrap_nfqueue_new, 0);

    expect_any(__wrap_nfqueue_get_fd, nfqueue);
    will_return(__wrap_nfqueue_get_fd, 5);

    fprintf(stdout, "enable the PacketInterception.Interception.1.Intercept.1 instance, expect status to be 'Enabled'\n");
    object = amxd_dm_findf(amxut_bus_dm(), "PacketInterception.Interception.1.Intercept.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_replace_rule(r1, 1);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    expect_any(__wrap_nfqueue_new, nfqueue);
    expect_value(__wrap_nfqueue_new, qnum, qnum + 1);
    expect_value(__wrap_nfqueue_new, ip_version, 0);
    expect_value(__wrap_nfqueue_new, conntrack_info, true);
    will_return(__wrap_nfqueue_new, 0);

    expect_any(__wrap_nfqueue_get_fd, nfqueue);
    will_return(__wrap_nfqueue_get_fd, 5);

    fprintf(stdout, "enable the PacketInterception.Interception.1.Intercept.2 instance, expect status to be 'Enabled'\n");
    object = amxd_dm_findf(amxut_bus_dm(), "PacketInterception.Interception.1.Intercept.2");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_replace_rule(r2, 2);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    fprintf(stdout, "enable the PacketInterception.Interception.1.Bypass instance, expect status to be 'Enabled'\n");
    object = amxd_dm_findf(amxut_bus_dm(), "PacketInterception.Interception.1.Bypass.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_replace_rule(r3, 1);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void test_ic_condition_dm_delete(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    fw_rule_t* r1 = setup_get_rule(2);
    fw_rule_t* r2 = setup_get_rule(3);
    fw_rule_t* r3 = setup_get_rule(4);

    amxd_trans_init(&trans);

    fprintf(stdout, "delete instance, allocated memory should be freed\n");
    expect_fw_delete_rule(r1, 1);
    amxd_trans_select_pathf(&trans, "PacketInterception.Interception.1.Intercept.");
    amxd_trans_del_inst(&trans, 0, TEST_IC_CONDITION1);

    expect_any(__wrap_nfqueue_get_fd, nfqueue);
    will_return(__wrap_nfqueue_get_fd, 5);
    expect_any(__wrap_nfqueue_delete, *nfqueue);
    expect_value(__wrap_nfqueue_delete, close_socket, true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();

    fprintf(stdout, "delete instance, allocated memory should be freed\n");
    expect_fw_delete_rule(r2, 1);
    amxd_trans_select_pathf(&trans, "PacketInterception.Interception.1.Intercept.");
    amxd_trans_del_inst(&trans, 0, TEST_IC_CONDITION2);

    expect_any(__wrap_nfqueue_get_fd, nfqueue);
    will_return(__wrap_nfqueue_get_fd, 5);
    expect_any(__wrap_nfqueue_delete, *nfqueue);
    expect_value(__wrap_nfqueue_delete, close_socket, true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();

    amxd_trans_clean(&trans);

    fprintf(stdout, "delete instance, allocated memory should be freed\n");
    expect_fw_delete_rule(r3, 1);
    amxd_trans_select_pathf(&trans, "PacketInterception.Interception.1.Bypass.");
    amxd_trans_del_inst(&trans, 0, TEST_BP_CONDITION);

    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();

    amxd_trans_clean(&trans);

    fprintf(stdout, "cleanup\n");
}

void test_interception_dm_create(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    fprintf(stdout, "add PacketInterception.Interception instance, status will be Disabled by default\n");
    amxd_trans_select_pathf(&trans, "PacketInterception.Interception.");
    amxd_trans_add_inst(&trans, 0, TEST_INTERCEPTION);
    amxd_trans_set_value(cstring_t, &trans, "TrafficRoute", TEST_TABLE);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_add_chain(TEST_CHAIN_NAME, packetinterception_get_table("interception", "mangle"));
    expect_fw_add_chain(TEST_CHAIN6_NAME, packetinterception_get_table("interception", "mangle"));
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    object = amxd_dm_findf(dm, "PacketInterception.Interception.1");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void test_interception_dm_enable(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;
    fw_rule_t* r1 = setup_get_rule(0);
    fw_rule_t* r2 = setup_get_rule(1);

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    fprintf(stdout, "enable the PacketInterception.Interception instance, expect status to be 'Enabled'\n");
    object = amxd_dm_findf(dm, "PacketInterception.Interception.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_replace_rule(r1, 1);
    expect_fw_replace_rule(r2, 1);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    object = amxd_dm_findf(dm, "PacketInterception.Interception.1");
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void test_interception_dm_delete(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    fw_rule_t* r1 = setup_get_rule(0);
    fw_rule_t* r2 = setup_get_rule(1);

    amxd_trans_init(&trans);

    fprintf(stdout, "delete instance, allocated memory should be freed\n");
    expect_fw_delete_rule(r1, 1);
    expect_fw_delete_rule(r2, 1);
    amxd_trans_select_pathf(&trans, "PacketInterception.Interception.");
    amxd_trans_del_inst(&trans, 0, TEST_INTERCEPTION);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    fprintf(stdout, "cleanup\n");
}
