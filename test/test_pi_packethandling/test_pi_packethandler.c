/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "packet-interception.h"
#include "packet-interception/variant_packet_verdict.h"
#include "packet-handler/packet-handler.h"

#include "test_pi_packethandler.h"
#include "mock.h"
#include "ic_condition_mock.h"
#include "test_setup.h"

#define TEST_HANDLER    "handler-test"

static ic_condition_t* ic_cond = NULL;

void test_packethandler_dm_create(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    fprintf(stdout, "add PacketInterception.PacketHandler instance, status will be Disabled by default\n");
    amxd_trans_select_pathf(&trans, "PacketInterception.PacketHandler.");
    amxd_trans_add_inst(&trans, 0, TEST_HANDLER);
    amxd_trans_set_value(int32_t, &trans, "Timeout", 100);
    amxd_trans_set_value(cstring_t, &trans, "DefaultVerdict", "DROP");
    amxd_trans_set_value(cstring_t, &trans, "DefaultMark", "1/1");

    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    object = amxd_dm_findf(amxut_bus_dm(), "PacketInterception.PacketHandler.1");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    ic_cond = test_ic_condition_get();

    ic_cond->handler = object->priv;
    ic_cond->qnum = 1;
    ic_cond->nr_of_packets = 1;

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void test_packethandler_default_action(UNUSED void** state) {
    //mock a packet
    expect_any(__wrap_nfqueue_read, nfqueue);
    will_return(__wrap_nfqueue_read, 1);
    will_return(__wrap_nfqueue_read, 1);
    will_return(__wrap_nfqueue_read, 0);
    will_return(__wrap_nfqueue_read, 0);
    will_return(__wrap_nfqueue_read, 6);
    will_return(__wrap_nfqueue_read, "Hello World");
    will_return(__wrap_nfqueue_read, 0);

    expect_any(__wrap_nfqueue_judge_packet, nfqueue);
    expect_value(__wrap_nfqueue_judge_packet, pkt_id, 1);
    expect_value(__wrap_nfqueue_judge_packet, pkt_verdict, NF_ACCEPT);
    will_return(__wrap_nfqueue_judge_packet, 0);

    packet_handler_packet_cb(5, ic_cond);
}

void test_packethandler_dm_enable(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    fprintf(stdout, "enable the PacketInterception.PacketHandler instance, expect status to be 'Enabled'\n");
    object = amxd_dm_findf(amxut_bus_dm(), "PacketInterception.PacketHandler.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void test_packethandler_timeout(UNUSED void** state) {
    amxd_object_t* object = NULL;
    uint32_t packets_received = 0;
    uint32_t packets_timeout = 0;

    // Mock a packet
    expect_any(__wrap_nfqueue_read, nfqueue);
    will_return(__wrap_nfqueue_read, 5);
    will_return(__wrap_nfqueue_read, 3);
    will_return(__wrap_nfqueue_read, 0);
    will_return(__wrap_nfqueue_read, 0);
    will_return(__wrap_nfqueue_read, 17);
    will_return(__wrap_nfqueue_read, "Hello World");
    will_return(__wrap_nfqueue_read, 0);

    // Check if the mock comm config will send out the packet
    expect_string(dummy_send_packet, name, "dummy_1");
    expect_value(dummy_send_packet, id, 3);
    expect_value(dummy_send_packet, group, 5);

    // Mock a new packet being intercepted and check if timer is started
    packet_handler_packet_cb(5, ic_cond);
    amxut_bus_handle_events();
    assert_int_equal(amxp_timer_get_state(ic_cond->handler->timer), amxp_timer_running);

    // Check that the test intercept condition will be looked up after timeout
    expect_value(__wrap_ic_condition_find_by_qnum, qnum, 5);

    // Check that the default verdict will be given for the packet after timeout
    expect_any(__wrap_nfqueue_judge_packet_mark, nfqueue);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_id, 3);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_verdict, NF_DROP);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_mark, 1);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_connmark, 0 | packetinterception_get_notoffload_mark());
    will_return(__wrap_nfqueue_judge_packet_mark, 0);

    amxut_timer_go_to_future_ms(1000000);

    // Check that the correct statistics have increased
    object = amxd_dm_findf(amxut_bus_dm(), "PacketInterception.PacketHandler.1.Stats");
    packets_received = amxd_object_get_value(uint32_t, object, "NrOfPacketsReceived", NULL);
    packets_timeout = amxd_object_get_value(uint32_t, object, "NrOfPacketsTimedout", NULL);

    assert_int_equal(packets_received, 1);
    assert_int_equal(packets_timeout, 1);
}

void test_packethandler_reply(UNUSED void** state) {
    amxd_object_t* object = NULL;
    uint32_t packets_received = 0;
    uint32_t packets_handled = 0;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* verdict_var = NULL;
    packet_verdict_t verdict;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    // Mock a packet
    expect_any(__wrap_nfqueue_read, nfqueue);
    will_return(__wrap_nfqueue_read, 5);
    will_return(__wrap_nfqueue_read, 65536 + 4);
    will_return(__wrap_nfqueue_read, 0);
    will_return(__wrap_nfqueue_read, 0);
    will_return(__wrap_nfqueue_read, 17);
    will_return(__wrap_nfqueue_read, "Hello World");
    will_return(__wrap_nfqueue_read, 0);

    // Check if the mock comm config will send out the packet
    expect_string(dummy_send_packet, name, "dummy_1");
    expect_value(dummy_send_packet, id, 65536 + 4);
    expect_value(dummy_send_packet, group, 5);

    // Mock a new packet being intercepted
    packet_handler_packet_cb(5, ic_cond);
    amxut_bus_handle_events();

    // Check that the test intercept condition will be looked up after a mock reply is received
    expect_value(__wrap_ic_condition_find_by_qnum, qnum, 5);

    // // Check that a custom verdict will be given for the packet after a mock reply is received
    expect_any(__wrap_nfqueue_judge_packet_mark, nfqueue);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_id, 65536 + 4);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_verdict, NF_ACCEPT);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_mark, 3);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_connmark, 0 | packetinterception_get_notoffload_mark());
    will_return(__wrap_nfqueue_judge_packet_mark, 0);

    //Send out a reply from the mock config
    verdict.verdict = NF_ACCEPT;
    verdict.mark = 3;
    verdict.mark_mask = 3;
    verdict.connmark = 0;
    verdict.connmark_mask = 0;

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Communication Config", "dummy_1");
    amxc_var_add_key(uint32_t, &args, "ID", 65536 + 4);
    amxc_var_add_key(uint32_t, &args, "Group", 5);
    verdict_var = amxc_var_add_new_key(&args, "Verdict");
    amxc_var_set_packet_verdict_t(verdict_var, &verdict);
    assert_int_equal(amxm_execute_function("self", "packet-interception", "comm-config-verdict", &args, &ret), 0);

    amxut_bus_handle_events();

    // Check that the correct statistics have increased
    object = amxd_dm_findf(amxut_bus_dm(), "PacketInterception.PacketHandler.1.Stats");
    packets_received = amxd_object_get_value(uint32_t, object, "NrOfPacketsReceived", NULL);
    packets_handled = amxd_object_get_value(uint32_t, object, "NrOfPacketsHandled", NULL);

    assert_int_equal(packets_received, 2);
    assert_int_equal(packets_handled, 1);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}



void test_packethandler_set_connmark(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    fprintf(stdout, "Change PacketInterception.PacketHandler instance, expect status to be 'Enabled'\n");
    object = amxd_dm_findf(amxut_bus_dm(), "PacketInterception.PacketHandler.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "DefaultMark", "");
    amxd_trans_set_value(cstring_t, &trans, "DefaultConnMark", "4/4");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void test_packethandler_timeout_connmark(UNUSED void** state) {
    amxd_object_t* object = NULL;
    uint32_t packets_received = 0;
    uint32_t packets_timeout = 0;

    // Mock a packet
    expect_any(__wrap_nfqueue_read, nfqueue);
    will_return(__wrap_nfqueue_read, 5);
    will_return(__wrap_nfqueue_read, 3);
    will_return(__wrap_nfqueue_read, 0);
    will_return(__wrap_nfqueue_read, 0);
    will_return(__wrap_nfqueue_read, 17);
    will_return(__wrap_nfqueue_read, "Hello World");
    will_return(__wrap_nfqueue_read, 0);

    // Check if the mock comm config will send out the packet
    expect_string(dummy_send_packet, name, "dummy_1");
    expect_value(dummy_send_packet, id, 3);
    expect_value(dummy_send_packet, group, 5);

    // Mock a new packet being intercepted and check if timer is started
    packet_handler_packet_cb(5, ic_cond);
    amxut_bus_handle_events();
    assert_int_equal(amxp_timer_get_state(ic_cond->handler->timer), amxp_timer_running);

    // Check that the test intercept condition will be looked up after timeout
    expect_value(__wrap_ic_condition_find_by_qnum, qnum, 5);

    // Check that the default verdict will be given for the packet after timeout
    expect_any(__wrap_nfqueue_judge_packet_mark, nfqueue);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_id, 3);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_verdict, NF_DROP);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_mark, 0);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_connmark, 4 | packetinterception_get_notoffload_mark());
    will_return(__wrap_nfqueue_judge_packet_mark, 0);

    amxut_timer_go_to_future_ms(1000000);

    // Check that the correct statistics have increased
    object = amxd_dm_findf(amxut_bus_dm(), "PacketInterception.PacketHandler.1.Stats");
    packets_received = amxd_object_get_value(uint32_t, object, "NrOfPacketsReceived", NULL);
    packets_timeout = amxd_object_get_value(uint32_t, object, "NrOfPacketsTimedout", NULL);

    assert_int_equal(packets_received, 3);
    assert_int_equal(packets_timeout, 2);
}

void test_packethandler_reply_connmark(UNUSED void** state) {
    amxd_object_t* object = NULL;
    uint32_t packets_received = 0;
    uint32_t packets_handled = 0;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* verdict_var = NULL;
    packet_verdict_t verdict;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    // Mock a packet
    expect_any(__wrap_nfqueue_read, nfqueue);
    will_return(__wrap_nfqueue_read, 5);
    will_return(__wrap_nfqueue_read, 4);
    will_return(__wrap_nfqueue_read, 0);
    will_return(__wrap_nfqueue_read, 6);
    will_return(__wrap_nfqueue_read, 17);
    will_return(__wrap_nfqueue_read, "Hello World");
    will_return(__wrap_nfqueue_read, 0);

    // Check if the mock comm config will send out the packet
    expect_string(dummy_send_packet, name, "dummy_1");
    expect_value(dummy_send_packet, id, 4);
    expect_value(dummy_send_packet, group, 5);

    // Mock a new packet being intercepted
    packet_handler_packet_cb(5, ic_cond);
    amxut_bus_handle_events();

    // Check that the test intercept condition will be looked up after a mock reply is received
    expect_value(__wrap_ic_condition_find_by_qnum, qnum, 5);

    // // Check that a custom verdict will be given for the packet after a mock reply is received
    expect_any(__wrap_nfqueue_judge_packet_mark, nfqueue);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_id, 4);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_verdict, NF_ACCEPT);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_mark, 0);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_connmark, 0 | packetinterception_get_notoffload_mark());
    will_return(__wrap_nfqueue_judge_packet_mark, 0);

    //Send out a reply from the mock config
    verdict.verdict = NF_ACCEPT;
    verdict.mark = 0;
    verdict.mark_mask = 0;
    verdict.connmark = 8;
    verdict.connmark_mask = 7;

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Communication Config", "dummy_1");
    amxc_var_add_key(uint32_t, &args, "ID", 4);
    amxc_var_add_key(uint32_t, &args, "Group", 5);
    verdict_var = amxc_var_add_new_key(&args, "Verdict");
    amxc_var_set_packet_verdict_t(verdict_var, &verdict);
    assert_int_equal(amxm_execute_function("self", "packet-interception", "comm-config-verdict", &args, &ret), 0);

    amxut_bus_handle_events();

    // Check that the correct statistics have increased
    object = amxd_dm_findf(amxut_bus_dm(), "PacketInterception.PacketHandler.1.Stats");
    packets_received = amxd_object_get_value(uint32_t, object, "NrOfPacketsReceived", NULL);
    packets_handled = amxd_object_get_value(uint32_t, object, "NrOfPacketsHandled", NULL);

    assert_int_equal(packets_received, 4);
    assert_int_equal(packets_handled, 2);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_packethandler_reply_big_packet_id(UNUSED void** state) {
    amxd_object_t* object = NULL;
    uint32_t packets_received = 0;
    uint32_t packets_handled = 0;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* verdict_var = NULL;
    packet_verdict_t verdict;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    // Mock a packet
    expect_any(__wrap_nfqueue_read, nfqueue);
    will_return(__wrap_nfqueue_read, 5);
    will_return(__wrap_nfqueue_read, 1000000);
    will_return(__wrap_nfqueue_read, 0);
    will_return(__wrap_nfqueue_read, 0);
    will_return(__wrap_nfqueue_read, 17);
    will_return(__wrap_nfqueue_read, "Hello World");
    will_return(__wrap_nfqueue_read, 0);

    // Check if the mock comm config will send out the packet
    expect_string(dummy_send_packet, name, "dummy_1");
    expect_value(dummy_send_packet, id, 1000000);
    expect_value(dummy_send_packet, group, 5);

    // Mock a new packet being intercepted
    packet_handler_packet_cb(5, ic_cond);
    amxut_bus_handle_events();

    // Check that the test intercept condition will be looked up after a mock reply is received
    expect_value(__wrap_ic_condition_find_by_qnum, qnum, 5);

    // // Check that a custom verdict will be given for the packet after a mock reply is received
    expect_any(__wrap_nfqueue_judge_packet_mark, nfqueue);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_id, 1000000);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_verdict, NF_ACCEPT);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_mark, 3);
    expect_value(__wrap_nfqueue_judge_packet_mark, pkt_connmark, 0 | packetinterception_get_notoffload_mark());
    will_return(__wrap_nfqueue_judge_packet_mark, 0);

    //Send out a reply from the mock config
    verdict.verdict = NF_ACCEPT;
    verdict.mark = 3;
    verdict.mark_mask = 3;
    verdict.connmark = 0;
    verdict.connmark_mask = 0;

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Communication Config", "dummy_1");
    amxc_var_add_key(uint32_t, &args, "ID", 1000000);
    amxc_var_add_key(uint32_t, &args, "Group", 5);
    verdict_var = amxc_var_add_new_key(&args, "Verdict");
    amxc_var_set_packet_verdict_t(verdict_var, &verdict);
    assert_int_equal(amxm_execute_function("self", "packet-interception", "comm-config-verdict", &args, &ret), 0);

    amxut_bus_handle_events();

    // Check that the correct statistics have increased
    object = amxd_dm_findf(amxut_bus_dm(), "PacketInterception.PacketHandler.1.Stats");
    packets_received = amxd_object_get_value(uint32_t, object, "NrOfPacketsReceived", NULL);
    packets_handled = amxd_object_get_value(uint32_t, object, "NrOfPacketsHandled", NULL);

    assert_int_equal(packets_received, 5);
    assert_int_equal(packets_handled, 3);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_packethandler_dm_delete(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    fprintf(stdout, "delete instance, allocated memory should be freed\n");
    amxd_trans_select_pathf(&trans, "PacketInterception.PacketHandler.");
    amxd_trans_del_inst(&trans, 0, TEST_HANDLER);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxut_bus_handle_events();
    amxd_trans_clean(&trans);

    fprintf(stdout, "cleanup\n");
}
