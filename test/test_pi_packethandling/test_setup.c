/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "packet-handler/packet-handler.h"
#include "packet-handler/dm_packet-handler.h"
#include "interception/ic_condition.h"

#include "test_setup.h"
#include "ic_condition_mock.h"

#define TEST_HANDLER    "handler-test"

static amxm_module_t* mod = NULL;

int dummy_send_packet(UNUSED const char* const function_name, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    amxc_var_t* data = NULL;
    uint32_t id;
    uint32_t group;
    const char* name = NULL;

    assert_non_null(args);

    name = GET_CHAR(args, "Name");
    id = GET_UINT32(args, "ID");
    group = GET_UINT32(args, "Group");
    data = GET_ARG(args, "Data");

    assert_non_null(data);

    check_expected(id);
    check_expected(group);
    check_expected(name);

    return 0;
}

int test_packethandling_setup(UNUSED void** state) {
    amxm_shared_object_t* so = amxm_get_so("self");
    amxd_dm_t* dm = NULL;

    mock_init(state);
    dm = amxut_bus_dm();

    amxm_module_register(&mod, so, TEST_MOD);
    assert_int_equal(amxm_module_add_function(mod, "send-packet", dummy_send_packet), 0);

    assert_int_equal(_packetinterception_main(0, dm, amxut_bus_parser()), 0);

    amxp_sigmngr_emit_signal(&dm->sigmngr, "app:start", NULL);

    amxut_bus_handle_events();

    test_ic_condition_create();

    return 0;
}

int test_packethandling_teardown(UNUSED void** state) {
    test_ic_condition_delete();

    mock_cleanup(state);

    return 0;
}
